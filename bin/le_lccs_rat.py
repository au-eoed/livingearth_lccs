#!/usr/bin/env python
"""
Script to run Living Earth LCCS Classification on a Raster Attribute
Table.

"""

import argparse
import os
import sys
import yaml

import numpy
import xarray

from osgeo import gdal
from rios import rat

# Add path relative to script (makes it easier for development)
sys.path.append(os.path.abspath("../"))

# Import le_lccs modules
from le_lccs import le_ingest
from le_lccs.le_classification import lccs_l3

def run_classification(config_file):

    # Read in config file
    with open(config_file, "r") as f:
        config = yaml.load(f)

    found_file = False
    rat_file = ""
    for var_name in config["Layers"]:
        if "input_file" in config["Layers"][var_name]:
            rat_file = config["Layers"][var_name]["input_file"]
            found_file = True
            break

    if not found_file:
        raise Exception("Could not find input RAT file for any layers")

    rat_ds = gdal.Open(rat_file, gdal.GA_ReadOnly)
    if rat_ds is None:
        raise IOError("Could not open '{}'".format(rat_file))

    # Get number of rows
    num_rows = rat.readColumn(rat_ds, rat.getColumnNames(rat_ds)[0]).size
    # Set up an array with object IDs
    object_id = numpy.arange(0, num_rows)

    rat_ds = None

    # Read each into xarray and save to list
    print("Reading in data")
    variables_xarray_list = []
    for var_name, ingest_parameters in config["Layers"].items():
        # Get the class to use for importing
        # TODO: Need to write a wrapper to make recursive calls of `getattr` nicer.
        import_class_name = ingest_parameters["ingest_class"]
        ImportClass = getattr(getattr(le_ingest, import_class_name.split(".")[0]),
                              import_class_name.split(".")[1])

        # Set up instance of import class
        import_obj = ImportClass(object_id)
        # Import
        ingest_parameters["variable_name"] = var_name
        variables_xarray_list.append(import_obj.read_to_xarray(**ingest_parameters))

    # Merge to a single dataset
    classification_data = xarray.merge(variables_xarray_list)

    # Apply Level 3 classification
    print("Applying Classification")
    level1, level2, level3 = lccs_l3.classify_lccs_level3(classification_data)

    # Get colour scheme
    red, green, blue, alpha = lccs_l3.colour_lccs_level3(level3)

    # Write columns back to RAT
    # TODO: Will have separate classes for writing data back out, defined in config.
    print("Writing back to RAT")
    rat_ds = gdal.Open(rat_file, gdal.GA_Update)

    if rat_ds is None:
        raise IOError("Could not open '{}' to update".format(rat_file))

    rat.writeColumn(rat_ds, "level3", level3)
    rat.writeColumn(rat_ds, "Red", red)
    rat.writeColumn(rat_ds, "Green", green)
    rat.writeColumn(rat_ds, "Blue", blue)
    rat.writeColumn(rat_ds, "Alpha", alpha)

    rat_ds = None

if __name__ == "__main__":

    # Read in config file
    parser = argparse.ArgumentParser(description="Run LE LCCS Classification "
                                                 "on a RAT.")
    parser.add_argument("configfile",
                        type=str,
                        nargs=1,
                        help="Config file")
    args = parser.parse_args()
    run_classification(args.configfile[0])
