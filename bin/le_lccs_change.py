#!/usr/bin/env python
"""
Script to calculate a change image from two input geotiff files.
"""
import argparse

from le_lccs.le_utils import change_utils

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--year1", type=str, required=True,
                        help="Specify geotiff with year 1 classification")
    parser.add_argument("--year2", type=str, required=True,
                        help="Specify geotiff with year 2 classification")
    parser.add_argument("--output_l3_change_file_name", type=str,
                        help="Path to Level 3 change output file",
                        required=False, default=None)
    parser.add_argument("--output_l4_change_file_name", type=str,
                        help="Path to Level 4 change output file",
                        required=False, default=None)
    parser.add_argument("--output_observed_change_file_name", type=str,
                        help="Path to Observed Change output file (data)",
                        required=False, default=None)
    parser.add_argument("--include_no_change", action="store_true", default=False,
                        help="Include values for pixels which haven't changed "
                             "defaut is to set to no-data")
    parser.add_argument("--classification_scheme", type=str,
                        help="Specify modified class structure if needed",
                        required=False, default="lccs")
    args = parser.parse_args()

    change_utils.calc_change_gtif(args.year1, args.year2,
                                  out_level3_change_file=args.output_l3_change_file_name,
                                  out_observed_change_file=args.output_observed_change_file_name,
                                  out_level4_change_file=args.output_l4_change_file_name,
                                  ignore_no_change=(not args.include_no_change),
                                  classification_scheme=args.classification_scheme)

