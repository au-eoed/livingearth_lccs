#!/usr/bin/env python
"""
Script to run Living Earth LCCS Classification on gridded data

"""

import argparse
import logging
import os
import sys

# Add path relative to script (makes it easier for development)
sys.path.append(os.path.abspath("../"))

# Import le_lccs modules
from le_lccs.le_utils import gridded_classification

if __name__ == "__main__":

    # Read in config file
    parser = argparse.ArgumentParser(description="Run LE LCCS Classification "
                                                 "on gridded data..")
    parser.add_argument("configfile",
                        type=str,
                        nargs=1,
                        help="Config file")
    parser.add_argument("--log", type=str,
                        help="Log file to write to. If none is provided then sends to stdout",
                        required=False,
                        default=None)
    parser.add_argument("--extent", type=float,
                        nargs=4,
                        help="Extent (min_x, min_y, max_x, max_y)",
                        required=False,
                        default=None)
    parser.add_argument("--output_l3_file_name", type=str,
                        help="Name Level 3 output file (data)",
                        required=False,
                        default=None)
    parser.add_argument("--output_l3_rgb_file_name", type=str,
                        help="Name Level 3 output file (RGB)",
                        required=False,
                        default=None)
    parser.add_argument("--output_l4_file_name", type=str,
                        help="Name Level 4 output file (data)",
                        required=False,
                        default=None)
    parser.add_argument("--output_l4_rgb_file_name", type=str,
                        help="Name Level 4 output file (RGB)",
                        required=False,
                        default=None)
    parser.add_argument("--level3_only", action="store_true",
                        help="Only run level 3 classificaation (ignoring level4 in config)",
                        required=False,
                        default=False)
    parser.add_argument("--product_date", type=str,
                        help="Start Date of Product",
                        required=False,
                        default=None)
    parser.add_argument("--tile-id", type=int,
                        nargs=2,
                        help="(x, y)",
                        required=False,
                        default=None)

    args = parser.parse_args()

    # Set up logger
    if args.log is None:
        logging.basicConfig(stream=sys.stdout, level=logging.WARNING)
    else:
        logging.basicConfig(filename=args.log, level=logging.INFO)

    gridded_classification.run_classification(
        args.configfile[0], args.extent,
        output_l3_file_name=args.output_l3_file_name,
        output_l3_rgb_file_name=args.output_l3_rgb_file_name,
        output_l4_file_name=args.output_l4_file_name,
        output_l4_rgb_file_name=args.output_l4_rgb_file_name,
        level3_only=args.level3_only,
        product_date=args.product_date,
        tile_id=args.tile_id)

