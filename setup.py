#!/usr/bin/env python
"""
Setup script for LivingEarth LCCS Library. Use like this for Unix:

$ python setup.py install

"""
# This file is part of 'livingearth_lccs'
#
# Copyright 2019.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Purpose: Installation of the LivingEarth LCCS software
#
# Author: Pete Bunting
# Email: pfb@aber.ac.uk
# Date: 04/01/2019
# Version: 1.0
#
# History:
# Version 1.0 - Created.

from distutils.core import setup
import os
import glob

setup(name='LivingEarth_LCCS',
      version='1.1.0',
      description='A python module to perform a classification using the FAO LCCS-2.',
      author='Richard Lucas, Dan Clewley, Ben Lewis, Belle Tissott, Pete Bunting, Chris Owers, Carole Planque',
      author_email='richard.lucas@aber.ac.uk',
      scripts=glob.glob('bin/*.py'),
      packages=['le_lccs', 'le_lccs.le_classification', 'le_lccs.le_ingest', 'le_lccs.le_export', 'le_lccs.le_utils'],
      data_files=[(os.path.join('share','lccs_colours'),
                  glob.glob(os.path.join('colour_schemes','*')))],
      license='LICENSE.txt',
      url='https://www.livingearth.online',
      classifiers=['Intended Audience :: Developers',
                   'Intended Audience :: Remote Sensing Scientists',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python :: 3.5',
                   'Programming Language :: Python :: 3.6',
                   'Programming Language :: Python :: 3.7'])
