Data Ingestion
==================

.. automodule:: le_lccs.le_ingest.ingest_base
   :members:
   :undoc-members:

Gridded Ingest
~~~~~~~~~~~~~~~

.. automodule:: le_lccs.le_ingest.gridded_ingest
   :members:
   :undoc-members:

Table Ingest
~~~~~~~~~~~~~~~

.. automodule:: le_lccs.le_ingest.table_ingest
   :members:
   :undoc-members:


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
