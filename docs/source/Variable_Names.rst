Variable Name Rules
===================

General rules
--------------

All variables should be lower case and use an underscore (``_``) to
separate works (i.e., snake_case).

An examples is:

    lifeform_veg_cat_l4a

+-----------+-----------+-----------+-----------+
| Section 1 | Section 2 | Section 3 | Section 4 |
+===========+===========+===========+===========+
| lifeform  | veg       | cat       | l4a       |
+-----------+-----------+-----------+-----------+
| lifeform  | wat       | con       | l4m       |
+-----------+-----------+-----------+-----------+
| lifeform  | phy       | cin       | l4d       |
+-----------+-----------+-----------+-----------+
| lifeform  | agr       |           |           |
+-----------+-----------+-----------+-----------+
| lifeform  | urb       |           |           |
+-----------+-----------+-----------+-----------+
| lifeform  | mar       |           |           |
+-----------+-----------+-----------+-----------+
| lifeform  | atm       |           |           |
+-----------+-----------+-----------+-----------+

Section 1
~~~~~~~~~~~

All variables should start with a unique 9 character identifier. The 9
character limit allows then to be exported to a shapefile.

Section 2
~~~~~~~~~~~

* vegetation (veg)
* urban (urb)
* agriculture (agr)
* physical (phy)
* atmosphere (atm)
* marine (mar)

    
Section 3
~~~~~~~~~~~

* Categorical (cat)
* Continuous - real (con)
* Continuous - integer (cin)

    
Section 4
~~~~~~~~~~

Used to indicate output from Level 4 classification

* l4a (for top level)
* l4m (modifier)
* l4d (derived - from a continuous surface)

    
Level 3 Classification
------------------------

Inputs
~~~~~~~

The following columns are used (if not provided they are ignored and a
warning given) to perform the level 1-3 classification:

+-----------------+-------------------------------------------------------+
| Variable        | Description                                           |
| Name            |                                                       |
+=================+=======================================================+
| vegetat_veg_cat | Binary column indicating the present (or otherwise)   |
|                 | of vegetation.                                        |
+-----------------+-------------------------------------------------------+
| aquatic_wat_cat | Binary column indicating the present (or otherwise)   |
|                 | of an aquatic land cover (e.g., water).               |
+-----------------+-------------------------------------------------------+
| cultman_agr_cat | Binary column indicating the present (or otherwise)   |
|                 | of cultivation.                                       |
+-----------------+-------------------------------------------------------+
| artific_urb_cat | Binary column indicating the present (or otherwise)   |
|                 | of Urban areas.                                       |
+-----------------+-------------------------------------------------------+
| artwatr_wat_cat | Binary column indicating the present (or otherwise)   |
|                 | of artifical water bodies.                            |
+-----------------+-------------------------------------------------------+

Outputs
~~~~~~~~~

The following columns are output by the level 1-3 classification:

+--------------+-------------------------------------------------------+
| Variable     | Description                                           |
| Name         |                                                       |
+==============+=======================================================+
| level1       | Assign level 1 class of primarily vegetated (A,100)   |
|              | or primarily non-vegetated (B,200).                   |
+--------------+-------------------------------------------------------+
| level2       | Assign level 2 class of terrestrial (10) or aquatic   |
|              | (20)                                                  |
+--------------+-------------------------------------------------------+
| level3       | Assign level 3 classes see codes from table above.    |
+--------------+-------------------------------------------------------+

The class names, codes and numerical codes for the Level 3
classification (level3) are shown below.

+----------------------------------+------+--------------+
| Class name                       | Code | Numeric code |
+==================================+======+==============+
| Cultivated Terrestrial Vegetated | A11  | 111          |
+----------------------------------+------+--------------+
| Natural Terrestrial Vegetated    | A12  | 112          |
+----------------------------------+------+--------------+
| Cultivated Aquatic Vegetated     | A23  | 123          |
+----------------------------------+------+--------------+
| Natural Aquatic Vegetated        | A24  | 124          |
+----------------------------------+------+--------------+
| Artificial Surface               | B15  | 215          |
+----------------------------------+------+--------------+
| Natural Surface                  | B16  | 216          |
+----------------------------------+------+--------------+
| Artificial Water                 | B27  | 227          |
+----------------------------------+------+--------------+
| Natural Water                    | B28  | 228          |
+----------------------------------+------+--------------+

Level 4 Classification
------------------------

Required Inputs
~~~~~~~~~~~~~~~~

The following columns are needed to perform the Level 4 classification

+-------------------+---------------------------------------------------------+
| Variable          | Description                                             |
| Name              |                                                         |
+===================+=========================================================+
| lifeform_veg_cat  | woody (trees, shrubs), herbeceous (graminoids, forbs)   |
|                   | & cryptograms (lichens, mosses)                         |
+-------------------+---------------------------------------------------------+
| spatdist_veg_cat  | spatial distribution (fragmented or continuous)         |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| leaftype_veg_cat  | broad-leaved, needle-leaved or aphyllous                |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| phenolog_veg_cat  | evergreen, deciduous, mixed, mixed (forbs, graminoids)  |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| strat2nd_veg_cat  | second layer present or absent                          |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| lifef2nd_veg_cat  | lifeform of the second layer                            |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| spatsize_agr_cat  | spatial size (ha)                                       |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| cropcomb_agr_cat  | crop combinations                                       |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| croplfc2_agr_cat  | crop lifeform for crop combinations C3 and C4           |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| croplfc3_agr_cat  | crop lifeform for crop combination C4                   |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| cropseqt_agr_cat  | crop sequences for cultivated terrestrial vegetation    |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| cropseqa_agr_cat  | crop sequences for cultivated aquatic vegetation        |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| watersup_agr_cat  | crop water supply                                       |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| timefact_agr_cat  | crop time factor                                        |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| waterday_agr_cat  | crop daily water variations                             |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| watersea_veg_cat  | water seasonality in (semi-)natural areas               |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| waterstt_wat_cat  | water state (water, ice or snow)                        |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| inttidal_wat_cat  | Tidal areas                                             |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| waterdtp_wat_cat  | water depth (shallow (<= 2 m) or deep (> 2m)            |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| wsedload_wat_cat  | water sediment loads (clear or turbid)                  |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| baresurf_phy_cat  | bare surface materials (consolidated or unconsolidated) |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| macropat_phy_cat  | macropattern (e.g., dune types)                         |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| urbanveg_urb_cat  | urban vegetation type (e.g., parks, lawns)              |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| artisurf_urb_cat  | artificial surface (urban built up or non-built up)     |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| mlifefrm_veg_cat  | Lifeform (free floating or rooted forbs)                |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| mphenlog_veg_cat  | Detailed leaf phenology (semi-, annual,                 |
|                   | perennial)                                              |
+-------------------+---------------------------------------------------------+
| mwatrmvt_wat_cat  | Water movement (flowing or standing)                    |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| msubstrt_wat_cat  | Substrate material (below water) (rock, soil, sand)     |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| mbaremat_phy_cat  | Bare materials (e.g., sands, muds, rock)                |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| mustones_phy_cat  | Unconsolidated materials (gravels, stones, boulders)    |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| mhardpan_phy_cat  | Physical descriptors                                    |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| mtclinea_urb_cat  | Linear structures (paved or unpaved roads)              |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| mnolinea_urb_cat  | non-linear surfaces (e.g., industrial)                  |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| murbdens_urb_cin  | Urban density (e.g. high, medium, low, scattered) (%)   |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| mnobuilt_urb_cat  | Non-built up areas (e.g., industrial sites, waste and   |
|                   | extraction sites)                                       |
+-------------------+---------------------------------------------------------+
| canopyco_veg_con  | Canopy cover (%)                                        |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| canopyht_veg_con  | Canopy height (m)                                       |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| cover2nd_veg_cat  | cover of the 2nd layer                                  |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| heigh2nd_veg_cat  | height of the 2nd layer                                 |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| waterper_wat_cin  | Water persistence (hydroperiod) (days)                  |
|                   |                                                         |
+-------------------+---------------------------------------------------------+
| snowcper_wat_cin  | Snow persistence (hydroperiod) (days)                   |
|                   |                                                         |
+-------------------+---------------------------------------------------------+

Environmental variables
~~~~~~~~~~~~~~~~~~~~~~~

In addition to the layers needed for the classification a number of
environmental variables may be included for additional analysis and use
in change detection.

Terrain
^^^^^^^

+----------------------+-------------------------------------+
| Variable Name        | Description                         |
+======================+=====================================+
| altitude_ter_env_con | Terrain altitude (m)                |
+----------------------+-------------------------------------+
| gradient_ter_env_con | Terrain gradient or slope (degrees) |
+----------------------+-------------------------------------+
| lnaspect_ter_env_con | Terrain aspect (degrees)            |
+----------------------+-------------------------------------+

Agriculture
^^^^^^^^^^^

+----------------------+------------------------------+
| Variable Name        | Description                  |
+======================+==============================+
| cropprod_agr_env_con | Herbaceous biomass (kg ha-1) |
+----------------------+------------------------------+
| croptype_agr_env_cat | Crop species type            |
+----------------------+------------------------------+

Vegetation
^^^^^^^^^^

+----------------------+-------------------------------------------------------+
| Variable Name        | Description                                           |
|                      |                                                       |
+======================+=======================================================+
| plantspd_veg_env_cat | Plant species (dominant)                              |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| plantspc_veg_env_cat | Plant species (co-dominant)                           |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| plantsps_veg_env_cat | Plant species (sub-dominant)                          |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| agbiomas_veg_env_con | Above ground biomass (Mg ha-1)                        |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| bgbiomas_veg_env_con | Below ground biomass (Mg ha-1)                        |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| gsvolume_veg_env_con | Gross standing volume (m3 m-3)                        |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| wooddens_veg_env_con | Wood density (g cm-3)                                 |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| fcchloro_veg_env_con | Foliar chemistry (chlorophyll; g cm-3)                |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| fclignin_veg_env_con | Foliar chemistry (lignin; g cm-3)                     |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| fcnitrog_veg_env_con | Foliar chemistry (nitrogen; g cm-3)                   |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| vegmoist_veg_env_con | Vegetation moisture (%)                               |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| lfareain_veg_env_con | Leaf area index (LA)                                  |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| annuanpp_veg_env_con | Annual Net Primary Productivity                       |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| annuagpp_veg_env_con | Annual Gross Primary Productivity                     |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| fractpar_veg_env_con | Fraction of Photosynthetically Active Radiation       |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| burnsevr_veg_env_cat | Burn severity                                         |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| leafflus_veg_env_cin | Vegetation green up (Day no since January 1st)        |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| leaffall_veg_env_cin | Vegetation senescenece or leaf fall (Day no since     |
|                      | January 1)                                            |
+----------------------+-------------------------------------------------------+
| fractgvg_veg_env_con | Fraction of Photosynthetic Vegetation (%)             |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| fractnpv_veg_env_con | Fraction of Non-Photosynthetic Vegetation (%)         |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| seasonlg_veg_env_cin | Season length (days)                                  |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| vegagedy_veg_env_cin | Vegetation age (days)                                 |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| vegagedy_veg_env_cin | Vegetation age (years)                                |
|                      |                                                       |
+----------------------+-------------------------------------------------------+

Snow and ice
^^^^^^^^^^^^

+----------------------+-------------------------+
| Variable Name        | Description             |
+======================+=========================+
| snowcofr_sni_env_con | Snow cover fraction (%) |
+----------------------+-------------------------+
| snowgrsz_sni_env_con | Snow grain size (mm)    |
+----------------------+-------------------------+
| snowmois_sni_env_con | Soil moisture (%)       |
+----------------------+-------------------------+
| snowwteq_sni_env_con | Snow water equivalent   |
+----------------------+-------------------------+
| snowdept_sni_env_con | Snow depth (cm)         |
+----------------------+-------------------------+

Physical and bare
^^^^^^^^^^^^^^^^^

+----------------------+--------------------------------------+
| Variable Name        | Description                          |
+======================+======================================+
| landtemp_phy_env_con | Land surface Temperature (degrees C) |
+----------------------+--------------------------------------+
| terragsz_phy_env_con | Grain size (mm)                      |
+----------------------+--------------------------------------+
| frostday_phy_env_cin | Frostdays (no)                       |
+----------------------+--------------------------------------+
| soilmois_phy_env_con | Soil moisture                        |
+----------------------+--------------------------------------+
| soilacid_phy_env_con | Soil acidity (pH)                    |
+----------------------+--------------------------------------+
| lsalbedo_phy_env_con | Land albedo                          |
+----------------------+--------------------------------------+

Marine
^^^^^^

+----------------------+--------------------------------+
| Variable Name        | Description                    |
+======================+================================+
| surftemp_mar_env_con | Sea surface temperature        |
+----------------------+--------------------------------+
| mcchloro_mar_env_con | Marine chemicstry: chlorophyll |
+----------------------+--------------------------------+
| dissorgm_mar_env_con | Dissolved organic matter       |
+----------------------+--------------------------------+
| totalsus_mar_env_con | Total suspended matter         |
+----------------------+--------------------------------+
| slevelpr_mar_env_con | Sea level pressure             |
+----------------------+--------------------------------+
| bathymtr_mar_env_con | Bathymetry (m)                 |
+----------------------+--------------------------------+

Atmosphere
^^^^^^^^^^

+----------------------+----------------------------+
| Variable Name        | Description                |
+======================+============================+
| cloudcov_atm_env_con | Cloud cover (%)            |
+----------------------+----------------------------+
| precipit_atm_env_con | Precipitation (mm)         |
+----------------------+----------------------------+
| humidity_atm_env_con | Air humidity               |
+----------------------+----------------------------+
| airtempr_atm_env_con | Air temperature (degree C) |
+----------------------+----------------------------+
| windvely_atm_env_con | Wind velocity              |
+----------------------+----------------------------+
| winddirc_atm_env_con | Wind direction             |
+----------------------+----------------------------+

.. _outputs-1:

Outputs
-------

Main
~~~~

+----------------------+-------------------------------------------------------+
| Variable             | Description                                           |
| Name                 |                                                       |
+======================+=======================================================+
| lifeform_veg_cat_l4a | woody (trees, shrubs), herbeceous (grasses, forbs) &  |
|                      | cryptograms (lichens, mosses)                         |
+----------------------+-------------------------------------------------------+
| spatdist_veg_cat_l4a | spatial distribution (fragmented or continuous)       |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| leaftype_veg_cat_l4a | broad-leaved, needle-leaved or aphyllous              |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| phenolog_veg_cat_l4a | evergreen, deciduous, mixed, mixed (forbs, graminoids)|
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| strat2nd_veg_cat_l4a | second layer present or absent                        |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| lifef2nd_veg_cat_l4a | lifeform of the second layer                          |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| spatsize_agr_cat_l4a | spatial size (ha)                                     |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| cropcomb_agr_cat_l4a | crop combinations                                     |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| croplfc2_agr_cat_l4a | crop lifeform for crop combinations C3 and C4         |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| croplfc3_agr_cat_l4a | crop lifeform for crop combination C4                 |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| cropseqt_agr_cat_l4a | crop sequences for cultivated terrestrial vegetation  |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| cropseqa_agr_cat_l4a | crop sequences for cultivated aquatic vegetation      |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| watersup_agr_cat_l4a | crop water supply                                     |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| timefact_agr_cat_l4a | crop time factor                                      |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| waterday_agr_cat_l4a | crop daily water variations                           |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| watersea_veg_cat_l4a | water seasonality in (semi-)natural areas             |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| waterstt_wat_cat_l4a | water state (water, ice or snow)                      |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| inttidal_wat_cat_l4a | Tidal areas                                           |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| waterdtp_wat_cat_l4a | water depth (shallow (<= 2 m) or deep (> 2m)          |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| wsedload_wat_cat_l4a | water sediment loads (clear or turbid)                |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| baresurf_phy_cat_l4a | bare surface materials (consolidated or               |
|                      | unconsolidated)                                       |
+----------------------+-------------------------------------------------------+
| macropat_phy_cat_l4a | macropattern (e.g., dune types)                       |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| urbanveg_urb_cat_l4a | urban vegetation type (e.g., parks, lawns)            |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| artisurf_urb_cat_l4a | artificial surface (urban built up or non-built up)   |
|                      |                                                       |
+----------------------+-------------------------------------------------------+

Modifiers
~~~~~~~~~

+----------------------+-------------------------------------------------------+
| Variable             | Description                                           |
| Name                 |                                                       |
+======================+=======================================================+
| mlifefrm_veg_cat_l4m | Lifeform (free floating or rooted forbs)              |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| mphenlog_veg_cat_l4m | Detailed leaf phenology (semi-, annual,               |
|                      | perennial)                                            |
+----------------------+-------------------------------------------------------+
| mwatrmvt_wat_cat_l4m | Water movement (flowing or standing)                  |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| msubstrt_wat_cat_l4m | Substrate material (below water) (rock, soil, sand)   |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| mbaremat_phy_cat_l4m | Bare materials (e.g., sands, muds, rock)              |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| mustones_phy_cat_l4m | Unconsolidated materials (gravels, stones, boulders)  |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| mhardpan_phy_cat_l4m | Physical descriptors                                  |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| mtclinea_urb_cat_l4m | Linear structures (paved or unpaved roads)            |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| mnolinea_urb_cat_l4m | non-linear surfaces (e.g., industrial)                |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| murbdens_urb_cat_l4m | Urban density (e.g. high, medium, low, scattered) (%) |
|                      |                                                       |
+----------------------+-------------------------------------------------------+
| mnobuilt_urb_cat_l4m | Non-built up areas (e.g., industrial sites, waste and |
|                      | extraction sites)                                     |
+----------------------+-------------------------------------------------------+

Derivatives (categorical from continuous)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+----------------------+---------------------------------+
| Variable Name        | Description                     |
+======================+=================================+
| canopyco_veg_cat_l4d | Canopy cover                    |
+----------------------+---------------------------------+
| canopyht_veg_cat_l4d | Canopy height                   |
+----------------------+---------------------------------+
| cover2nd_veg_cat_l4d | cover of the 2nd layer          |
+----------------------+---------------------------------+
| heigh2nd_veg_cat_l4d | height of the 2nd layer         |
+----------------------+---------------------------------+
| waterper_wat_cat_l4d | Water persistence (hydroperiod) |
+----------------------+---------------------------------+
| snowcper_wat_cat_l4d | Snow persistence (hydroperiod)  |
+----------------------+---------------------------------+
