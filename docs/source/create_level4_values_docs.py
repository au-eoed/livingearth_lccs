#!/usr/bin/env python
"""
A script to create RST tables for all level 4 classes showing their values.

Takes directly from code so can automatically regenerate new documentation if names change

Dan Clewley - 2019-11-12


"""

import sys
sys.path.append("../../")

from le_lccs.le_classification import lccs_l4
from le_lccs.le_classification import l4_base

def get_table(outfile, layer_type, classification_scheme="lccs"):

    for layer_class in lccs_l4.LAYER_CLASSES[classification_scheme]:
        layer = layer_class()

        if layer.layer_type == layer_type:
            layer_name = layer.output_layer_name
            
            outfile.write("\n")
            outfile.write(layer_name + "\n")
            outfile.write(("~" * len(layer_name)) + "\n")
            outfile.write("\n")

            outfile.write("+" + "-" * 8 + "+" + "-" * 12 + "+" + "-" * 61 + "+" + "\n")
            outfile.write("| {code: <7}| {str_code: <11}| {str_desc: <60}|"
                      "".format(code="Value", str_code="LCCS Code",
                                str_desc="LCCS Description") + "\n")
            outfile.write("+" + "=" * 8 + "+" + "=" * 12 + "+" + "=" * 61 + "+" + "\n")


            for code_int, code_desc_str in layer.output_codes_descriptions.items():
                outfile.write("| {code: <7}| {str_code: <11}| {str_desc: <60}|"
                      "".format(code=code_int, str_code=code_desc_str[0],
                                str_desc=code_desc_str[1]) + "\n")
                outfile.write("+" + "-" * 8 + "+" + "-" * 12 + "+" + "-" * 61 + "+" + "\n")

 
if __name__ == "__main__":

    # Standard classification
    with open("Level4_Layer_Values.rst", "w") as outfile:
        outfile.write("Level 4 Classification - FAO: Layer Values\n")
        outfile.write(("=" * len("Level 4 Classification Layer Values - FAO: Layer Values)")) + "\n")
        outfile.write("\n")
        outfile.write("These are all the layers used in the implementation of the FAO classification scheme (lccs).\n")
        outfile.write("\n")

        outfile.write("Main\n")
        outfile.write(("-" * len("Main")) + "\n")
        get_table(outfile, l4_base.ClassificationLayerTypes.MAIN, "lccs")
        outfile.write("\n")

        outfile.write("Derivatives\n")
        outfile.write(("-" * len("Derivatives")) + "\n")
        get_table(outfile, l4_base.ClassificationLayerTypes.DERIVATIVE, "lccs")
        outfile.write("\n")

        outfile.write("Modifiers\n")
        outfile.write(("-" * len("Modifiers")) + "\n")
        get_table(outfile, l4_base.ClassificationLayerTypes.MODIFIER, "lccs")
        outfile.write("\n")

    # AU classification
    with open("Level4_Layer_Values_AU.rst", "w") as outfile:
        outfile.write("Level 4 Classification - Modified version for Australia: Layer Values\n")
        outfile.write(("=" * len("Level 4 Classification - Modified version for Australia: Layer Values")) + "\n")
        outfile.write("\n")
        outfile.write("These are all the layers used in the implementation of modified LCCS classification scheme for Australia (au).\n")
        outfile.write("\n")

        outfile.write("Main\n")
        outfile.write(("-" * len("Main")) + "\n")
        get_table(outfile, l4_base.ClassificationLayerTypes.MAIN, "au")
        outfile.write("\n")

        outfile.write("Derivatives\n")
        outfile.write(("-" * len("Derivatives")) + "\n")
        get_table(outfile, l4_base.ClassificationLayerTypes.DERIVATIVE, "au")
        outfile.write("\n")

        outfile.write("Modifiers\n")
        outfile.write(("-" * len("Modifiers")) + "\n")
        get_table(outfile, l4_base.ClassificationLayerTypes.MODIFIER, "au")
        outfile.write("\n")

    # AU classification
    with open("Level4_Layer_Values_LW.rst", "w") as outfile:
        outfile.write("Level 4 Classification - Modified version for Living Wales: Layer Values\n")
        outfile.write(("=" * len("Level 4 Classification - Modified version for Living Wales: Layer Values")) + "\n")
        outfile.write("\n")
        outfile.write("These are all the layers used in the implementation of modified LCCS classification scheme for the Living Wales project (lw).\n")
        outfile.write("\n")

        outfile.write("Main\n")
        outfile.write(("-" * len("Main")) + "\n")
        get_table(outfile, l4_base.ClassificationLayerTypes.MAIN, "au")
        outfile.write("\n")

        outfile.write("Derivatives\n")
        outfile.write(("-" * len("Derivatives")) + "\n")
        get_table(outfile, l4_base.ClassificationLayerTypes.DERIVATIVE, "au")
        outfile.write("\n")

        outfile.write("Modifiers\n")
        outfile.write(("-" * len("Modifiers")) + "\n")
        get_table(outfile, l4_base.ClassificationLayerTypes.MODIFIER, "au")
        outfile.write("\n")






