Quick Start Guide
===================

The following provides some guidance on quickly getting started with the code to generate a land cover classification. Please note that this is still a work in progress and more detail will be added.

Installation
-------------

The recommended way to install the software is within a conda environment using the following steps::

    conda create -n le_lccs
    conda activate le_lccs
    conda install --file requirements.txt
    python setup.py install

Open Data Cube
---------------

While it is possible to run the classification outside `Open Data Cube <https://www.opendatacube.org/>`__ (e.g., by passing in GeoTiff files), the system was designed to work best within an Open Data Cube environment.

The Config File
-----------------

Everything needed to run the classification is defined in the config file. For a simple example, using band indices, see `lccs_indices_template.yaml <https://bitbucket.org/au-eoed/livingearth_lccs/src/main/docs/source/example_config/lccs_indices_template.yaml>`__.

Running the classification
----------------------------

Once the config file has been generated the most basic usage to produce a classification is::

    le_lccs_odc.py lccs_indices_template.yaml

The outputs are defined in the config file, typically a GeoTiff containing the data from the classification and a three band GeoTiff with the colour scheme applied for visualisation purposes.

Plugins
---------

The example provided uses simple band indices to which thresholds are used to create the input layers for the LCCS classification. For real world application such a simple approach is unlikely to produce good results, especially when applied to time series data over large areas. Therefore more advanced approaches are likely to be needed.

While these layers can be generated using more advanced methods independently and the le_lccs code used to simply combine them to produce a classification the use of plugins allows generation of new products on the fly and is better suited for large scale processing or routine monitoring. For more details on writing plugins see `here <le_ingest_plugins.html>`__.
