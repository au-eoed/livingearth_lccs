Introduction to LCCS
==========================================================

`Di Gregorio, A (2005). Land Cover Classification System: Classification
concepts and user manual. Software version 2. Food and Agriculture
Organization of the United
Nations <http://www.fao.org/3/y7220e/y7220e00.htm>`__

The Food and Agriculture Organization’s (FAO’s) Land Cover Classification System (LCCS) has two stages, a Dichotomous phase (Level 3) and modular-hierarchical phase (Level 4).

Dichotomous phase (Level 3)
------------------------------

The initial stage is a 3-bit binary (i.e. three dichotomous fields)
classification scheme, giving 8 (i.e. 2^3) classes.

This is coded progressively as ``(A|B)[(1[1-4]|2[5-8])]``, meaning that
a string representation of the Level 1-3 classification has a length of
1-3 characters respectively.

-  **Level 1**: Vegetation (A) or bare (B).
-  **Level 2**: Terrestrial (1) or aquatic (2).
-  **Level 3**: Managed (odd) or natural (even). The value for this
   numeral is ``1 + managed + 2*aquatic + 4*bare``.

Thus, for a complete Level 3 (L3) classification, the 8 allowed codes are
A11, A12, A23, A24, B15, B16, B27, B28. Note that the third character
fully encodes all three levels; the preceding two characters are
redundant.

Modular-hierarchical phase (Level 4)
---------------------------------------

The final stage (Level 4; L4) applies a different classification scheme to each
of the eight L3 classes separately. Each class scheme consists of
roughly a dozen additional categorical classes (L4 classes). Some of the
L4 classes are common among multiple L3 classes
(i.e. LifeformVegCatL4a), and other fields are specific to one class
(i.e. CropcombAgrCatL4a). In total there are 41 `L4
classes <http://www.fao.org/docrep/008/y7220e/y7220e0b.htm#bm11>`__.
Some fields only have minor differences, for example there are several
variants of main life form (i.e. main vegetation) field.

Within each L4 class scheme, there is a superficial organisation among
the fields. The fields are grouped (conceptually), ordered (in a
sequence relating to intended priority of data acquisition), and
categorised (as land cover, environment or discipline-specific).

The L4 classes operate in a hierachy fashion, whereby several classes
must be present for subsequent classes to be populated as useful
landscape descriptors. All of the L4 classes are categorical (i.e. multiple
mutually-incompatible choices), with several having continous input data
(i.e. CanopyhtVegCatL4d). For some fields there are possibilities of
answering with greater or lesser specificity, or in other words, there
is a heirarchical relationship among the possible choices. Usually this
relationship is encoded by appending (i.e. concatenating together)
certain modifier codes to progressively add specificity. Abstractly, the
valid alternatives for a given field can be represented as simple rooted
paths of a directed graph structure.

The L4 codes are usually unique within a single L3 category, with some
exceptions which must be interpreted in relation to preceding codes. For
example, some modifier codes are repeated for multiple parent options
(or can be used without any parent code, to convey a different meaning
again). Similarly, the same height codes are used for multiple levels of
natural vegetation.

The scheme permits only finitely many possible classifications, and LCCS
assigns each distinct classification a single unique integer for
convenient representation in GIS applications.

The Modular-hierarchical phase has been well documented within the
l4_layers_lccs.py code, with docstring for each L4 class describing
modifications from FAO LCCS (optmised for EO implementation). All
dependencies and permitted additional attributes (i.e. concatenating for
useful landscape descriptions) are documented with unit tests performed
on each L4 class.
