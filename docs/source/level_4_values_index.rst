Layer Values - Level 4 Classification
========================================================

The LCCS code provides a full implementation of the FAO LCCS rules, there are also adaptions on this to fit with specific requirements (e.g., existing standards already in use). The values and coresponding LCCS codes for each classification scheme are described in the following pages.

.. toctree::
      :maxdepth: 1

      Level4_Layer_Values
      Level4_Layer_Values_AU
      Level4_Layer_Values_LW


