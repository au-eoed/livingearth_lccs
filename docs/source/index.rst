Living Earth LCCS Documentation
================================

The LivingEarth LCCS code provides an implementation of the Food and Agriculture Organization’s (FAO’s) Land Cover Classification System (LCCS) designed to be applied to Earth Observation (EO) data. It has been developed to generate land cover maps for Australia and `Wales <https://wales.livingearth.online/>`_.
The system takes multiple products and combines these through a series of decision trees to produce all classes described as part of LCCS v2, with some modifications where required to apply to EO data.
The system is capable of ingesting products from OpenDataCube and any raster format which compatible using GDAL (local or remote) with the option to expand via plugins. Existing products can be read directly or products can be created from Analysis Ready Data (ARD), though the use of plugins and `virtual products <https://datacube-core.readthedocs.io/en/latest/dev/api/virtual-products.html>`_.

The Living Earth LCCS code is being developed by `Geoscience Australia <http://www.ga.gov.au/dea>`_, `Aberystwyth University <https://www.aber.ac.uk/en/dges/research/earth-observation-laboratory/>`_ and `Plymouth Marine Laboratory <https://pml.ac.uk/>`_.

.. toctree::
      :maxdepth: 2

      intro
      quick_start
      Variable_Names
      level_4_values_index

Code Documentation
--------------------

.. toctree::
      :maxdepth: 2

      le_ingest
      le_ingest_plugins
      lccs_l3
      lccs_l4
      change
      le_utils
      le_export

