"""
Test for LCCS Level 4 Classification Layers

NOTE: unit test take in level 4 inputs, run classification, then expected is output classes.
Level 4 filters are applied to output classes.

NOTE: Unit tests for a specific class must consider parent dependencies,
as well as direct dependencies of class
e.g. mphenlog_veg_cat_l4m has dependencies on  phenolog_veg_cat_l4a and leaftype_veg_cat_l4a,
    however phenolog_veg_cat_l4a and leaftype_veg_cat_l4a also have dependencies on lifeform_veg_cat_l4a,
    canopyco_veg_cat_l4d, canopyht_veg_cat_l4d and watersea_veg_cat_l4a.
    Therefore mphenlog_veg_cat_l4m unit tests must consider lifeform_veg_cat_l4a, canopyco_veg_cat_l4d,
    canopyht_veg_cat_l4d and watersea_veg_cat_l4a.

"""
import numpy
import xarray

from le_lccs.le_classification import l4_layers_lccs
from le_lccs.le_classification import lccs_l4


def test_LifeformVegCatL4a():
    """
    Test the lifeform_veg_cat_l4a class.

    Dependencies for class
    if lifeform_veg_cat_l4a == 1 | 2 | 5 | 6
        level3 == 111 | 112 | 123 | 124
    if lifeform_veg_cat_l4a == 3 | 4
        level3 == 111 | 112 | 124
    if lifeform_veg_cat_l4a == 7 | 8 | 9
        level3 == 112 | 124

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111, 111, 111, 111, 111, 111,
                          112, 112, 112, 112, 112, 112, 112, 112, 112,
                          123, 123, 123, 123, 123, 123, 123, 123, 123,
                          124, 124, 124, 124, 124, 124, 124, 124, 124])

    # For each level 4 class set up an array with possible values
    lifeform_veg_cat = numpy.array([1, 2, 3, 4, 5, 6, 7, 8, 9,
                                    1, 2, 3, 4, 5, 6, 7, 8, 9,
                                    1, 2, 3, 4, 5, 6, 7, 8, 9,
                                    1, 2, 3, 4, 5, 6, 7, 8, 9])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([1, 2, 3, 4, 5, 6, 0, 0, 0,
                                    1, 2, 3, 4, 5, 6, 7, 8, 9,
                                    1, 2, 0, 0, 5, 6, 0, 0, 0,
                                    1, 2, 3, 4, 5, 6, 7, 8, 9])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["lifeform_veg_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_MlifefrmVegCatL4m():
    """
    Test the mlifefrm_veg_cat_l4m class.

    Dependencies for class
    lifeform_veg_cat_l4a == 5

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111,
                          112, 112,
                          123, 123,
                          124, 124])

    # For each level 4 class set up an array with possible values
    lifeform_veg_cat = numpy.array([5, 5,
                                    5, 5,
                                    5, 5,
                                    5, 5])

    mlifefrm_veg_cat = numpy.array([8, 9,
                                    8, 9,
                                    8, 9,
                                    8, 9])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0,
                                    0, 0,
                                    0, 0,
                                    8, 9])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "mlifefrm_veg_cat" : ({"origin"}, mlifefrm_veg_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["mlifefrm_veg_cat_l4m"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()
  

def test_CanopycoVegCatL4d():
    """
    Test the canopyco_veg_cat_l4d class.

    Dependencies for class
    none

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111, 111,
                          112, 112, 112, 112, 112,
                          123, 123, 123, 123, 123,
                          124, 124, 124, 124, 124,
                          124, 124, 124, 124, 124])

    # For each level 4 class set up an array with possible values
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([65.5, 40.5, 15.5, 4.5, 1.5,
                                    65.5, 40.5, 15.5, 4.5, 1.5,
                                    65.5, 40.5, 15.5, 4.5, 1.5,
                                    65.5, 40.5, 15.5, 4.5, 1.5,
                                    0,    0,    0,    0,   0])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([10, 12, 13, 15, 16,
                                    10, 12, 13, 15, 16,
                                    10, 12, 13, 15, 16,
                                    10, 12, 13, 15, 16,
                                    0,  0,  0,  0,  0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["canopyco_veg_cat_l4d"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()
    

def test_CanopyhtVegCatL4d():
    """
    Test the canopyht_veg_cat_l4d class.

    Dependencies for class
    none

    Permitted additional attributes
    if canopyht_veg_cat_l4d == 5 | 6 | 7
        lifeform_veg_cat_l4a == 0| 1 | 3
    if canopyht_veg_cat_l4d == 8
        lifeform_veg_cat_l4a == 0| 1 | 3 | 4
    if canopyht_veg_cat_l4d == 9 | 10
        lifeform_veg_cat_l4a == 0 | 1 | 4
    if canopyht_veg_cat_l4d == 11
        lifeform_veg_cat_l4a == 2 | 5 | 6
    if canopyht_veg_cat_l4d == 12
        lifeform_veg_cat_l4a == 2 | 5 | 6 | 7 | 9
    if canopyht_veg_cat_l4d == 13
        lifeform_veg_cat_l4a == 2 | 5 | 6 | 7 | 8 | 9
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111, 111, 111, 111, 111, 111,
                          112, 112, 112, 112, 112, 112, 112, 112, 112,
                          123, 123, 123, 123, 123, 123, 123, 123, 123,
                          124, 124, 124, 124, 124, 124, 124, 124, 124])

    # For each level 4 class set up an array with possible values
    lifeform_veg_cat = numpy.array([1, 1, 3, 4, 4, 4, 2, 5, 6,
                                    1, 1, 3, 4, 4, 4, 2, 5, 6,
                                    1, 1, 0, 0, 0, 0, 2, 5, 6,
                                    1, 1, 3, 4, 4, 4, 2, 5, 6])

    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([14.5, 7.5, 5.5, 2.5, 1, 0.3, 1, 0.5, 0.1,
                                    14.5, 7.5, 5.5, 2.5, 1, 0.3, 1, 0.5, 0.1,
                                    14.5, 7.5, 5.5, 2.5, 1, 0.3, 1, 0.5, 0.1,
                                    14.5, 7.5, 5.5, 2.5, 1, 0.3, 1, 0.5, 0.1])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([5, 6, 7, 8, 9, 10, 11, 12, 13,
                                    5, 6, 7, 8, 9, 10, 11, 12, 13,
                                    5, 6, 7, 8, 9, 10, 11, 12, 13,
                                    5, 6, 7, 8, 9, 10, 11, 12, 13])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["canopyht_veg_cat_l4d"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_WaterdayAgrCatL4a():
    """
    Test the waterday_agr_cat_l4a class.

    Dependencies for class
    lifeform_veg_cat_l4a > 0
    canopyco_veg_cat_l4d > 0
    canopyht_veg_cat_l4d > 0

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111,
                          112, 112, 112,
                          123, 123, 123,
                          124, 124, 124])

    # For each level 4 class set up an array with possible values
    lifeform_veg_cat = numpy.array([1, 1, 1,
                                    0, 0, 0,
                                    1, 1, 1,
                                    0, 0, 0])

    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([50.0, 50.0, 50.0,
                                    0.0, 0.0, 0.0,
                                    50.0, 50.0, 50.0,
                                    0.0, 0.0, 0.0])

    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([15.0, 15.0, 15.0,
                                    0.0, 0.0, 0.0,
                                    15.0, 15.0, 15.0,
                                    0.0, 0.0, 0.0])

    waterday_agr_cat = numpy.array([1, 2, 3,
                                    1, 2, 3,
                                    1, 2, 3,
                                    1, 2, 3])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0,
                                    0, 0, 0,
                                    1, 2, 3,
                                    0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["waterday_agr_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_WaterseaVegCatL4a():
    """
    Test the watersea_veg_cat_l4a class.

    Dependencies for class
    lifeform_veg_cat_l4a > 0
    canopyco_veg_cat_l4d > 0
    canopyht_veg_cat_l4d > 0

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at.
    level3 = numpy.array([112, 112, 112, 112, 112,
                          112, 112, 112, 112, 112,
                          124, 124, 124, 124, 124,
                          124, 124, 124, 124, 124])

    # For each level 4 class set up an array with possible values
    lifeform_veg_cat = numpy.array([0, 0, 0, 0, 0,
                                    1, 1, 1, 1, 1,
                                    0, 0, 0, 0, 0,
                                    1, 1, 1, 1, 1])

    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([0.0, 0.0, 0.0, 0.0, 0.0,
                                    50.0, 50.0, 50.0, 50.0, 50.0,
                                    0.0, 0.0, 0.0, 0.0, 0.0,
                                    50.0, 50.0, 50.0, 50.0, 50.0])

    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([0.0, 0.0, 0.0, 0.0, 0.0,
                                    15.0, 15.0, 15.0, 15.0, 15.0,
                                    0.0, 0.0, 0.0, 0.0, 0.0,
                                    15.0, 15.0, 15.0, 15.0, 15.0])

    watersea_veg_cat = numpy.array([1, 2, 3, 4, 5,
                                    1, 2, 3, 4, 5,
                                    1, 2, 3, 4, 5,
                                    1, 2, 3, 4, 5])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0,
                                    1, 2, 3, 4, 5])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["watersea_veg_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_LeaftypeVegCatL4a():
    """
    Test the leaftype_veg_cat_l4a class.

    Dependencies for class
    if leaftype_veg_cat_l4a == 1 | 2
        level3 == 111 | 112
        lifeform_veg_cat_l4a == 1 | 3 | 4
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if leaftype_veg_cat_l4a == 3
        level3 == 112
        lifeform_veg_cat_l4a == 1 | 3 | 4
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if leaftype_veg_cat_l4a == 1 | 2 | 3
        level3 == 124
        lifeform_veg_cat_l4a == 1 | 3 | 4
        watersea_veg_cat_l4a > 0

    if leaftype_veg_cat_l4a == 4
        level3 == 111 | 112
        lifeform_veg_cat_l4a == 2 | 5 | 6 | 7 | 8 | 9
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if leaftype_veg_cat_l4a == 4
        level3 == 124
        lifeform_veg_cat_l4a == 2 | 5 | 6 | 7 | 8 | 9
        watersea_veg_cat_l4a > 0

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111,
                          112, 112, 112, 112,
                          123, 123, 123, 123,
                          124, 124, 124, 124])
    
    # For each level 4 class set up an array with possible values
    lifeform_veg_cat = numpy.array([1, 3, 4, 2,
                                    4, 3, 1, 2,
                                    1, 3, 4, 2,
                                    4, 3, 1, 2])

    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([10.0, 30.0, 40.0, 50.0,
                                    10.0, 30.0, 40.0, 50.0,
                                    10.0, 30.0, 40.0, 50.0,
                                    10.0, 30.0, 40.0, 50.0])

    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([15.0, 30.0, 2.0, 1.0,
                                    2.0, 30.0, 15.0, 1.0,
                                    15.0, 30.0, 2.0, 1.0,
                                    2.0, 30.0, 15.0, 1.0])

    watersea_veg_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 1, 2, 3])

    leaftype_veg_cat = numpy.array([1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([1, 2, 0, 4,
                                    1, 2, 3, 4,
                                    0, 0, 0, 0,
                                    0, 2, 3, 4])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["leaftype_veg_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_PhenologVegCatL4a():
    """
    Test the phenolog_veg_cat_l4a class.

    Dependencies for class
    if phenolog_veg_cat_l4a == 1 | 2 | 3
        level3 == 111 | 112
        lifeform_veg_cat_l4a == 1 | 3 | 4
        leaftype_veg_cat_l4a == 1 | 2
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if phenolog_veg_cat_l4a == 1 | 2 | 3
        level3 == 124
        lifeform_veg_cat_l4a == 1 | 3 | 4
        leaftype_veg_cat_l4a == 1 | 2
        watersea_veg_cat_l4a > 0
    if phenolog_veg_cat_l4a == 5
        level3 == 112
        lifeform_veg_cat_l4a == 2 | 5 | 6
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if phenolog_veg_cat_l4a == 5
        level3 == 124
        lifeform_veg_cat_l4a == 2 | 5 | 6
        watersea_veg_cat_l4a > 0

    if phenolog_veg_cat_l4a == 6
        level3 == 111 | 112
        lifeform_veg_cat_l4a == 1 | 3 | 4
        leaftype_veg_cat_l4a == 0 | 3
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if phenolog_veg_cat_l4a == 6
        level3 == 124
        lifeform_veg_cat_l4a == 1 | 3 | 4
        leaftype_veg_cat_l4a == 0 | 3
        watersea_veg_cat_l4a > 0
    if phenolog_veg_cat_l4a == 6
        level3 == 111 | 112
        lifeform_veg_cat_l4a == 2 | 5 | 6 | 7 | 8 | 9
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if phenolog_veg_cat_l4a == 6
        level3 == 124
        lifeform_veg_cat_l4a == 2 | 5 | 6 | 7 | 8 | 9
        watersea_veg_cat_l4a > 0
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111, 111,
                          112, 112, 112, 112, 112,
                          123, 123, 123, 123, 123,
                          124, 124, 124, 124, 124])

    # For each level 4 class set up an array with possible values
    lifeform_veg_cat = numpy.array([2, 2, 3, 5, 7,
                                    2, 2, 3, 5, 7,
                                    2, 2, 3, 5, 7,
                                    2, 2, 3, 5, 7])

    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([10.0, 30.0, 40.0, 50.0, 60.0,
                                    10.0, 30.0, 40.0, 50.0, 60.0,
                                    10.0, 30.0, 40.0, 50.0, 60.0,
                                    10.0, 30.0, 40.0, 50.0, 60.0])

    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([1.0, 2.0, 3.0, 0.3, 0.1,
                                    1.0, 2.0, 3.0, 0.3, 0.1,
                                    1.0, 2.0, 3.0, 0.3, 0.1,
                                    1.0, 2.0, 3.0, 0.3, 0.1])

    watersea_veg_cat = numpy.array([0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0,
                                    1, 2, 3, 4, 5])

    leaftype_veg_cat = numpy.array([3, 2, 1, 0, 0,
                                    3, 2, 1, 0, 0,
                                    3, 2, 1, 0, 0,
                                    3, 2, 1, 0, 0])

    phenolog_veg_cat = numpy.array([1, 2, 3, 5, 6,
                                    1, 2, 3, 5, 6,
                                    1, 2, 3, 5, 6,
                                    1, 2, 3, 5, 6])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 3, 0, 0,
                                    0, 0, 3, 5, 6,
                                    0, 0, 0, 0, 0,
                                    0, 0, 3, 5, 6])

    # # Set up an array of expected classes given inputs
    # expected_classes = numpy.array([0, 0, 3, 0, 0,
    #                                 0, 0, 3, 5, 6,
    #                                 0, 0, 0, 0, 0, dw
    #                                 0, 0, 3, 5, 6])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3": ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["phenolog_veg_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_MphenlogVegCatL4m():
    """
    Test the mphenlog_veg_cat_l4m class.

    Dependencies for class
    if mphenlog_veg_cat_l4m == 4
        phenolog_veg_cat_l4a == 1 | 2
        leaftype_veg_cat_l4a == 1
    if mphenlog_veg_cat_l4m == 6 | 7
        phenolog_veg_cat_l4a == 5

    Permitted additional attributes
    all
    if mphenlog_veg_cat_l4m == 6 | 7
        lifeform_veg_cat_l4a == 2 | 5 | 6
        phenolog_veg_cat_l4a == 6
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([112, 112, 112,
                          112, 112, 112,
                          124, 124, 124,
                          124, 124, 124])

    # For each level 4 class set up an array with possible values
    # lifeform_veg_cat is a parent dependency
    lifeform_veg_cat = numpy.array([1, 1, 1,
                                    3, 3, 3,
                                    4, 4, 4,
                                    5, 5, 5])

    # canopyco_veg_con is a parent dependency
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([10.0, 20.0, 30.0,
                                    10.0, 20.0, 30.0,
                                    10.0, 20.0, 30.0,
                                    10.0, 20.0, 30.0])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([10.0, 10.0, 10.0,
                                    10.0, 10.0, 10.0,
                                    0.5, 0.5, 0.5,
                                    0.5, 0.5, 0.5])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0,
                                    0, 0, 0,
                                    1, 2, 3,
                                    1, 2, 3])

    phenolog_veg_cat = numpy.array([1, 1, 1,
                                    2, 2, 2,
                                    3, 3, 3,
                                    5, 5, 5])

    leaftype_veg_cat = numpy.array([0, 0, 0,
                                    1, 1, 1,
                                    2, 2, 2,
                                    3, 3, 3])

    mphenlog_veg_cat = numpy.array([4, 6, 7,
                                    4, 6, 7,
                                    4, 6, 7,
                                    4, 6, 7])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0,
                                    4, 0, 0,
                                    0, 0, 0,
                                    0, 6, 7])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3": ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "mphenlog_veg_cat" : ({"origin"}, mphenlog_veg_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["mphenlog_veg_cat_l4m"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_UrbanvegUrbCatL4a():
    """
    Test the urbanveg_urb_cat_l4a class.

    Dependencies for class
    if urbanveg_urb_cat_l4a == 6
        lifeform_veg_cat_l4a == 1 | 2 | 3 | 4 | 5 | 6
    if urbanveg_urb_cat_l4a == 11
        lifeform_veg_cat_l4a == 1 | 3
        canopyht_veg_cat_l4d == 0 | 5 | 6 | 7 | 8
    if urbanveg_urb_cat_l4a == 12
        lifeform_veg_cat_l4a == 1 | 3 | 4
    if urbanveg_urb_cat_l4a == 13
        lifeform_veg_cat_l4a == 2 | 6
        canopyht_veg_cat_l4d == 0

    Permitted additional attributes (in this case to stop urbanveg_urb_cat_l4a being attributed
    to other 111 classes)
    leaftype_veg_cat_l4a == 0
    phenolog_veg_cat_l4a == 0
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111,
                          111, 111, 111, 111,
                          111, 111, 111, 111,
                          111, 111, 111, 111])

    # For each level 4 class set up an array with possible values
    lifeform_veg_cat = numpy.array([1, 3, 3, 2,
                                    1, 3, 3, 2,
                                    1, 3, 3, 2,
                                    1, 3, 3, 2])

    # canopyco_veg_con is a parent dependency
    canopyco_veg_con = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # canopyht_veg_con is a parent dependency
    canopyht_veg_con = numpy.array([20, 20, 20, 20,
                                    0.5, 0.5, 0.5, 0.5,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    leaftype_veg_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    phenolog_veg_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    urbanveg_urb_cat = numpy.array([6, 11, 12, 13,
                                    6, 11, 12, 13,
                                    6, 11, 12, 13,
                                    6, 11, 12, 13])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([6, 11, 12, 13,
                                    6, 11, 12, 0,
                                    6, 11, 12, 13,
                                    6, 11, 12, 13])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "urbanveg_urb_cat" : ({"origin"}, urbanveg_urb_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["urbanveg_urb_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_SpatdistVegCatL4a():
    """
    Test the spatdist_veg_cat_l4a class.

    Dependencies for class
    if spatdist_veg_cat_l4a = 1
        level3 == 111 | 112
        canopyco_veg_cat_l4d == 10 | 12 | 13
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatdist_veg_cat_l4a = 1
        level3 == 123
        canopyco_veg_cat_l4d == 10 | 12 | 13
        waterday_agr_cat_l4a > 0
    if spatdist_veg_cat_l4a = 2 | 4 | 5
        level3 == 112
        canopyco_veg_cat_l4d == 10 | 12 | 13
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatdist_veg_cat_l4a = 3
        level3 == 112
        canopyco_veg_cat_l4d == 15
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatdist_veg_cat_l4a = 6 | 7
        level3 == 111
        canopyco_veg_cat_l4d == 16
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatdist_veg_cat_l4a = 6 | 7
        level3 == 123
        canopyco_veg_cat_l4d == 16
        waterday_agr_cat_l4a > 0

    if spatdist_veg_cat_l4a = 8
        level3 == 111
        canopyco_veg_cat_l4d == 10 | 12 | 13 | 15
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatdist_veg_cat_l4a = 8
        level3 == 112
        canopyco_veg_cat_l4d == 16
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatdist_veg_cat_l4a = 8
        level3 == 123
        canopyco_veg_cat_l4d == 10 | 12 | 13 | 15
        waterday_agr_cat_l4a > 0

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111, 111, 111, 111, 111,
                          112, 112, 112, 112, 112, 112, 112, 112,
                          123, 123, 123, 123, 123, 123, 123, 123,
                          124, 124, 124, 124, 124, 124, 124, 124])

    # For each level 4 class set up an array with possible values
    # lifeform_veg_cat is a parent dependency
    lifeform_veg_cat = numpy.array([1, 1, 1, 1, 1, 1, 1, 1,
                                    1, 1, 1, 1, 1, 1, 1, 1,
                                    1, 1, 1, 1, 1, 1, 1, 1,
                                    1, 1, 1, 1, 1, 1, 1, 1])

    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([70.0, 70.0, 10.0, 70.0, 70.0, 3.0, 3.0, 10.0,
                                    70.0, 70.0, 10.0, 70.0, 70.0, 3.0, 3.0, 10.0,
                                    70.0, 70.0, 10.0, 70.0, 70.0, 3.0, 3.0, 10.0,
                                    70.0, 70.0, 10.0, 70.0, 70.0, 3.0, 3.0, 10.0])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([15.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0,
                                    15.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0,
                                    15.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0,
                                    15.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0])

    waterday_agr_cat = numpy.array([0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0,
                                    1, 2, 1, 2, 1, 2, 1, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0,
                                    1, 2, 1, 2, 1, 2, 1, 0])

    leaftype_veg_cat = numpy.array([1, 1, 1, 1, 1, 1, 1, 1,
                                    1, 1, 1, 1, 1, 1, 1, 1,
                                    0, 0, 0, 0, 0, 0, 0, 0,
                                    1, 1, 1, 1, 1, 1, 1, 1])

    phenolog_veg_cat = numpy.array([1, 1, 1, 1, 1, 1, 1, 1,
                                    1, 1, 1, 1, 1, 1, 1, 1,
                                    0, 0, 0, 0, 0, 0, 0, 0,
                                    1, 1, 1, 1, 1, 1, 1, 1])

    spatdist_veg_cat = numpy.array([1, 2, 3, 4, 5, 6, 7, 8,
                                    1, 2, 3, 4, 5, 6, 7, 8,
                                    1, 2, 3, 4, 5, 6, 7, 8,
                                    1, 2, 3, 4, 5, 6, 7, 8])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([1, 0, 0, 0, 0, 6, 7, 8,
                                    1, 2, 3, 4, 5, 0, 0, 0,
                                    1, 0, 0, 0, 0, 6, 7, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "spatdist_veg_cat" : ({"origin"}, spatdist_veg_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["spatdist_veg_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_Strat2ndVegCatL4a():
    """
    Test the strat2nd_veg_cat_l4a class.

    Dependencies for class
    if strat2nd_veg_cat_l4a == 1 | 2
        level3 == 112
        spatdist_veg_cat_l4a > 0
    if strat2nd_veg_cat_l4a == 1 | 2
        level3 == 124
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0

    Permitted additional attributes
    lifeform_veg_cat_l4a == 1 | 3 | 4
    canopyht_veg_cat_l4d == 5 | 6 | 7 | 8 | 9
    """

    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111,
                          112, 112,
                          123, 123,
                          124, 124])

    # For each level 4 class set up an array with possible values
    # lifeform_veg_cat is a parent dependency
    lifeform_veg_cat = numpy.array([1, 1,
                                    1, 1,
                                    1, 1,
                                    1, 1])

    # canopyco_veg_con is a parent dependency
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([100.0, 70.0,
                                    100.0, 70.0,
                                    100.0, 70.0,
                                    100.0, 70.0])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([20.0, 30.0,
                                    20.0, 30.0,
                                    20.0, 30.0,
                                    20.0, 30.0])

    # waterday_agr_cat is a parent dependency
    waterday_agr_cat = numpy.array([0, 0,
                                    0, 0,
                                    1, 1,
                                    0, 0])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0,
                                    0, 0,
                                    0, 0,
                                    1, 1])

    leaftype_veg_cat = numpy.array([1, 1,
                                    1, 1,
                                    0, 0,
                                    1, 1])

    phenolog_veg_cat = numpy.array([1, 1,
                                    1, 1,
                                    0, 0,
                                    1, 1])

    spatdist_veg_cat = numpy.array([1, 1,
                                    1, 1,
                                    1, 1,
                                    0, 0])

    strat2nd_veg_cat = numpy.array([0, 0,
                                    1, 2,
                                    0, 0,
                                    1, 2])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0,
                                    1, 2,
                                    0, 0,
                                    1, 2])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3": ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "spatdist_veg_cat" : ({"origin"}, spatdist_veg_cat),
         "strat2nd_veg_cat" : ({"origin"}, strat2nd_veg_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["strat2nd_veg_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_Lifef2ndVegCatL4a():
    """
    Test the lifef2nd_veg_cat_l4a class.

    Dependencies for class
    strat2nd_veg_cat_l4a == 2

    Permitted additional attributes
    if lifef2nd_veg_cat_l4a == 3
        lifeform_veg_cat_l4a == 1
    if lifef2nd_veg_cat_l4a == 4
        lifeform_veg_cat_l4a == 1 | 3
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8
    if lifef2nd_veg_cat_l4a == 5 | 6
        lifeform_veg_cat_l4a == 1 | 3 | 4
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([112, 112, 112, 112,
                          112, 112, 112, 112,
                          124, 124, 124, 124,
                          124, 124, 124, 124])

    # For each level 4 class set up an array with possible values
    lifeform_veg_cat = numpy.array([1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2])

    # canopyco_veg_con is a parent dependency
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([100.0, 70.0, 100.0, 70.0,
                                    100.0, 70.0, 100.0, 70.0,
                                    100.0, 70.0, 100.0, 70.0,
                                    100.0, 70.0, 100.0, 70.0])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([20.0, 20.0, 20.0, 20.0,
                                    1.0, 1.0, 1.0, 1.0,
                                    15.0, 15.0, 15.0, 15.0,
                                    0.5, 0.5, 0.5, 0.5])

    # leaftype_veg_cat is a parent dependency
    leaftype_veg_cat = numpy.array([1, 1, 1, 1,
                                    0, 0, 0, 0,
                                    2, 2, 2, 2,
                                    0, 0, 0, 0])

    # phenolog_veg_cat is a parent dependency
    phenolog_veg_cat = numpy.array([1, 1, 1, 1,
                                    0, 0, 0, 0,
                                    3, 3, 3, 3,
                                    0, 0, 0, 0])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2])

    # spatdist_veg_cat is a parent dependency
    spatdist_veg_cat = numpy.array([1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # waterday_agr_cat is a parent dependency
    waterday_agr_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    strat2nd_veg_cat = numpy.array([2, 2, 2, 2,
                                    2, 2, 2, 2,
                                    2, 2, 2, 2,
                                    2, 2, 2, 2])

    lifef2nd_veg_cat = numpy.array([3, 4, 5, 6,
                                    3, 4, 5, 6,
                                    3, 4, 5, 6,
                                    3, 4, 5, 6])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([3, 4, 5, 6,
                                    0, 0, 0, 0,
                                    3, 4, 5, 6,
                                    0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3": ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "spatdist_veg_cat" : ({"origin"}, spatdist_veg_cat),
         "strat2nd_veg_cat" : ({"origin"}, strat2nd_veg_cat),
         "lifef2nd_veg_cat" : ({"origin"}, lifef2nd_veg_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["lifef2nd_veg_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_Cover2ndVegCatL4a():
    """
    Test the cover2nd_veg_cat_l4d class.

    Dependencies for class
    strat2nd_veg_cat_l4a == 2

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([112, 112, 112,
                          112, 112, 112,
                          124, 124, 124,
                          124, 124, 124])

    # For each level 4 class set up an array with possible values
    # lifeform_veg_cat is a parent dependency
    lifeform_veg_cat = numpy.array([1, 1, 1,
                                    2, 2, 2,
                                    1, 1, 1,
                                    2, 2, 2])

    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([100, 65.5, 40.5,
                                    100, 65.5, 40.5,
                                    100, 65.5, 40.5,
                                    100, 65.5, 40.5])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([20.0, 20.0, 20.0,
                                    1.0, 1.0, 1.0,
                                    15.0, 15.0, 15.0,
                                    0.5, 0.5, 0.5])

    # waterday_agr_cat is a parent dependency
    waterday_agr_cat = numpy.array([0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0,
                                    0, 0, 0,
                                    3, 3, 3,
                                    4, 4, 4])

    # leaftype_veg_cat is a parent dependency
    leaftype_veg_cat = numpy.array([1, 1, 1,
                                    0, 0, 0,
                                    2, 2, 2,
                                    0, 0, 0])

    # phenolog_veg_cat is a parent dependency
    phenolog_veg_cat = numpy.array([1, 1, 1,
                                    0, 0, 0,
                                    3, 3, 3,
                                    0, 0, 0])

    # spatdist_veg_cat is a parent dependency
    spatdist_veg_cat = numpy.array([1, 1, 1,
                                    2, 2, 2,
                                    0, 0, 0,
                                    0, 0, 0])

    strat2nd_veg_cat = numpy.array([2, 2, 2,
                                    2, 2, 2,
                                    2, 2, 2,
                                    2, 2, 2])

    # cover2nd_veg_con inputs must be continuous
    cover2nd_veg_con = numpy.array([70.0, 40.0, 10.0,
                                    70.0, 40.0, 10.0,
                                    70.0, 40.0, 10.0,
                                    70.0, 40.0, 10.0])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([8, 9, 10,
                                    0, 0, 0,
                                    8, 9, 10,
                                    0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3": ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "spatdist_veg_cat" : ({"origin"}, spatdist_veg_cat),
         "strat2nd_veg_cat" : ({"origin"}, strat2nd_veg_cat),
         "cover2nd_veg_con" : ({"origin"}, cover2nd_veg_con)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["cover2nd_veg_cat_l4d"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_Heigh2ndVegCatL4d():
    """
    Test the heigh2nd_veg_cat_l4d class.

    Dependencies for class
    strat2nd_veg_cat_l4a == 2

    Permitted additional attributes
    if heigh2nd_veg_cat_l4d == 5 | 6
        canopyht_veg_cat_l4d == 5
        lifef2nd_veg_cat_l4a == 0 | 3 | 4
    if heigh2nd_veg_cat_l4d == 7
        canopyht_veg_cat_l4d == 5 | 6
        lifef2nd_veg_cat_l4a == 0 | 3 | 4
    if heigh2nd_veg_cat_l4d == 8
        canopyht_veg_cat_l4d == 5 | 6 | 7
        lifef2nd_veg_cat_l4a == 0 | 3 | 4 | 5
    if heigh2nd_veg_cat_l4d == 9
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8
        lifef2nd_veg_cat_l4a == 0 | 3 | 5
    if heigh2nd_veg_cat_l4d == 10
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8 | 9
        lifef2nd_veg_cat_l4a == 0 | 3 | 5
    if heigh2nd_veg_cat_l4d == 11
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8 | 9
        lifef2nd_veg_cat_l4a == 6
    if heigh2nd_veg_cat_l4d == 12
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8 | 9 | 11 | 12
        lifef2nd_veg_cat_l4a == 6
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([112, 112, 112, 112, 112, 112, 112, 112,
                          112, 112, 112, 112, 112, 112, 112, 112,
                          124, 124, 124, 124, 124, 124, 124, 124,
                          124, 124, 124, 124, 124, 124, 124, 124])

    # For each level 4 class set up an array with possible values
    # lifeform_veg_cat is a parent dependency
    lifeform_veg_cat = numpy.array([1, 1, 1, 1, 1, 1, 1, 1,
                                    3, 3, 3, 3, 3, 3, 3, 3,
                                    1, 1, 1, 1, 1, 1, 1, 1,
                                    3, 3, 3, 3, 3, 3, 3, 3])

    # canopyco_veg_con is a parent dependency
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([100, 65.5, 40.5, 100, 65.5, 40.5, 100, 65.5,
                                    100, 65.5, 40.5, 100, 65.5, 40.5, 100, 65.5,
                                    100, 65.5, 40.5, 100, 65.5, 40.5, 100, 65.5,
                                    100, 65.5, 40.5, 100, 65.5, 40.5, 100, 65.5])

    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([30.0, 15.0, 8.0, 7.0, 3.0, 10.0, 5.0, 20.0,
                                    30.0, 15.0, 8.0, 7.0, 3.0, 10.0, 5.0, 20.0,
                                    30.0, 15.0, 8.0, 7.0, 3.0, 10.0, 5.0, 20.0,
                                    30.0, 15.0, 8.0, 7.0, 3.0, 10.0, 5.0, 20.0])

    # waterday_agr_cat is a parent dependency
    waterday_agr_cat = numpy.array([0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0,
                                    3, 3, 3, 3, 3, 3, 3, 3,
                                    4, 4, 4, 4, 4, 4, 4, 4])

    # leaftype_veg_cat is a parent dependency
    leaftype_veg_cat = numpy.array([1, 1, 1, 1, 1, 1, 1, 1,
                                    1, 1, 1, 1, 1, 1, 1, 1,
                                    2, 2, 2, 2, 2, 2, 2, 2,
                                    2, 2, 2, 2, 2, 2, 2, 2])

    # phenolog_veg_cat is a parent dependency
    phenolog_veg_cat = numpy.array([1, 1, 1, 1, 1, 1, 1, 1,
                                    1, 1, 1, 1, 1, 1, 1, 1,
                                    3, 3, 3, 3, 3, 3, 3, 3,
                                    3, 3, 3, 3, 3, 3, 3, 3])

    # spatdist_veg_cat is a parent dependency
    spatdist_veg_cat = numpy.array([1, 1, 1, 1, 1, 1, 1, 1,
                                    2, 2, 2, 2, 2, 2, 2, 2,
                                    0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0])

    strat2nd_veg_cat = numpy.array([2, 2, 2, 2, 2, 2, 2, 2,
                                    2, 2, 2, 2, 2, 2, 2, 2,
                                    2, 2, 2, 2, 2, 2, 2, 2,
                                    2, 2, 2, 2, 2, 2, 2, 2])

    lifef2nd_veg_cat = numpy.array([3, 3, 3, 3, 3, 3, 3, 3,
                                    4, 4, 4, 4, 4, 4, 4, 4,
                                    5, 5, 5, 5, 5, 5, 5, 5,
                                    6, 6, 6, 6, 6, 6, 6, 6])

    # heigh2nd_veg_con inputs must be continuous
    heigh2nd_veg_con = numpy.array([17, 10, 4, 2.5, 1, 0.2, 1.5, 0.2,
                                    17, 10, 4, 2.5, 1, 0.2, 1.5, 0.2,
                                    17, 10, 4, 2.5, 1, 0.2, 1.5, 0.2,
                                    17, 10, 4, 2.5, 1, 0.2, 1.5, 0.2])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([5, 6, 8, 8, 9, 10, 9, 10,
                                    5, 6, 8, 8, 0, 0, 0, 0,
                                    0, 0, 8, 8, 9, 10, 9, 10,
                                    0, 0, 0, 11, 11, 12, 11, 12])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3": ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "spatdist_veg_cat" : ({"origin"}, spatdist_veg_cat),
         "strat2nd_veg_cat" : ({"origin"}, strat2nd_veg_cat),
         "lifef2nd_veg_cat" : ({"origin"}, lifef2nd_veg_cat),
         "heigh2nd_veg_con" : ({"origin"}, heigh2nd_veg_con)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["heigh2nd_veg_cat_l4d"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_SpatsizeAgrCatL4a():
    """
    Test the spatsize_agr_cat_l4a class.

    Dependencies for class
    if spatsize_agr_cat_l4a = 1 | 2 | 3 | 4
        level3 == 111
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatsize_agr_cat_l4a = 1 | 2 | 3 | 4
        level3 == 123
        waterday_agr_cat_l4a > 0

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111,
                          111, 111, 111, 111,
                          123, 123, 123, 123,
                          123, 123, 123, 123])

    # For each level 4 class set up an array with possible values
    # lifeform_veg_cat is a parent dependency
    lifeform_veg_cat = numpy.array([1, 1, 1, 1,
                                    3, 3, 3, 3,
                                    1, 1, 1, 1,
                                    3, 3, 3, 3])

    # canopyco_veg_con is a parent dependency
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([100, 65.5, 40.5, 100,
                                    100, 65.5, 40.5, 100,
                                    100, 65.5, 40.5, 100,
                                    100, 65.5, 0.0, 0.0])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([30.0, 15.0, 8.0, 7.0,
                                    30.0, 15.0, 8.0, 7.0,
                                    30.0, 15.0, 8.0, 7.0,
                                    0.0, 0.0, 8.0, 7.0])

    waterday_agr_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    leaftype_veg_cat = numpy.array([1, 1, 1, 1,
                                    0, 0, 1, 1,
                                    2, 2, 2, 2,
                                    2, 2, 2, 2])

    phenolog_veg_cat = numpy.array([1, 1, 1, 1,
                                    1, 1, 0, 0,
                                    3, 3, 3, 3,
                                    3, 3, 3, 3])

    spatsize_agr_cat = numpy.array([1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([1, 2, 3, 4,
                                    0, 0, 0, 0,
                                    1, 2, 3, 4,
                                    0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "spatsize_agr_cat" : ({"origin"}, spatsize_agr_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["spatsize_agr_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_CropcombAgrCatL4a():
    """
    Test the cropcomb_agr_cat_l4a class.

    Dependencies for class
    spatdist_veg_cat_l4a > 0
    spatsize_agr_cat_l4a > 0

    Permitted additional attributes
    if cropcomb_agr_cat_l4a == 1 | 2
        lifeform_veg_cat_l4a == 1 | 2 | 3 | 4 | 5 | 6
    if cropcomb_agr_cat_l4a == 3 | 4
        lifeform_veg_cat_l4a == 1 | 2 | 3 | 4
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111,
                          112, 112, 112, 112,
                          111, 111, 111, 111,
                          112, 112, 112, 112])

    # For each level 4 class set up an array with possible values
    # lifeform_veg_cat is a parent dependency
    lifeform_veg_cat = numpy.array([1, 1, 1, 1,
                                    3, 3, 3, 3,
                                    1, 1, 1, 1,
                                    3, 3, 3, 3])

    # canopyco_veg_con is a parent dependency
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([100, 65.5, 3.0, 1.0,
                                    100, 65.5, 3.0, 1.0,
                                    100, 65.5, 3.0, 1.0,
                                    100, 65.5, 3.0, 1.0])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([30.0, 15.0, 8.0, 7.0,
                                    30.0, 15.0, 8.0, 7.0,
                                    30.0, 15.0, 8.0, 7.0,
                                    30.0, 15.0, 8.0, 7.0])

    # waterday_agr_cat is a parent dependency
    waterday_agr_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # leaftype_veg_cat is a parent dependency
    leaftype_veg_cat = numpy.array([1, 1, 1, 1,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    2, 2, 2, 2])

    # phenolog_veg_cat is a parent dependency
    phenolog_veg_cat = numpy.array([1, 1, 1, 1,
                                    1, 1, 1, 1,
                                    3, 3, 3, 3,
                                    3, 3, 3, 3])

    spatdist_veg_cat = numpy.array([1, 2, 6, 7,
                                    1, 2, 6, 7,
                                    1, 2, 6, 7,
                                    1, 2, 6, 7])

    spatsize_agr_cat = numpy.array([1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4])

    cropcomb_agr_cat = numpy.array([1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([1, 0, 3, 4,
                                    0, 0, 0, 0,
                                    1, 0, 3, 4,
                                    0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "spatdist_veg_cat" : ({"origin"}, spatdist_veg_cat),
         "spatsize_agr_cat" : ({"origin"}, spatsize_agr_cat),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "cropcomb_agr_cat" : ({"origin"}, cropcomb_agr_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["cropcomb_agr_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_Croplfc2AgrCatL4a():
    """
    Test the croplfc2_agr_cat_l4a class.

    Dependencies for class
    cropcomb_agr_cat_l4a == 3 | 4

    Permitted additional attributes
    if croplfc2_agr_cat_l4a == 5
        lifeform_veg_cat_l4a == 1 | 3
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8
    if croplfc2_agr_cat_l4a == 6
        lifeform_veg_cat_l4a == 1 | 3 | 4
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8 | 9
    if croplfc2_agr_cat_l4a == 7 | 8
        lifeform_veg_cat_l4a == 1 | 2 | 3 | 4
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8 | 9 | 11 | 12
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111,
                          111, 111, 111, 111,
                          112, 112, 112, 112,
                          112, 112, 112, 112])

    # For each level 4 class set up an array with possible values
    # lifeform_veg_cat is a parent dependency
    lifeform_veg_cat = numpy.array([1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2])

    # canopyco_veg_con is a parent dependency
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([100.0, 100.0, 50.0, 50.0,
                                    100.0, 100.0, 50.0, 50.0,
                                    100.0, 100.0, 50.0, 50.0,
                                    100.0, 100.0, 50.0, 50.0])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([30.0, 15.0, 30.0, 15.0,
                                    2.0, 1.0, 2.0, 1.0,
                                    30.0, 15.0, 30.0, 15.0,
                                    2.0, 1.0, 2.0, 1.0])

    # waterday_agr_cat is a parent dependency
    waterday_agr_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # leaftype_veg_cat is a parent dependency
    leaftype_veg_cat = numpy.array([1, 1, 1, 1,
                                    4, 4, 4, 4,
                                    1, 1, 1, 1,
                                    4, 4, 4, 4])

    # phenolog_veg_cat is a parent dependency
    phenolog_veg_cat = numpy.array([1, 1, 1, 1,
                                    6, 6, 6, 6,
                                    1, 1, 1, 1,
                                    5, 5, 5, 5])

    # spatdist_veg_cat is a parent dependency
    spatdist_veg_cat = numpy.array([1, 1, 1, 1,
                                    1, 1, 1, 1,
                                    1, 1, 1, 1,
                                    1, 1, 1, 1])

    # spatsize_agr_cat is a parent dependency
    spatsize_agr_cat = numpy.array([1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    cropcomb_agr_cat = numpy.array([3, 3, 4, 4,
                                    3, 3, 4, 4,
                                    3, 3, 4, 4,
                                    3, 3, 4, 4])

    croplfc2_agr_cat = numpy.array([5, 6, 7, 8,
                                    5, 6, 7, 8,
                                    5, 6, 7, 8,
                                    5, 6, 7, 8,])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([5, 6, 7, 8,
                                    0, 0, 7, 8,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "spatdist_veg_cat" : ({"origin"}, spatdist_veg_cat),
         "spatsize_agr_cat" : ({"origin"}, spatsize_agr_cat),
         "cropcomb_agr_cat" : ({"origin"}, cropcomb_agr_cat),
         "croplfc2_agr_cat" : ({"origin"}, croplfc2_agr_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["croplfc2_agr_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_Croplfc3AgrCatL4a():
    """
    Test the croplfc3_agr_cat_l4a class.

    Dependencies for class
    cropcomb_agr_cat_l4a == 4

    Permitted additional attributes
    if croplfc3_agr_cat_l4a == 13
        croplfc2_agr_cat_l4a == 5
    if croplfc3_agr_cat_l4a == 14
        croplfc2_agr_cat_l4a == 5 | 6
    if croplfc3_agr_cat_l4a == 15 | 16
        croplfc2_agr_cat_l4a == 7 | 8
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111,
                          111, 111, 111, 111,
                          112, 112, 112, 112,
                          112, 112, 112, 112])

    # For each level 4 class set up an array with possible values
    # lifeform_veg_cat is a parent dependency
    lifeform_veg_cat = numpy.array([1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2])

    # canopyco_veg_con is a parent dependency
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([100.0, 100.0, 50.0, 50.0,
                                    100.0, 100.0, 50.0, 50.0,
                                    100.0, 100.0, 50.0, 50.0,
                                    100.0, 100.0, 50.0, 50.0])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([30.0, 15.0, 30.0, 15.0,
                                    2.0, 1.0, 2.0, 1.0,
                                    30.0, 15.0, 30.0, 15.0,
                                    2.0, 1.0, 2.0, 1.0])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # waterday_agr_cat is a parent dependency
    waterday_agr_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # leaftype_veg_cat is a parent dependency
    leaftype_veg_cat = numpy.array([1, 1, 1, 1,
                                    4, 4, 4, 4,
                                    1, 1, 1, 1,
                                    4, 4, 4, 4])

    # phenolog_veg_cat is a parent dependency
    phenolog_veg_cat = numpy.array([1, 1, 1, 1,
                                    6, 6, 6, 6,
                                    1, 1, 1, 1,
                                    5, 5, 5, 5])

    # spatdist_veg_cat is a parent dependency
    spatdist_veg_cat = numpy.array([1, 1, 1, 1,
                                    1, 1, 1, 1,
                                    1, 1, 1, 1,
                                    1, 1, 1, 1])

    # spatsize_agr_cat is a parent dependency
    spatsize_agr_cat = numpy.array([1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])


    cropcomb_agr_cat = numpy.array([4, 4, 4, 4,
                                    4, 4, 4, 4,
                                    4, 4, 4, 4,
                                    4, 4, 4, 4])

    croplfc2_agr_cat = numpy.array([5, 6, 7, 8,
                                    5, 6, 7, 8,
                                    5, 6, 7, 8,
                                    5, 6, 7, 8])

    croplfc3_agr_cat = numpy.array([13, 14, 15, 16,
                                    13, 14, 15, 16,
                                    13, 14, 15, 16,
                                    13, 14, 15, 16])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([13, 14, 15, 16,
                                    0, 0, 15, 16,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "spatdist_veg_cat" : ({"origin"}, spatdist_veg_cat),
         "spatsize_agr_cat" : ({"origin"}, spatsize_agr_cat),
         "cropcomb_agr_cat" : ({"origin"}, cropcomb_agr_cat),
         "croplfc2_agr_cat" : ({"origin"}, croplfc2_agr_cat),
         "croplfc3_agr_cat" : ({"origin"}, croplfc3_agr_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["croplfc3_agr_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_CropseqtAgrCatL4a():
    """
    Test the cropseqt_agr_cat_l4a class.

    Dependencies for class
    cropcomb_agr_cat_l4a == 2 | 3 | 4

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111,
                          112, 112, 112, 112,
                          111, 111, 111, 111,
                          112, 112, 112, 112])

    # For each level 4 class set up an array with possible values
    # lifeform_veg_cat is a parent dependency
    lifeform_veg_cat = numpy.array([1, 1, 1, 1,
                                    3, 3, 3, 3,
                                    1, 1, 1, 1,
                                    3, 3, 3, 3])

    # canopyco_veg_con is a parent dependency
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([100, 65.5, 3.0, 1.0,
                                    100, 65.5, 3.0, 1.0,
                                    100, 65.5, 3.0, 1.0,
                                    100, 65.5, 3.0, 1.0])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([30.0, 15.0, 8.0, 7.0,
                                    30.0, 15.0, 8.0, 7.0,
                                    30.0, 15.0, 8.0, 7.0,
                                    30.0, 15.0, 8.0, 7.0])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # waterday_agr_cat is a parent dependency
    waterday_agr_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # leaftype_veg_cat is a parent dependency
    leaftype_veg_cat = numpy.array([1, 1, 1, 1,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    2, 2, 2, 2])

    # phenolog_veg_cat is a parent dependency
    phenolog_veg_cat = numpy.array([1, 1, 1, 1,
                                    1, 1, 1, 1,
                                    3, 3, 3, 3,
                                    3, 3, 3, 3])

    # spatdist_veg_cat is a parent dependency
    spatdist_veg_cat = numpy.array([1, 2, 6, 7,
                                    1, 2, 6, 7,
                                    1, 2, 6, 7,
                                    1, 2, 6, 7])

    # spatsize_agr_cat is a parent dependency
    spatsize_agr_cat = numpy.array([1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4])

    cropcomb_agr_cat = numpy.array([1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4])
    
    cropseqt_agr_cat = numpy.array([0, 17, 18, 19,
                                    0, 17, 18, 19,
                                    0, 17, 18, 19,
                                    0, 17, 18, 19])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 18, 19,
                                    0, 0, 0, 0,
                                    0, 0, 18, 19,
                                    0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "spatdist_veg_cat" : ({"origin"}, spatdist_veg_cat),
         "spatsize_agr_cat" : ({"origin"}, spatsize_agr_cat),
         "cropcomb_agr_cat" : ({"origin"}, cropcomb_agr_cat),
         "cropseqt_agr_cat" : ({"origin"}, cropseqt_agr_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["cropseqt_agr_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_CropseqaAgrCatL4a():
    """
    Test the cropseqa_agr_cat_l4a class.

    Dependencies for class
    spatdist_veg_cat_l4a > 0
    spatsize_agr_cat_l4a > 0

    Permitted additional attributes
    all
    """

    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111,
                          111, 111, 111,
                          123, 123, 123,
                          123, 123, 123])

    # For each level 4 class set up an array with possible values
    # lifeform_veg_cat is a parent dependency
    lifeform_veg_cat = numpy.array([1, 1, 1,
                                    3, 3, 3,
                                    1, 1, 1,
                                    3, 3, 3])

    # canopyco_veg_con is a parent dependency
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([100, 65.5, 40.5,
                                    100, 65.5, 40.5,
                                    100, 65.5, 40.5,
                                    100, 65.5, 40.5])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([30.0, 15.0, 8.0,
                                    30.0, 15.0, 8.0,
                                    30.0, 15.0, 8.0,
                                    30.0, 15.0, 8.0])

    # waterday_agr_cat is a parent dependency
    waterday_agr_cat = numpy.array([0, 0, 0,
                                    0, 0, 0,
                                    1, 1, 1,
                                    2, 2, 2])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0])

    # leaftype_veg_cat is a parent dependency
    leaftype_veg_cat = numpy.array([1, 1, 1,
                                    1, 1, 1,
                                    0, 0, 0,
                                    0, 0, 0])

    # phenolog_veg_cat is a parent dependency
    phenolog_veg_cat = numpy.array([1, 1, 1,
                                    1, 1, 1,
                                    0, 0, 0,
                                    0, 0, 0])

    spatdist_veg_cat = numpy.array([1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1])

    spatsize_agr_cat = numpy.array([1, 1, 1,
                                    2, 2, 2,
                                    3, 3, 3,
                                    4, 4, 4])

    cropseqa_agr_cat = numpy.array([1, 2, 3,
                                    1, 2, 3,
                                    1, 2, 3,
                                    1, 2, 3])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0,
                                    0, 0, 0,
                                    1, 2, 3,
                                    0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "spatdist_veg_cat" : ({"origin"}, spatdist_veg_cat),
         "spatsize_agr_cat" : ({"origin"}, spatsize_agr_cat),
         "cropseqa_agr_cat" : ({"origin"}, cropseqa_agr_cat)},
         coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["cropseqa_agr_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_WatersupAgrCatL4a():
    """
    Test the watersup_agr_cat_l4a class.

    Dependencies for class
    cropcomb_agr_cat_l4a > 0

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111,
                          111, 111, 111, 111,
                          111, 111, 111, 111,
                          111, 111, 111, 111])

    # For each level 4 class set up an array with possible values
    # lifeform_veg_cat is a parent dependency
    lifeform_veg_cat = numpy.array([1, 1, 1, 1,
                                    3, 3, 3, 3,
                                    1, 1, 1, 1,
                                    3, 3, 3, 3])

    # canopyco_veg_con is a parent dependency
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([100, 65.5, 3.0, 1.0,
                                    100, 65.5, 3.0, 1.0,
                                    100, 65.5, 3.0, 1.0,
                                    100, 65.5, 3.0, 1.0])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([30.0, 15.0, 8.0, 7.0,
                                    30.0, 15.0, 8.0, 7.0,
                                    30.0, 15.0, 8.0, 7.0,
                                    30.0, 15.0, 8.0, 7.0])

    # waterday_agr_cat is a parent dependency
    waterday_agr_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # leaftype_veg_cat is a parent dependency
    leaftype_veg_cat = numpy.array([1, 1, 1, 1,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    2, 2, 2, 2])

    # phenolog_veg_cat is a parent dependency
    phenolog_veg_cat = numpy.array([1, 1, 1, 1,
                                    1, 1, 1, 1,
                                    3, 3, 3, 3,
                                    3, 3, 3, 3])

    # spatdist_veg_cat is a parent dependency
    spatdist_veg_cat = numpy.array([1, 2, 6, 7,
                                    1, 2, 6, 7,
                                    1, 2, 6, 7,
                                    1, 2, 6, 7])

    # spatsize_agr_cat is a parent dependency
    spatsize_agr_cat = numpy.array([1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4])

    cropcomb_agr_cat = numpy.array([0, 0, 0, 0,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    3, 3, 3, 3])

    watersup_agr_cat = numpy.array([2, 4, 5, 6,
                                    2, 4, 5, 6,
                                    2, 4, 5, 6,
                                    2, 4, 5, 6])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0, 0,
                                    2, 0, 5, 6,
                                    2, 0, 5, 6,
                                    2, 0, 5, 6])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "spatdist_veg_cat" : ({"origin"}, spatdist_veg_cat),
         "spatsize_agr_cat" : ({"origin"}, spatsize_agr_cat),
         "cropcomb_agr_cat" : ({"origin"}, cropcomb_agr_cat),
         "watersup_agr_cat" : ({"origin"}, watersup_agr_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["watersup_agr_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_TimefactAgrCatL4a():
    """
    Test the timefact_agr_cat_l4a class.

    Dependencies for class
    if timefact_agr_cat_l4a == 7 | 8
        cropcomb_agr_cat_l4a > 0
    if timefact_agr_cat_l4a == 9
        watersup_agr_cat_l4a == 0 | 1 | 3 | 4 | 5 | 6
        cropcomb_agr_cat_l4a > 0

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111,
                          111, 111, 111,
                          111, 111, 111,
                          111, 111, 111])

    # For each level 4 class set up an array with possible values
    lifeform_veg_cat = numpy.array([1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1])

    # canopyco_veg_con is a parent dependency
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([100.0, 70.0, 50.0,
                                    100.0, 70.0, 50.0,
                                    100.0, 70.0, 50.0,
                                    100.0, 70.0, 50.0])

    # canopyht_veg_con is a parent dependency
    # canopyht_veg_con inputs must be continuous
    canopyht_veg_con = numpy.array([30.0, 15.0, 10.0,
                                    30.0, 15.0, 10.0,
                                    30.0, 15.0, 10.0,
                                    30.0, 15.0, 10.0])

    # waterday_agr_cat is a parent dependency
    waterday_agr_cat = numpy.array([0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0])

    # watersea_veg_cat is a parent dependency
    watersea_veg_cat = numpy.array([0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0])

    # leaftype_veg_cat is a parent dependency
    leaftype_veg_cat = numpy.array([1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1])

    # phenolog_veg_cat is a parent dependency
    phenolog_veg_cat = numpy.array([1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1])

    # spatdist_veg_cat is a parent dependency
    spatdist_veg_cat = numpy.array([1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1])

    # spatsize_agr_cat is a parent dependency
    spatsize_agr_cat = numpy.array([1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1])

    cropcomb_agr_cat = numpy.array([4, 4, 4,
                                    4, 4, 4,
                                    4, 4, 4,
                                    4, 4, 4])

    croplfc2_agr_cat = numpy.array([5, 5, 5,
                                    6, 6, 6,
                                    7, 7, 7,
                                    8, 8, 8])

    croplfc3_agr_cat = numpy.array([13, 13, 13,
                                    14, 14, 14,
                                    15, 15, 15,
                                    0, 0, 0])

    watersup_agr_cat = numpy.array([1, 1, 1,
                                    2, 2, 2,
                                    3, 3, 3,
                                    4, 4, 4])

    timefact_agr_cat = numpy.array([7, 8, 9,
                                    7, 8, 9,
                                    7, 8, 9,
                                    7, 8, 9])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([7, 8, 9,
                                    7, 8, 0,
                                    7, 8, 9,
                                    7, 8, 9])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "lifeform_veg_cat" : ({"origin"}, lifeform_veg_cat),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con),
         "canopyht_veg_con" : ({"origin"}, canopyht_veg_con),
         "waterday_agr_cat" : ({"origin"}, waterday_agr_cat),
         "watersea_veg_cat" : ({"origin"}, watersea_veg_cat),
         "leaftype_veg_cat" : ({"origin"}, leaftype_veg_cat),
         "phenolog_veg_cat" : ({"origin"}, phenolog_veg_cat),
         "spatdist_veg_cat" : ({"origin"}, spatdist_veg_cat),
         "spatsize_agr_cat" : ({"origin"}, spatsize_agr_cat),
         "cropcomb_agr_cat" : ({"origin"}, cropcomb_agr_cat),
         "croplfc2_agr_cat" : ({"origin"}, croplfc2_agr_cat),
         "croplfc3_agr_cat" : ({"origin"}, croplfc3_agr_cat),
         "watersup_agr_cat" : ({"origin"}, watersup_agr_cat),
         "timefact_agr_cat" : ({"origin"}, timefact_agr_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["timefact_agr_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_WatersttWatCatL4a():
    """
    Test the waterstt_wat_cat_l4a class.

    Dependencies for class
    none

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at.
    level3 = numpy.array([220, 220, 220,
                          227, 227, 227,
                          228, 228, 228])

    # For each level 4 class set up an array with possible values
    waterstt_wat_cat = numpy.array([1, 2, 3,
                                    1, 2, 3,
                                    1, 2, 3])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([1, 2, 3,
                                    1, 2, 3,
                                    1, 2, 3])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "waterstt_wat_cat" : ({"origin"}, waterstt_wat_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["waterstt_wat_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_InttidalWatCatL4d():
    """
    Test the inttidal_wat_cat_l4d class.

    Dependencies on class
    waterstt_wat_cat_l4a == 1

    Permitted additional attributes
    waterper_wat_cat_l4d == 0
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([220, 220, 220, 220,
                          220, 220, 220, 220,
                          227, 227, 227, 227,
                          228, 228, 228, 228])

    # For each level 4 class set up an array with  possible values
    waterstt_wat_cat = numpy.array([0, 0, 0, 0,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    3, 3, 3, 3])

    inttidal_wat_cat = numpy.array([0, 0, 0, 0,
                                    3, 3, 3, 3,
                                    3, 3, 3, 3,
                                    0, 0, 0, 0])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0, 0,
                                    3, 3, 3, 3,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "waterstt_wat_cat" : ({"origin"}, waterstt_wat_cat),
         "inttidal_wat_cat" : ({"origin"}, inttidal_wat_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["inttidal_wat_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_WaterperWatCatL4d():
    """
    Test the waterper_wat_cat_l4d class.

    Dependencies on class
    waterstt_wat_cat_l4a == 1

    Permitted additional attributes
    inttidal_wat_cat_l4a == 0
    """
    # Set up array of level 3 classes to look at.
    level3 = numpy.array([220, 220, 220, 220,
                          220, 220, 220, 220,
                          227, 227, 227, 227,
                          228, 228, 228, 228,
                          220, 220, 220, 220])
    
    # For each level 4 class set up an array with possible values
    waterstt_wat_cat = numpy.array([0, 0, 0, 0,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    3, 3, 3, 3,
                                    1, 1, 1, 1])

    inttidal_wat_cat = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # waterper_wat_cin inputs must be continuous integer (i.e. number of days)
    waterper_wat_cin = numpy.array([271.0, 210.0, 120.0, 30.0,
                                    271.0, 210.0, 120.0, 30.0,
                                    271.0, 210.0, 120.0, 30.0,
                                    271.0, 210.0, 120.0, 30.0,
                                    0, 0, 0, 0])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0, 0,
                                    1, 7, 8, 9,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "waterstt_wat_cat" : ({"origin"}, waterstt_wat_cat),
         "inttidal_wat_cat" : ({"origin"}, inttidal_wat_cat),
         "waterper_wat_cin" : ({"origin"}, waterper_wat_cin)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["waterper_wat_cat_l4d"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_SnowcperWatCatL4d():
    """
    Test the snowcper_wat_cat_l4d class.

    Dependencies on class
    waterstt_wat_cat_l4a == 2

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([220, 220, 220, 220,
                          220, 220, 220, 220,
                          227, 227, 227, 227,
                          228, 228, 228, 228])

    # For each level 4 class set up an array with possible values
    waterstt_wat_cat = numpy.array([0, 0, 0, 0,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    3, 3, 3, 3])

    # snowcper_wat_cin inputs must be continuous integer (i.e. number of days)
    snowcper_wat_cin = numpy.array([271.0, 210.0, 120.0, 30.0,
                                    271.0, 210.0, 120.0, 30.0,
                                    271.0, 210.0, 120.0, 30.0,
                                    271.0, 210.0, 120.0, 30.0])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    1, 7, 8, 9,
                                    0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "waterstt_wat_cat" : ({"origin"}, waterstt_wat_cat),
         "snowcper_wat_cin" : ({"origin"}, snowcper_wat_cin)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["snowcper_wat_cat_l4d"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_WaterdptWatCatL4a():
    """
    Test the waterdpt_wat_cat_l4a class.

    Dependencies on class
    waterstt_wat_cat_l4a == 1 & inttidal_wat_cat_l4a == 3
    or
    waterstt_wat_cat_l4a == 1 & waterper_wat_cat_l4d > 0
    or
    waterstt_wat_cat_l4a == 2 & snowcper_wat_cat_l4d > 0
    or
    waterstt_wat_cat_l4a == 3

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([220, 220,
                          220, 220,
                          227, 227,
                          228, 228])

    # For each level 4 class set up an array with possible values
    waterstt_wat_cat = numpy.array([1, 1,
                                    1, 1,
                                    2, 2,
                                    3, 3])

    inttidal_wat_cat = numpy.array([0, 0,
                                    3, 3,
                                    0, 0,
                                    0, 0])

    # waterper_wat_cin inputs must be continuous integer (i.e. number of days)
    waterper_wat_cin = numpy.array([220.0, 220.0,
                                    0.0, 0.0,
                                    0.0, 0.0,
                                    0.0, 0.0])

    # snowcper_wat_cin inputs must be continuous integer (i.e. number of days)
    snowcper_wat_cin = numpy.array([0.0, 0.0,
                                    0.0, 0.0,
                                    220.0, 220.0,
                                    0.0, 0.0])

    waterdpt_wat_cat = numpy.array([1, 2,
                                    1, 2,
                                    1, 2,
                                    1, 2])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([1, 2,
                                    1, 2,
                                    1, 2,
                                    1, 2])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "waterstt_wat_cat" : ({"origin"}, waterstt_wat_cat),
         "inttidal_wat_cat" : ({"origin"}, inttidal_wat_cat),
         "waterper_wat_cin" : ({"origin"}, waterper_wat_cin),
         "snowcper_wat_cin" : ({"origin"}, snowcper_wat_cin),
         "waterdpt_wat_cat" : ({"origin"}, waterdpt_wat_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["waterdpt_wat_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_MwatrmvtWatCatL4m():
    """
    Test the mwatrmvt_wat_cat_l4a class.

    Dependencies for class
    if mwatrmvt_wat_cat_l4m == 4 | 5
        waterper_wat_cat_l4d > 0
        waterdpt_wat_cat_l4a > 0
    if mwatrmvt_wat_cat_l4m == 6 | 7
        waterstt_wat_cat_l4a == 3
        waterdpt_wat_cat_l4a > 0

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([220, 220, 220, 220,
                          220, 220, 220, 220,
                          227, 227, 227, 227,
                          227, 227, 227, 227,
                          228, 228, 228, 228,
                          228, 228, 228, 228])

    # For each level 4 class set up an array with possible values
    waterstt_wat_cat = numpy.array([1, 1, 1, 1,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    2, 2, 2, 2,
                                    3, 3, 3, 3,
                                    3, 3, 3, 3])

    # inttidal_wat_cat is a parent dependency
    inttidal_wat_cat = numpy.array([0, 0, 0, 0,
                                    3, 3, 3, 3,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # waterper_wat_cin inputs must be continuous integer (i.e. number of days)
    waterper_wat_cin = numpy.array([220.0, 220.0, 220.0, 220.0,
                                    0.0, 0.0, 0.0, 0.0,
                                    0.0, 0.0, 0.0, 0.0,
                                    0.0, 0.0, 0.0, 0.0,
                                    0.0, 0.0, 0.0, 0.0,
                                    0.0, 0.0, 0.0, 0.0])

    # snowcper_wat_cin is a parent dependency
    # snowcper_wat_cin inputs must be continuous integer (i.e. number of days)
    snowcper_wat_cin = numpy.array([0.0, 0.0, 0.0, 0.0,
                                    0.0, 0.0, 0.0, 0.0,
                                    0.0, 0.0, 0.0, 0.0,
                                    0.0, 0.0, 0.0, 0.0,
                                    0.0, 0.0, 0.0, 0.0,
                                    0.0, 0.0, 0.0, 0.0])

    waterdpt_wat_cat = numpy.array([1, 2, 1, 2,
                                    1, 2, 1, 2,
                                    1, 2, 1, 2,
                                    1, 2, 1, 2,
                                    1, 2, 1, 2,
                                    1, 2, 1, 2])

    mwatrmvt_wat_cat = numpy.array([4, 5, 6, 7,
                                    4, 5, 6, 7,
                                    4, 5, 6, 7,
                                    4, 5, 6, 7,
                                    4, 5, 6, 7,
                                    4, 5, 6, 7])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([4, 5, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 6, 7,
                                    0, 0, 6, 7])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "waterstt_wat_cat" : ({"origin"}, waterstt_wat_cat),
         "inttidal_wat_cat" : ({"origin"}, inttidal_wat_cat),
         "waterper_wat_cin" : ({"origin"}, waterper_wat_cin),
         "snowcper_wat_cin" : ({"origin"}, snowcper_wat_cin),
         "waterdpt_wat_cat" : ({"origin"}, waterdpt_wat_cat),
         "mwatrmvt_wat_cat" : ({"origin"}, mwatrmvt_wat_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["mwatrmvt_wat_cat_l4m"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_WsedloadWatCatL4a():
    """
    Test the wsedload_wat_cat_l4a class.

    Dependencies on class
    inttidal_wat_cat_l4a == 3 or waterper_wat_cat_l4d > 0
    waterdpt_wat_cat > 0

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([220, 220,
                          220, 220,
                          227, 227,
                          228, 228])

    # For each level 4 class set up an array with possible values
    # waterstt_wat_cat is a parent dependency
    waterstt_wat_cat = numpy.array([1, 1,
                                    1, 1,
                                    1, 1,
                                    1, 1])

    inttidal_wat_cat = numpy.array([0, 0,
                                    3, 3,
                                    0, 0,
                                    0, 0])

    # waterper_wat_cin inputs must be continuous integer (i.e. number of days)
    waterper_wat_cin = numpy.array([120.0, 120.0,
                                    0.0, 0.0,
                                    0.0, 0.0,
                                    0.0, 0.0])

    # snowcper_wat_cin is a parent dependency
    # snowcper_wat_cin inputs must be continuous integer (i.e. number of days)
    snowcper_wat_cin = numpy.array([0.0, 0.0,
                                    0.0, 0.0,
                                    0.0, 0.0,
                                    0.0, 0.0])

    waterdpt_wat_cat = numpy.array([1, 2,
                                    1, 2,
                                    1, 2,
                                    1, 2])

    wsedload_wat_cat = numpy.array([1, 2,
                                    1, 2,
                                    1, 2,
                                    1, 2])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([1, 2,
                                    1, 2,
                                    0, 0,
                                    0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "waterstt_wat_cat": ({"origin"}, waterstt_wat_cat),
         "inttidal_wat_cat": ({"origin"}, inttidal_wat_cat),
         "waterper_wat_cin": ({"origin"}, waterper_wat_cin),
         "snowcper_wat_cin": ({"origin"}, snowcper_wat_cin),
         "waterdpt_wat_cat": ({"origin"}, waterdpt_wat_cat),
         "wsedload_wat_cat" : ({"origin"}, wsedload_wat_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["wsedload_wat_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()
    

def test_MsubstrtWatCatL4m():
    """
    Test the msubstrt_wat_cat_l4m class.

    Dependencies on class
    inttidal_wat_cat_l4a == 3 or waterper_wat_cat_l4d > 0 & != 1
    snowcper_wat_cat_l4d > 0 & != 1
    waterdpt_wat_cat > 0

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([220, 220, 220,
                          220, 220, 220,
                          220, 220, 220,
                          220, 220, 220,
                          227, 227, 227,
                          227, 227, 227,
                          227, 227, 227,
                          228, 228, 228,
                          228, 228, 228,
                          228, 228, 228])

    # For each level 4 class set up an array with possible values
    # waterstt_wat_cat is a parent dependency
    waterstt_wat_cat = numpy.array([1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1,
                                    2, 2, 2,
                                    2, 2, 2,
                                    2, 2, 2,
                                    2, 2, 2,
                                    2, 2, 2])

    inttidal_wat_cat = numpy.array([0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0])

    # waterper_wat_cin inputs must be continuous integer (i.e. number of days)
    waterper_wat_cin = numpy.array([10.0, 10.0, 10.0,
                                    330.0, 330.0, 330.0,
                                    250.0, 250.0, 250.0,
                                    170.0, 170.0, 170.0,
                                    40.0, 40.0, 40.0,
                                    10.0, 10.0, 10.0,
                                    330.0, 330.0, 330.0,
                                    250.0, 250.0, 250.0,
                                    170.0, 170.0, 170.0,
                                    40.0, 40.0, 40.0])

    # snowcper_wat_cin inputs must be continuous integer (i.e. number of days)
    snowcper_wat_cin = numpy.array([10.0, 10.0, 10.0,
                                    330.0, 330.0, 330.0,
                                    250.0, 250.0, 250.0,
                                    170.0, 170.0, 170.0,
                                    40.0, 40.0, 40.0,
                                    10.0, 10.0, 10.0,
                                    330.0, 330.0, 330.0,
                                    250.0, 250.0, 250.0,
                                    170.0, 170.0, 170.0,
                                    40.0, 40.0, 40.0])

    waterdpt_wat_cat = numpy.array([1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1,
                                    1, 1, 1,
                                    2, 2, 2,
                                    2, 2, 2,
                                    2, 2, 2,
                                    2, 2, 2,
                                    2, 2, 2])

    msubstrt_wat_cat = numpy.array([4, 5, 6,
                                    4, 5, 6,
                                    4, 5, 6,
                                    4, 5, 6,
                                    4, 5, 6,
                                    4, 5, 6,
                                    4, 5, 6,
                                    4, 5, 6,
                                    4, 5, 6,
                                    4, 5, 6])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0,
                                    0, 0, 0,
                                    4, 5, 6,
                                    4, 5, 6,
                                    4, 5, 6,
                                    0, 0, 0,
                                    0, 0, 0,
                                    4, 5, 6,
                                    4, 5, 6,
                                    4, 5, 6])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "waterstt_wat_cat" : ({"origin"}, waterstt_wat_cat),
         "inttidal_wat_cat" : ({"origin"}, inttidal_wat_cat),
         "waterper_wat_cin" : ({"origin"}, waterper_wat_cin),
         "snowcper_wat_cin" : ({"origin"}, snowcper_wat_cin),
         "waterdpt_wat_cat" : ({"origin"}, waterdpt_wat_cat),
         "msubstrt_wat_cat" : ({"origin"}, msubstrt_wat_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["msubstrt_wat_cat_l4m"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_BaresurfPhyCatL4a():
    """
    Test the baresurf_phy_cat_l4a class.

    Dependencies for class
    none

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([215, 215, 215, 215, 215, 215, 215,
                          216, 216, 216, 216, 216, 216, 216,
                          227, 227, 227, 227, 227, 227, 227,
                          228, 228, 228, 228, 228, 228, 228])

    # For each level 4 class set up an array with possible values
    baresurf_phy_cat = numpy.array([0, 1, 2, 3, 4, 5, 6,
                                    0, 1, 2, 3, 4, 5, 6,
                                    0, 1, 2, 3, 4, 5, 6,
                                    0, 1, 2, 3, 4, 5, 6])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0, 0, 0, 0, 0,
                                    0, 1, 2, 3, 4, 5, 6,
                                    0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "baresurf_phy_cat" : ({"origin"}, baresurf_phy_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["baresurf_phy_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_MbarematPhyCatL4m():
    """
    Test the mbaremat_phy_cat_l4m class.

    Dependencies for class
    baresurf_phy_cat_l4a == 3

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([216, 216, 216, 216, 216, 216,
                          216, 216, 216, 216, 216, 216,
                          216, 216, 216, 216, 216, 216,
                          216, 216, 216, 216, 216, 216,
                          216, 216, 216, 216, 216, 216,
                          216, 216, 216, 216, 216, 216,
                          216, 216, 216, 216, 216, 216])


    # For each level 4 class set up an array with possible values
    baresurf_phy_cat = numpy.array([0, 0, 0, 0, 0, 0,
                                    1, 1, 1, 1, 1, 1,
                                    2, 2, 2, 2, 2, 2,
                                    3, 3, 3, 3, 3, 3,
                                    4, 4, 4, 4, 4, 4,
                                    5, 5, 5, 5, 5, 5,
                                    6, 6, 6, 6, 6, 6])

    mbaremat_phy_cat = numpy.array([0, 7, 8, 14, 15, 16,
                                    0, 7, 8, 14, 15, 16,
                                    0, 7, 8, 14, 15, 16,
                                    0, 7, 8, 14, 15, 16,
                                    0, 7, 8, 14, 15, 16,
                                    0, 7, 8, 14, 15, 16,
                                    0, 7, 8, 14, 15, 16])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0,
                                    0, 7, 8, 14, 15, 16,
                                    0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "baresurf_phy_cat" : ({"origin"}, baresurf_phy_cat),
         "mbaremat_phy_cat" : ({"origin"}, mbaremat_phy_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["mbaremat_phy_cat_l4m"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_MustonesPhyCatL4m():
    """
    Test the mustones_phy_cat class.

    Dependencies for class
    if mustones_phy_cat_l4m == 12
        baresurf_phy_cat_l4a == 2 | 5 | 6
    if mustones_phy_cat_l4m == 13
        baresurf_phy_cat_l4a == 2 | 5

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([216, 216, 216,
                          216, 216, 216,
                          216, 216, 216,
                          216, 216, 216,
                          216, 216, 216,
                          216, 216, 216,
                          216, 216, 216])

    # For each level 4 class set up an array with possible values
    baresurf_phy_cat = numpy.array([0, 0, 0,
                                    1, 1, 1,
                                    2, 2, 2,
                                    3, 3, 3,
                                    4, 4, 4,
                                    5, 5, 5,
                                    6, 6, 6])

    mustones_phy_cat = numpy.array([0, 12, 13,
                                    0, 12, 13,
                                    0, 12, 13,
                                    0, 12, 13,
                                    0, 12, 13,
                                    0, 12, 13,
                                    0, 12, 13])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0,
                                    0, 0, 0,
                                    0, 12, 13,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 12, 13,
                                    0, 12, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "baresurf_phy_cat" : ({"origin"}, baresurf_phy_cat),
         "mustones_phy_cat" : ({"origin"}, mustones_phy_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["mustones_phy_cat_l4m"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_MhardpanPhyCatL4m():
    """
    Test the mhardpan_phy_cat_l4m class.

    Dependencies for class
    baresurf_phy_cat_l4a == 4

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([216, 216, 216, 216,
                          216, 216, 216, 216,
                          216, 216, 216, 216,
                          216, 216, 216, 216,
                          216, 216, 216, 216,
                          216, 216, 216, 216,
                          216, 216, 216, 216])

    # For each level 4 class set up an array with possible values
    baresurf_phy_cat = numpy.array([0, 0, 0, 0,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    3, 3, 3, 3,
                                    4, 4, 4, 4,
                                    5, 5, 5, 5,
                                    6, 6, 6, 6])


    mhardpan_phy_cat = numpy.array([0, 9, 10, 11,
                                    0, 9, 10, 11,
                                    0, 9, 10, 11,
                                    0, 9, 10, 11,
                                    0, 9, 10, 11,
                                    0, 9, 10, 11,
                                    0, 9, 10, 11])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 9, 10, 11,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "baresurf_phy_cat" : ({"origin"}, baresurf_phy_cat),
         "mhardpan_phy_cat" : ({"origin"}, mhardpan_phy_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["mhardpan_phy_cat_l4m"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_MacropatPhyCatL4a():
    """
    Test the macropat_phy_cat_l4a class.

    Dependencies for class
    none

    Permitted additional attributes
    if macropat_phy_cat_l4a == 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10
        baresurf_phy_cat_l4a == 2 | 6
    if macropat_phy_cat_l4a == 11 | 12 | 13
        baresurf_phy_cat_l4a == 2 | 5
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216,
                          216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216,
                          216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216,
                          216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216,
                          216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216,
                          216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216,
                          216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216, 216])

    # For each level 4 class set up an array with possible values
    baresurf_phy_cat = numpy.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                                    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                                    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
                                    5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
                                    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6])

    mustones_phy_cat = numpy.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 12, 12, 12, 12, 13, 13, 13, 0, 0, 0])

    macropat_phy_cat = numpy.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 12, 13,
                                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "baresurf_phy_cat": ({"origin"}, baresurf_phy_cat),
         "mustones_phy_cat": ({"origin"}, mustones_phy_cat),
         "macropat_phy_cat" : ({"origin"}, macropat_phy_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["macropat_phy_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_ArtisurfUrbCatL4a():
    """
    Test the artisurf_urb_cat_l4a class.

    Dependencies for class
    none

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111,
                          124, 124, 124, 124,
                          216, 216, 216, 216,
                          215, 215, 215, 215])

    # For each level 4 class set up an array with possible values
    artisurf_urb_cat = numpy.array([1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4,
                                    1, 2, 3, 4])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    1, 2, 3, 4])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "artisurf_urb_cat" : ({"origin"}, artisurf_urb_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["artisurf_urb_cat_l4a"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_MtclineaUrbCatL4m():
    """
    Test the mtclinea_urb_cat_l4m class.

    Dependencies for class
    none

    Permitted additional attributes
    artisurf_urb_cat_l4a == 3
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([215, 215, 215, 215, 215,
                          215, 215, 215, 215, 215,
                          215, 215, 215, 215, 215,
                          215, 215, 215, 215, 215])

    # For each level 4 class set up an array with possible values
    artisurf_urb_cat = numpy.array([1, 1, 1, 1, 1,
                                    2, 2, 2, 2, 2,
                                    3, 3, 3, 3, 3,
                                    4, 4, 4, 4, 4])

    mtclinea_urb_cat = numpy.array([7, 8, 9, 10, 11,
                                    7, 8, 9, 10, 11,
                                    7, 8, 9, 10, 11,
                                    7, 8, 9, 10, 11])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0,
                                    7, 8, 9, 10, 11,
                                    0, 0, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "artisurf_urb_cat" : ({"origin"}, artisurf_urb_cat),
         "mtclinea_urb_cat" : ({"origin"}, mtclinea_urb_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["mtclinea_urb_cat_l4m"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_MnolineaUrbCatL4m():
    """
    Test the mnolinea_urb_cat_l4m class.

    Dependencies for class
    artisurf_urb_cat_l4a == 4

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([215, 215,
                          215, 215,
                          215, 215,
                          215, 215])

    # For each level 4 class set up an array with possible values
    artisurf_urb_cat = numpy.array([1, 1,
                                    2, 2,
                                    3, 3,
                                    4, 4])

    mnolinea_urb_cat = numpy.array([12, 13,
                                    12, 13,
                                    12, 13,
                                    12, 13])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0,
                                    0, 0,
                                    0, 0,
                                    12, 13])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "artisurf_urb_cat" : ({"origin"}, artisurf_urb_cat),
         "mnolinea_urb_cat" : ({"origin"}, mnolinea_urb_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["mnolinea_urb_cat_l4m"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()  


def test_MartdensUrbCatL4m():
    """
    Test the martdens_urb_cat_l4d class.

    Dependencies for class
    artisurf_urb_cat_l4a == 4
    mnolinea_urb_cat_l4m == 12 | 13

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([215, 215, 215, 215,
                          215, 215, 215, 215,
                          215, 215, 215, 215,
                          215, 215, 215, 215,
                          215, 215, 215, 215,
                          215, 215, 215, 215,
                          215, 215, 215, 215,
                          215, 215, 215, 215])

    # For each level 4 class set up an array with possible values
    artisurf_urb_cat = numpy.array([1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    3, 3, 3, 3,
                                    4, 4, 4, 4,
                                    1, 1, 1, 1,
                                    2, 2, 2, 2,
                                    3, 3, 3, 3,
                                    4, 4, 4, 4])

    mnolinea_urb_cat = numpy.array([12, 12, 12, 12,
                                    12, 12, 12, 12,
                                    12, 12, 12, 12,
                                    12, 12, 12, 12,
                                    13, 13, 13, 13,
                                    13, 13, 13, 13,
                                    13, 13, 13, 13,
                                    13, 13, 13, 13])

    # martdens_urb_cin inputs must be continuous integer (i.e 15 - 100 %)
    martdens_urb_cin = numpy.array([75.0, 50.0, 30.0, 15.0,
                                    75.0, 50.0, 30.0, 15.0,
                                    75.0, 50.0, 30.0, 15.0,
                                    75.0, 50.0, 30.0, 15.0,
                                    75.0, 50.0, 30.0, 15.0,
                                    75.0, 50.0, 30.0, 15.0,
                                    75.0, 50.0, 30.0, 15.0,
                                    75.0, 50.0, 30.0, 15.0])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    14, 15, 16, 17,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    14, 15, 16, 17])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "artisurf_urb_cat" : ({"origin"}, artisurf_urb_cat),
         "mnolinea_urb_cat" : ({"origin"}, mnolinea_urb_cat),
         "martdens_urb_cin" : ({"origin"}, martdens_urb_cin)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["martdens_urb_cat_l4d"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()


def test_MnobuiltUrbCatL4m():
    """
    Test the mnobuilt_urb_cat_l4m class.

    Dependencies for class
    artisurf_urb_cat_l4a == 2

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([215, 215,
                          215, 215,
                          215, 215,
                          215, 215])

    # For each level 4 class set up an array with possible values
    artisurf_urb_cat = numpy.array([1, 1,
                                    2, 2,
                                    3, 3,
                                    4, 4])

    mnobuilt_urb_cat = numpy.array([5, 6,
                                    5, 6,
                                    5, 6,
                                    5, 6])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([0, 0,
                                    5, 6,
                                    0, 0,
                                    0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "artisurf_urb_cat" : ({"origin"}, artisurf_urb_cat),
         "mnobuilt_urb_cat" : ({"origin"}, mnobuilt_urb_cat)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["mnobuilt_urb_cat_l4m"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()

