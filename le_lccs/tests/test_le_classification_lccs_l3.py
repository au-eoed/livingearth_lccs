"""
Test for LCCS Level 3 Classification

Uses 1D arrays (table) as makes test simpler.

Class (LCCS) | Class (numeric) | vegetat | aquatic | cultman | artific | artwatr
-----------------------------------------------------------------------------------
    A11      |       111       |    1    |    0    |    1    |  nan    |  nan
    A12      |       112       |    1    |    0    |    0    |  nan    |  nan
    A23      |       123       |    1    |    1    |    1    |  nan    |  nan
    A24      |       124       |    1    |    1    |    0    |  nan    |  nan
    B15      |       215       |    0    |    0    |   nan   |   1     |  nan
    B16      |       216       |    0    |    0    |   nan   |   0     |  nan
    B27      |       227       |    0    |    1    |   nan   |  nan    |   1
    B28      |       228       |    0    |    1    |   nan   |  nan    |   0
    B20      |       220       |    0    |    1    |   nan   |  nan    |  nan
    ""       |        0        |   nan   |    0    |   nan   |  nan    |  nan
    ""       |        0        |    0    |   nan   |   nan   |  nan    |  nan

"""
import numpy
import xarray

from le_lccs.le_classification import lccs_l3

# Number of times to repeat each sequence. Normally 1
# set to a large number to evaluate performance.
REPEAT_NUM = 1

# Set up test array of object IDs
TEST_OBJECT_IDS = numpy.repeat(numpy.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]), REPEAT_NUM)

# Set up test arrays for each of the variables needed for L3 classification
TEST_VEGETAT = numpy.repeat(numpy.array([1, 1, 1, 1, 0, 0, 0, 0, 0, numpy.nan, 0]), REPEAT_NUM)
TEST_AQUATIC = numpy.repeat(numpy.array([0, 0, 1, 1, 0, 0, 1, 1, 1, 0, numpy.nan]), REPEAT_NUM)
TEST_CULTMAN = numpy.repeat(numpy.array([1, 0, 1, 0, numpy.nan, numpy.nan, numpy.nan, numpy.nan, numpy.nan, numpy.nan, numpy.nan]), REPEAT_NUM)
TEST_ARTIFIC = numpy.repeat(numpy.array([numpy.nan, numpy.nan, numpy.nan, numpy.nan, 1, 0, numpy.nan, numpy.nan, numpy.nan, numpy.nan, numpy.nan]), REPEAT_NUM)
TEST_ARTWATR = numpy.repeat(numpy.array([numpy.nan, numpy.nan, numpy.nan, numpy.nan, numpy.nan, numpy.nan, 1, 0, numpy.nan, numpy.nan, numpy.nan]), REPEAT_NUM)

# Set up arrays of expected outputs for test variable arrays
TEST_EXPECTED_L1_CLASS = numpy.repeat(numpy.array([100, 100, 100, 100, 200, 200, 200, 200, 200, 0, 200]), REPEAT_NUM)
TEST_EXPECTED_L2_CLASS = numpy.repeat(numpy.array([10, 10, 20, 20, 10, 10, 20, 20, 20, 10, 0]), REPEAT_NUM)
TEST_EXPECTED_L3_CLASS = numpy.repeat(numpy.array([111, 112, 123, 124, 215, 216, 227, 228, 220, 0, 0]), REPEAT_NUM)

def test_level3_classification():

    # Set up xarray of test data
    classification_data = xarray.Dataset(
        {"vegetat_veg_cat" : (TEST_OBJECT_IDS.shape, TEST_VEGETAT),
         "aquatic_wat_cat" : (TEST_OBJECT_IDS.shape, TEST_AQUATIC),
         "cultman_agr_cat" : (TEST_OBJECT_IDS.shape, TEST_CULTMAN),
         "artific_urb_cat" : (TEST_OBJECT_IDS.shape, TEST_ARTIFIC),
         "artwatr_wat_cat" : (TEST_OBJECT_IDS.shape, TEST_ARTWATR)},
    coords={"object" : TEST_OBJECT_IDS})

    # Apply classification
    level1, level2, level3 = lccs_l3.classify_lccs_level3(classification_data)

    # Check equal to test arrays
    assert (level3 == TEST_EXPECTED_L3_CLASS).all()
    assert (level2 == TEST_EXPECTED_L2_CLASS).all()
    assert (level1 == TEST_EXPECTED_L1_CLASS).all()

