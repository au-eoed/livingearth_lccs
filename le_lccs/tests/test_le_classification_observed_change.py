"""
Test for LCCS Evidence Based Change

Uses 1D arrays (table) as makes test simpler.

"""
import numpy
import xarray

from le_lccs.le_classification import simple_change
from le_lccs.le_classification import observed_change


def test_CTVExtentGain():
    """
    Test the CTVExtentGain class
    """
    # Get output code for change
    change_output_code = observed_change.CTVExtentGain().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([112, 123, 124, 215, 216, 220])
    date2_level3 = numpy.array([111, 111, 111, 112, 123, 124])

    # Set up array of expected change codes
    expected_changes = numpy.array([change_output_code, change_output_code, change_output_code, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.CTVExtentGain().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_CTVExtentLoss():
    """
    Test the CTVExtentLoss class
    """
    # Get output code for change
    change_output_code = observed_change.CTVExtentLoss().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([111, 111, 111, 111, 124, 220])
    date2_level3 = numpy.array([112, 220, 215, 111, 112, 220])

    # Set up array of expected change codes
    expected_changes = numpy.array([change_output_code, change_output_code, change_output_code, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.CTVExtentLoss().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_NTVExtentGain():
    """
    Test the NTVExtentGain class
    """
    # Get output code for change
    change_output_code = observed_change.NTVExtentGain().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([111, 124, 220, 111, 124, 220])
    date2_level3 = numpy.array([112, 112, 112, 111, 215, 220])

    # Set up array of expected change codes
    expected_changes = numpy.array([change_output_code, change_output_code, change_output_code, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NTVExtentGain().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_NTVExtentLoss():
    """
    Test the NTVExtentLoss class
    """
    # Get output code for change
    change_output_code = observed_change.NTVExtentLoss().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([112, 112, 112, 112, 215, 220])
    date2_level3 = numpy.array([111, 124, 220, 112, 124, 220])

    # Set up array of expected change codes
    expected_changes = numpy.array([change_output_code, change_output_code, change_output_code, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NTVExtentLoss().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_NAVExtentGain():
    """
    Test the NAVExtentGain class
    """
    # Get output code for change
    change_output_code = observed_change.NAVExtentGain().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([111, 123, 220, 111, 124, 220])
    date2_level3 = numpy.array([124, 124, 124, 111, 124, 220])

    # Set up array of expected change codes
    expected_changes = numpy.array([change_output_code, change_output_code, change_output_code, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NAVExtentGain().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_NAVExtentLoss():
    """
    Test the NAVExtentLoss class
    """
    # Get output code for change
    change_output_code = observed_change.NAVExtentLoss().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([124, 124, 124, 112, 124, 220])
    date2_level3 = numpy.array([111, 112, 220, 112, 124, 220])

    # Set up array of expected change codes
    expected_changes = numpy.array([change_output_code, change_output_code, change_output_code, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NAVExtentLoss().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_ASExtentGain():
    """
    Test the ASExtentGain class
    """
    # Get output code for change
    change_output_code = observed_change.ASExtentGain().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([111, 123, 220, 111, 215, 220])
    date2_level3 = numpy.array([215, 215, 215, 111, 215, 220])

    # Set up array of expected change codes
    expected_changes = numpy.array([change_output_code, change_output_code, change_output_code, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.ASExtentGain().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_ASExtentLoss():
    """
    Test the ASExtentLoss class
    """
    # Get output code for change
    change_output_code = observed_change.ASExtentLoss().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([215, 215, 215, 112, 215, 220])
    date2_level3 = numpy.array([111, 112, 220, 112, 215, 220])

    # Set up array of expected change codes
    expected_changes = numpy.array([change_output_code, change_output_code, change_output_code, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.ASExtentLoss().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_BSExtentGain():
    """
    Test the BSExtentGain class
    """
    # Get output code for change
    change_output_code = observed_change.BSExtentGain().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([111, 215, 220, 111, 216, 215])
    date2_level3 = numpy.array([216, 216, 216, 111, 216, 220])

    # Set up array of expected change codes
    expected_changes = numpy.array([change_output_code, change_output_code, change_output_code, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.BSExtentGain().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_BSExtentLoss():
    """
    Test the BSExtentLoss class
    """
    # Get output code for change
    change_output_code = observed_change.BSExtentLoss().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([216, 216, 216, 112, 216, 220])
    date2_level3 = numpy.array([111, 215, 220, 112, 216, 215])

    # Set up array of expected change codes
    expected_changes = numpy.array([change_output_code, change_output_code, change_output_code, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.BSExtentLoss().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_WExtentGain():
    """
    Test the WExtentGain class
    """
    # Get output code for change
    change_output_code = observed_change.WExtentGain().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([111, 215, 216, 111, 220, 215])
    date2_level3 = numpy.array([220, 220, 220, 111, 220, 216])

    # Set up array of expected change codes
    expected_changes = numpy.array([change_output_code, change_output_code, change_output_code, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.WExtentGain().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_WExtentLoss():
    """
    Test the WExtentLoss class
    """
    # Get output code for change
    change_output_code = observed_change.WExtentLoss().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([220, 220, 220, 112, 220, 216])
    date2_level3 = numpy.array([111, 215, 216, 112, 220, 215])

    # Set up array of expected change codes
    expected_changes = numpy.array([change_output_code, change_output_code, change_output_code, 0, 0, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.WExtentLoss().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()


def test_CTVGreening():
    """
    Test the CTVGreening class
    """
    # Get output code for change
    change_output_code = observed_change.CTVGreening().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([111, 111, 111, 111])
    date2_level3 = numpy.array([111, 111, 111, 111])

    # Set up canopy cover classes
    date1_canopy_cover = numpy.array([10, 12, 16, 13])
    date2_canopy_cover = numpy.array([10, 10, 13, 16])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "canopyco_veg_cat_l4d": ({"object"}, date1_canopy_cover)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "canopyco_veg_cat_l4d": ({"object"}, date2_canopy_cover)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.CTVGreening().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_CTVBrowning():
    """
    Test the CTVBrowning class
    """
    # Get output code for change
    change_output_code = observed_change.CTVBrowning().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([111, 111, 111, 111])
    date2_level3 = numpy.array([111, 111, 111, 111])

    # Set up canopy cover classes
    date1_canopy_cover = numpy.array([10, 10, 13, 16])
    date2_canopy_cover = numpy.array([10, 12, 16, 13])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "canopyco_veg_cat_l4d": ({"object"}, date1_canopy_cover)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "canopyco_veg_cat_l4d": ({"object"}, date2_canopy_cover)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.CTVBrowning().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_NAVGreening():
    """
    Test the NAVGreening class
    """
    # Get output code for change
    change_output_code = observed_change.NAVGreening().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([124, 124, 124, 124])
    date2_level3 = numpy.array([124, 124, 124, 124])

    # Set up canopy cover classes
    date1_canopy_cover = numpy.array([10, 12, 16, 13])
    date2_canopy_cover = numpy.array([10, 10, 13, 16])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "canopyco_veg_cat_l4d": ({"object"}, date1_canopy_cover)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "canopyco_veg_cat_l4d": ({"object"}, date2_canopy_cover)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NAVGreening().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()


def test_NAVBrowning():
    """
    Test the NAVBrowning class
    """
    # Get output code for change
    change_output_code = observed_change.NAVBrowning().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([124, 124, 124, 124])
    date2_level3 = numpy.array([124, 124, 124, 124])
    
    # Set up canopy cover classes
    date1_canopy_cover = numpy.array([10, 10, 13, 16])
    date2_canopy_cover = numpy.array([10, 12, 16, 13])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3" : ({"object"}, date1_level3),
         "canopyco_veg_cat_l4d" : ({"object"}, date1_canopy_cover)},
        coords={"object" : object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3" : ({"object"}, date2_level3),
         "canopyco_veg_cat_l4d" : ({"object"}, date2_canopy_cover)},
        coords={"object" : object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NAVBrowning().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()
  
def test_NTVWoodyGreening():
    """
    Test the NTVWoodyGreening class
    """
    # Get output code for change
    change_output_code = observed_change.NTVWoodyGreening().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([112, 112, 112, 112])
    date2_level3 = numpy.array([112, 112, 112, 112])

    # Set up lifeform classes
    date1_lifeform = numpy.array([1, 1, 1, 1])
    date2_lifeform = numpy.array([1, 1, 1, 1])

    # Set up canopy cover classes
    date1_canopy_cover = numpy.array([10, 12, 16, 13])
    date2_canopy_cover = numpy.array([10, 10, 13, 16])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date1_lifeform),
         "canopyco_veg_cat_l4d": ({"object"}, date1_canopy_cover)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date2_lifeform),
         "canopyco_veg_cat_l4d": ({"object"}, date2_canopy_cover)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NTVWoodyGreening().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_NTVWoodyBrowning():
    """
    Test the NTVWoodyBrowning class
    """
    # Get output code for change
    change_output_code = observed_change.NTVWoodyBrowning().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([112, 112, 112, 112])
    date2_level3 = numpy.array([112, 112, 112, 112])

    # Set up lifeform classes
    date1_lifeform = numpy.array([1, 1, 1, 1])
    date2_lifeform = numpy.array([1, 1, 1, 1])

    # Set up canopy cover classes
    date1_canopy_cover = numpy.array([10, 10, 13, 16])
    date2_canopy_cover = numpy.array([10, 12, 16, 13])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date1_lifeform),
         "canopyco_veg_cat_l4d": ({"object"}, date1_canopy_cover)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date2_lifeform),
         "canopyco_veg_cat_l4d": ({"object"}, date2_canopy_cover)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NTVWoodyBrowning().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_NTVHerbaceousGreening():
    """
    Test the NTVHerbaceousGreening class
    """
    # Get output code for change
    change_output_code = observed_change.NTVHerbaceousGreening().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([112, 112, 112, 112])
    date2_level3 = numpy.array([112, 112, 112, 112])

    # Set up lifeform classes
    date1_lifeform = numpy.array([2, 2, 2, 2])
    date2_lifeform = numpy.array([2, 2, 2, 2])

    # Set up canopy cover classes
    date1_canopy_cover = numpy.array([10, 12, 16, 13])
    date2_canopy_cover = numpy.array([10, 10, 13, 16])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date1_lifeform),
         "canopyco_veg_cat_l4d": ({"object"}, date1_canopy_cover)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date2_lifeform),
         "canopyco_veg_cat_l4d": ({"object"}, date2_canopy_cover)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NTVHerbaceousGreening().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_NTVHerbaceousBrowning():
    """
    Test the NTVHerbaceousBrowning class
    """
    # Get output code for change
    change_output_code = observed_change.NTVHerbaceousBrowning().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([112, 112, 112, 112])
    date2_level3 = numpy.array([112, 112, 112, 112])

    # Set up lifeform classes
    date1_lifeform = numpy.array([2, 2, 2, 2])
    date2_lifeform = numpy.array([2, 2, 2, 2])

    # Set up canopy cover classes
    date1_canopy_cover = numpy.array([10, 10, 13, 16])
    date2_canopy_cover = numpy.array([10, 12, 16, 13])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date1_lifeform),
         "canopyco_veg_cat_l4d": ({"object"}, date1_canopy_cover)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date2_lifeform),
         "canopyco_veg_cat_l4d": ({"object"}, date2_canopy_cover)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NTVHerbaceousBrowning().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_NTVWoodyExtentGain():
    """
    Test the NTVWoodyExtentGain class
    """
    # Get output code for change
    change_output_code = observed_change.NTVWoodyExtentGain().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([112, 112, 112, 111, 124])
    date2_level3 = numpy.array([112, 112, 112, 112, 112])

    # Set up lifeform (woody / herbeceous) classes
    date1_lifeform = numpy.array([1, 2, 2, 1, 0])
    date2_lifeform = numpy.array([1, 1, 2, 1, 1])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, 0, change_output_code, change_output_code])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date1_lifeform)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date2_lifeform)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NTVWoodyExtentGain().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_NTVWoodyExtentLoss():
    """
    Test the NTVWoodyExtentLoss class
    """
    # Get output code for change
    change_output_code = observed_change.NTVWoodyExtentLoss().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([112, 112, 112, 112, 112, 124])
    date2_level3 = numpy.array([112, 112, 112, 112, 124, 112])

    # Set up lifeform (woody / herbeceous) classes
    date1_lifeform = numpy.array([1, 2, 1, 2, 1, 1])
    date2_lifeform = numpy.array([1, 1, 2, 2, 1, 2])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, 0, change_output_code, 0, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date1_lifeform)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date2_lifeform)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NTVWoodyExtentLoss().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_NTVHerbaceousExtentGain():
    """
    Test the NTVHerbaceousExtentGain class
    """
    # Get output code for change
    change_output_code = observed_change.NTVHerbaceousExtentGain().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([112, 112, 112, 111, 124])
    date2_level3 = numpy.array([112, 112, 112, 112, 112])

    # Set up lifeform (woody / herbeceous) classes
    date1_lifeform = numpy.array([1, 1, 2, 2, 0])
    date2_lifeform = numpy.array([1, 2, 2, 2, 2])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, 0, change_output_code, change_output_code])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date1_lifeform)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date2_lifeform)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NTVHerbaceousExtentGain().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_NTVHerbaceousExtentLoss():
    """
    Test the NTVHerbaceousExtentLoss class
    """
    # Get output code for change
    change_output_code = observed_change.NTVHerbaceousExtentLoss().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([112, 112, 112, 112, 112, 124])
    date2_level3 = numpy.array([112, 112, 112, 112, 124, 112])

    # Set up lifeform (woody / herbeceous) classes
    date1_lifeform = numpy.array([1, 2, 1, 2, 2, 2])
    date2_lifeform = numpy.array([1, 1, 2, 2, 2, 1])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, 0, 0, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date1_lifeform)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "lifeform_veg_cat_l4a": ({"object"}, date2_lifeform)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.NTVHerbaceousExtentLoss().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_WPersistenceIncrease():
    """
    Test the WPersistenceIncrease class
    """
    # Get output code for change
    change_output_code = observed_change.WPersistenceIncrease().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([220, 220, 220, 220])
    date2_level3 = numpy.array([220, 220, 220, 220])

    # Set up water persistence classes
    date1_water_persistence = numpy.array([9, 8, 7, 1])
    date2_water_persistence = numpy.array([9, 1, 1, 9])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "waterper_wat_cat_l4d": ({"object"}, date1_water_persistence)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "waterper_wat_cat_l4d": ({"object"}, date2_water_persistence)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.WPersistenceIncrease().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_WPersistenceDecrease():
    """
    Test the WPersistenceDecrease class
    """
    # Get output code for change
    change_output_code = observed_change.WPersistenceDecrease().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([220, 220, 220, 220])
    date2_level3 = numpy.array([220, 220, 220, 220])

    # Set up water persistence classes
    date1_water_persistence = numpy.array([9, 1, 1, 9])
    date2_water_persistence = numpy.array([9, 8, 7, 1])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "waterper_wat_cat_l4d": ({"object"}, date1_water_persistence)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "waterper_wat_cat_l4d": ({"object"}, date2_water_persistence)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.WPersistenceDecrease().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_BSBrowning():
    """
    Test the BSBrowning class
    """
    # Get output code for change
    change_output_code = observed_change.BSBrowning().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([216, 216, 216, 216])
    date2_level3 = numpy.array([216, 216, 216, 216])

    # Set up bare gradation classes
    date1_bare_grad = numpy.array([12, 10, 12, 15])
    date2_bare_grad = numpy.array([12, 15, 15, 12])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "baregrad_phy_cat_l4d_au": ({"object"}, date1_bare_grad)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "baregrad_phy_cat_l4d_au": ({"object"}, date2_bare_grad)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.BSBrowning().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()

def test_BSGreening():
    """
    Test the BSGreening class
    """
    # Get output code for change
    change_output_code = observed_change.BSGreening().output_code

    # Set up level3 classes
    date1_level3 = numpy.array([216, 216, 216, 216])
    date2_level3 = numpy.array([216, 216, 216, 216])

    # Set up bare gradation classes
    date1_bare_grad = numpy.array([12, 15, 12, 12])
    date2_bare_grad = numpy.array([12, 10, 10, 15])

    # Set up array of expected change codes
    expected_changes = numpy.array([0, change_output_code, change_output_code, 0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, date1_level3.size)

    # Create an xarray with input classes
    date1_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date1_level3),
         "baregrad_phy_cat_l4d_au": ({"object"}, date1_bare_grad)},
        coords={"object": object_ids})

    date2_level4_classification_xarray = xarray.Dataset(
        {"level3": ({"object"}, date2_level3),
         "baregrad_phy_cat_l4d_au": ({"object"}, date2_bare_grad)},
        coords={"object": object_ids})

    # Run simple change
    level4_change = simple_change.get_lccs_l4_change_codes(date1_level4_classification_xarray,
                                                           date2_level4_classification_xarray,
                                                           ignore_no_change=True)

    # Run evidence based change
    output_change_numpy = numpy.zeros(level4_change["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    output_change_numpy = observed_change.BSGreening().get_change(
        level4_change, date1_level4_classification_xarray, date2_level4_classification_xarray,
        output_change_numpy)

    # Check if the output exactly matches the expected values
    assert (output_change_numpy == expected_changes).all()
