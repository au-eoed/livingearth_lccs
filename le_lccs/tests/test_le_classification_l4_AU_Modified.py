"""
Test for LCCS Level 4 Classification Layers that are modified for the AU implimentation of LCCS

Currently used modified layers for Australia LCCS implementation (2022):


CanopycoVegCatL4d_AU,

WaterseaVegCatL4a_AU, <- to add

WaterperWatCatL4d_AU, <- to add

BaregradPhyCatL4d_AU, <- to add

NOTE: unit test take in level 4 inputs, run classification, then expected is output classes.
Level 4 filters are applied to output classes.

NOTE: Unit tests for a specific class must consider parent dependencies,
as well as direct dependencies of class

"""
import numpy
import xarray

from le_lccs.le_classification import l4_layers_AU_Modified
from le_lccs.le_classification import lccs_l4

def test_CanopycoVegCatL4d_AU():
    """
    Test the canopyco_veg_cat_l4d_au class.

    Dependencies for class
    none

    Permitted additional attributes
    all
    """
    # Set up array of level 3 classes to look at
    level3 = numpy.array([111, 111, 111, 111, 111,
                          112, 112, 112, 112, 112,
                          123, 123, 123, 123, 123,
                          124, 124, 124, 124, 124,
                          124, 124, 124, 124, 124])

    # For each level 4 class set up an array with possible values
    # canopyco_veg_con inputs must be continuous
    canopyco_veg_con = numpy.array([65.5, 40.5, 20.5, 4.5, 1.5,
                                    65.5, 40.5, 20.5, 4.5, 1.5,
                                    65.5, 40.5, 20.5, 4.5, 1.5,
                                    65.5, 40.5, 20.5, 4.5, 1.5,
                                    0,    0,    0,    0,   0])

    # Set up an array of expected classes given inputs
    expected_classes = numpy.array([10, 12, 13, 15, 16,
                                    10, 12, 13, 15, 16,
                                    10, 12, 13, 15, 16,
                                    10, 12, 13, 15, 16,
                                    0,  0,  0,  0,  0])

    # Set up an array of IDs (replicates a table)
    object_ids = numpy.arange(0, level3.size)

    # Create an xarray with input classes
    classification_data = xarray.Dataset(
        {"level3" : ({"origin"}, level3),
         "canopyco_veg_con" : ({"origin"}, canopyco_veg_con)},
        coords={"object" : object_ids})

    # Run classification
    out_l4_xarray = lccs_l4.classify_lccs_level4(classification_data)
    # Will return an xarray.Dataset so pull out values as a numpy array
    out_l4_values = out_l4_xarray["canopyco_veg_cat_l4d_au"].values

    # Check if the output exactly matches the expected values
    assert (out_l4_values == expected_classes).all()
