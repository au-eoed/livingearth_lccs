"""
LCCS Level 4 Classification

Functions to apply the LCCS Level 4 classification to an xarray of data.

The output is a stack with the relevant code for each Level 4 category.

"""

import glob
import logging
import os

import numpy
import xarray

from . import l4_base

# Load standard l4 layers
from . import l4_layers_lccs
from . import l4_layers_AU_modified
from . import l4_layers_LW_modified


# Create list of all level 4 classes, in the order they should be applied
# for the codes
ALL_L4_LAYER_CLASSES_LCCS = [l4_layers_lccs.LCCSLevel3,
                             l4_layers_lccs.LifeformVegCatL4a,
                             l4_layers_lccs.MlifefrmVegCatL4m,
                             l4_layers_lccs.CanopycoVegCatL4d,
                             l4_layers_lccs.CanopyhtVegCatL4d,
                             l4_layers_lccs.WaterdayAgrCatL4a,
                             l4_layers_lccs.WaterseaVegCatL4a,
                             l4_layers_lccs.LeaftypeVegCatL4a,
                             l4_layers_lccs.PhenologVegCatL4a,
                             l4_layers_lccs.MphenlogVegCatL4m,
                             l4_layers_lccs.UrbanvegUrbCatL4a,
                             l4_layers_lccs.SpatdistVegCatL4a,
                             l4_layers_lccs.Strat2ndVegCatL4a,
                             l4_layers_lccs.Lifef2ndVegCatL4a,
                             l4_layers_lccs.Cover2ndVegCatL4d,
                             l4_layers_lccs.Heigh2ndVegCatL4d,
                             l4_layers_lccs.SpatsizeAgrCatL4a,
                             l4_layers_lccs.CropcombAgrCatL4a,
                             l4_layers_lccs.Croplfc2AgrCatL4a,
                             l4_layers_lccs.Croplfc3AgrCatL4a,
                             l4_layers_lccs.CropseqtAgrCatL4a,
                             l4_layers_lccs.CropseqaAgrCatL4a,
                             l4_layers_lccs.WatersupAgrCatL4a,
                             l4_layers_lccs.TimefactAgrCatL4a,
                             l4_layers_lccs.WatersttWatCatL4a,
                             l4_layers_lccs.InttidalWatCatL4a,
                             l4_layers_lccs.WaterperWatCatL4d,
                             l4_layers_lccs.SnowcperWatCatL4d,
                             l4_layers_lccs.WaterdptWatCatL4a,
                             l4_layers_lccs.MwatrmvtWatCatL4m,
                             l4_layers_lccs.WsedloadWatCatL4a,
                             l4_layers_lccs.MsubstrtWatCatL4m,
                             l4_layers_lccs.BaresurfPhyCatL4a,
                             l4_layers_lccs.MbarematPhyCatL4m,
                             l4_layers_lccs.MustonesPhyCatL4m,
                             l4_layers_lccs.MhardpanPhyCatL4m,
                             l4_layers_lccs.MacropatPhyCatL4a,
                             l4_layers_lccs.ArtisurfUrbCatL4a,
                             l4_layers_lccs.MtclineaUrbCatL4m,
                             l4_layers_lccs.MnolineaUrbCatL4m,
                             l4_layers_lccs.MartdensUrbCatL4m,
                             l4_layers_lccs.MnobuiltUrbCatL4m]

#: Classes used standard lccs and modified versions.
LAYER_CLASSES = {"lccs" : ALL_L4_LAYER_CLASSES_LCCS,
                 "au"   : l4_layers_AU_modified.ALL_L4_LAYER_CLASSES_AU_MODIFIED,
                 "lw"   : l4_layers_LW_modified.ALL_L4_LAYER_CLASSES_LW_MODIFIED}


def _get_colour_schemes_csv_dict():
    """
    Function to try and figure out where the CSV with the colour schemes
    are.

    If the library has been installed with setup.py then will be in:
        PREFIX/share/lccs_colours

    If running from repo checkout then will be in:
        CHECKOUT/colour_schemes

    If can't find in either of these locations return None and worry about later.
    Function to apply colour scheme will take path to file as an argument so isn't a
    problem if we don't find here.

    """
    colour_schemes_csv_dict = {}

    install_prefix = __file__[:__file__.find("lib")]
    dev_checkout = __file__.split(os.path.sep)[0:-3]
    dev_checkout = "{}".format(os.path.sep).join(dev_checkout)

    colour_scheme_csv_installed_path = os.path.join(install_prefix, "share",
                                                    "lccs_colours")
    colour_scheme_csv_dev_path = os.path.join(dev_checkout, "colour_schemes")

    if os.path.isdir(colour_scheme_csv_installed_path):
        colour_scheme_csv_list = glob.glob(os.path.join(colour_scheme_csv_installed_path, "*csv"))
    elif os.path.isdir(colour_scheme_csv_dev_path):
        colour_scheme_csv_list = glob.glob(os.path.join(colour_scheme_csv_dev_path, "*csv"))
    else:
        logging.warning("Could not find directory containing level 4 colour schemes")

    for csv_file in colour_scheme_csv_list:
        colour_scheme_name = os.path.splitext(os.path.basename(csv_file))[0]
        colour_scheme_name = colour_scheme_name.replace("lccs_l4_colour_scheme_","")

        colour_schemes_csv_dict[colour_scheme_name] = csv_file

    return colour_schemes_csv_dict


# Dictionary with CSV files containing colour schemes
LCCS_L4_COLOURS_CSV_DICT = _get_colour_schemes_csv_dict()
# TODO: Need golden_dark colour scheme for LivingWales
DEFAULT_LCCS_LC_COLOUR_SCHEMES = {"lccs" : "golden_dark", "au" : "golden_dark_au", "lw" : "lw_LifeCover"}

def classify_lccs_level4(in_values_xarray, level4_classes_list=LAYER_CLASSES["lccs"]):
    """
    Apply standard LCCS classification to an xarray
    """

    classification = None

    for l4_layer_class in level4_classes_list:
        try:
            out_var = l4_layer_class().transform(in_values_xarray, classification)
            
            if classification is None and out_var is not None:
                classification = out_var
            elif out_var is not None:
                classification = xarray.merge([classification, out_var])
            else:
                logging.debug("Could not calculate classification for layer"
                              " '{}'".format(l4_layer_class().output_layer_name))
        except KeyError as err:
            logging.debug("Missing all inputs for layer '{}'. Missing key {}"
                          "".format(l4_layer_class().input_layer_name, err))

    return classification

def get_lccs_level4_code(classification_array, level4_classes_list=LAYER_CLASSES["lccs"]):
    """
    Take classified data and calculate the LCCS Level 4 classification code
    for each.
    """
    # Set up array to hold output codes (string)
    out_class_codes = None

    for l4_layer_class in level4_classes_list:
        try:
            layer_codes = l4_layer_class().get_output_code(classification_array)
        except KeyError:
            logging.debug("Missing classification layer '{}'"
                          "".format(l4_layer_class().output_layer_name))
            continue

        # Append to output class codes
        # TODO: Make sure this is the best way of doing string concatenation
        if out_class_codes is None:
            out_class_codes = layer_codes
        else:
            # If there is a new code for an element (pixel/row) then add delimiter
            # onto the end of the old one.
            delimiter = l4_base.CODE_DELIMITERS[l4_layer_class().layer_type]
            out_class_codes[layer_codes != ""] = numpy.core.defchararray.add(
                out_class_codes[layer_codes != ""], delimiter)
            # Append new code
            out_class_codes = numpy.core.defchararray.add(out_class_codes, layer_codes)

    return out_class_codes

def get_lccs_level4_description(classification_array,
                                level4_classes_list=LAYER_CLASSES["lccs"]):
    """
    Take classified data and calculate the LCCS Level 4 classification description
    for each.
    """
    # Set up array to hold output descriptions (string)
    out_class_descriptions = None

    for l4_layer_class in level4_classes_list:
        try:
            layer_descriptions = l4_layer_class().get_output_description(classification_array)
        except KeyError:
            logging.debug("Missing classification layer '{}'"
                          "".format(l4_layer_class().output_layer_name))
            continue


        # Append to output class descriptions
        # TODO: Make sure this is the best way of doing string concatenation
        if out_class_descriptions is None:
            out_class_descriptions = layer_descriptions
        else:
            # If there is a new description for an element (pixel/row) then add a '.' on
            # the end of the new one.
            out_class_descriptions[layer_descriptions != ""] = numpy.core.defchararray.add(
                out_class_descriptions[layer_descriptions != ""], " ")
            # Append new description
            out_class_descriptions = numpy.core.defchararray.add(out_class_descriptions,
                                                                 layer_descriptions)
    return out_class_descriptions

def get_combined_level4(classification_array, colour_scheme_csv=DEFAULT_LCCS_LC_COLOUR_SCHEMES["lccs"]):
    """"
    Get pixel IDs and colour mapping for singleband version of level 4 classification
    Returns five arrays:

    * pixel_id
    * red
    * green
    * blue
    * alpha

    :param xarray.Dataset classification_array: xarray dataset containing classified level 4 data layers.
    :param str colour_scheme_csv: The name of the colour scheme in LCCS_L4_COLOURS_CSV_DICT if using a
                                  standard colour scheme or the path to a CSV file for a non-standard
                                  scheme.

    """
    # Only need pandas for this function so import here
    # Is needed by xarray so should be installed as part of requirements
    import pandas

    # Get colour scheme to use
    if not os.path.isfile(colour_scheme_csv):
        try:
            colour_scheme_csv = LCCS_L4_COLOURS_CSV_DICT[colour_scheme_csv]
        except KeyError as err:
            raise KeyError("Colour scheme was not a path to a CSV file or a valid "
                           " colour scheme name. Options are:\n {}\nError: {}"
                           "".format(",".join(list(LCCS_L4_COLOURS_CSV_DICT.keys())),
                                     err))

    # Create a list of non-layer names in CSV.
    # For all other columns the heading is assumed to be the layer name
    non_layer_names = ["pixel_ID", "LCCS_Code", "LCCS_Description", "Red", "Green", "Blue", "Alpha"]

    # Read colour scheme CSV file
    if colour_scheme_csv is None:
        raise Exception("Could not find colour scheme CSV files in"
                        " expected location and one not supplied to colour_lccs_level4")

    colour_scheme = pandas.read_csv(colour_scheme_csv)

    # Set up arrays for output
    red = numpy.zeros_like(classification_array[l4_base.LEVEL3_LAYER_NAME].values,
                           dtype=numpy.uint8)
    green = numpy.zeros_like(red)
    blue = numpy.zeros_like(red)
    alpha = numpy.zeros_like(red)
    pixel_id = numpy.zeros_like(red)

    # Iterate through rows colour scheme
    for _, line in colour_scheme.iterrows():

        # If the column is a layer name add values to
        # 'layer_val_list'
        layer_val_list = []
        for layer, val in line.items():
            if layer not in non_layer_names:
                layer_val_list.append((layer, val))

        # Using 'layer_val_list' get subset mask of elements (pixels) which
        # should be coloured
        subset = l4_base.get_colours_mask(classification_array, layer_val_list)

        # Colour output arrays
        pixel_id[subset] = line["pixel_ID"]
        red[subset] = line["Red"]
        green[subset] = line["Green"]
        blue[subset] = line["Blue"]
        alpha[subset] = line["Alpha"]

    return pixel_id, red, green, blue, alpha

