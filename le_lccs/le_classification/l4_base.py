"""
Base classes for Level 4 classification layers.
"""
from __future__ import print_function
from __future__ import division

from abc import ABCMeta, abstractmethod
import copy
from enum import Enum
import logging

import numpy
import xarray

LEVEL3_LAYER_NAME = "level3"

class ClassificationLayerTypes(Enum):
    MAIN = 1
    MODIFIER = 2
    DERIVATIVE = 3

CODE_DELIMITERS = {ClassificationLayerTypes.MAIN : ".",
                   ClassificationLayerTypes.MODIFIER : "_",
                   ClassificationLayerTypes.DERIVATIVE : "."}

class Level4ClassificationLayer(object):
    """
    Base class for a single output layer in level 4 classification.
    """

    __metaclass__ = ABCMeta

    input_layer_name = None
    input_layer_uncertainty_name = None
    output_layer_name = None
    output_layer_uncertainty_name = None
    output_codes_descriptions = None
    class_boundaries = None
    # List of valid level 3 classes to run the classification for. Set to None for all classes
    valid_level3_classes = None
    # List of level4 filters in the form "(waterstt_wat_cat == 1 ) & (inttidal_wat_cat != 3)"
    valid_level4_layers_filters = None
    layer_type = None

    @abstractmethod
    def transform(self, in_values_xarray, out_values_xarray=None):
        """
        Abstract function to transform input values and return output
        for layer.
        """
        pass

    @abstractmethod
    def check_inputs(self, in_values_xarray):
        """
        Abstract function to check input values for array.
        """
        pass

    @abstractmethod
    def get_valid_input_values(self):
        """
        Abstract function to get valid input values for array
        """
        pass

    @staticmethod
    def get_uncertainty_layer_name(layer_name):
        """
        Gets the name of standard uncertainty layer name coresponding
        to input layer.
        """
        return "{}_unc".format(layer_name)

    def get_output_code(self, classification_array):
        """
        Function to get output codes.

        :param xarray.Dataset classification_array: Input xarray dataset containing classified data.
        :returns: 4 character numpy array with output code for each element.
        :rtype: numpy.array

        """
        if self.output_codes_descriptions is None:
            raise Exception("The output codes (output_codes) are not defined for the class")
        if self.output_layer_name is None:
            raise Exception("The output layer name (output_layer_name) is not defined for the"
                            " class")

        # Set up an array of 4 character strings to hold output values
        output_values = numpy.empty(classification_array[self.output_layer_name].values.shape,
                                    dtype="U4")

        for code_int, code_str in self.output_codes_descriptions.items():
            mask = (classification_array[self.output_layer_name].values == code_int)
            output_values[mask] = code_str[0]
        return output_values

    def get_output_description(self, classification_array):
        """
        Function to get output descriptions.

        :param xarray.Dataset classification_array: Input xarray dataset containing classified data.
        :returns: 255 character numpy array with output code for each element.
        :rtype: numpy.array

        """
        if self.output_codes_descriptions is None:
            raise Exception("The output descriptions (output_descriptions) are not"
                            " defined for the class")
        if self.output_layer_name is None:
            raise Exception("The output layer name (output_layer_name) is not defined for the"
                            " class")

        # Set up an array of 255 character strings to hold output values
        output_values = numpy.empty(classification_array[self.output_layer_name].values.shape,
                                    dtype="U100")

        for code_int, code_str in self.output_codes_descriptions.items():
            mask = (classification_array[self.output_layer_name].values == code_int)
            output_values[mask] = code_str[1]

        return output_values

    def _get_layer_mask(self, out_values_xarray):
        """
        Get mask of valid pixels

        :param xarray.Dataset out_values_xarray: Input xarray dataset containing output layers (including for class but unmasked)
        :returns: boolean numpy array. True are values to be masked out, False are valid data.
        :rtype: numpy.array

        """
        # Set up output mask
        mask = numpy.empty(out_values_xarray[self.output_layer_name].values.shape,
                           dtype=numpy.bool_)
        # Initiate all values to be True (mask all values out). Will go through
        # and set to False if they match the required criteria.
        mask[...] = True

        # Go through all valid level 3 classes.
        for l3_class in self.valid_level3_classes:
            subset = None
            # Mask using only level 3 classes
            if self.valid_level4_layers_filters is None:
                subset = (out_values_xarray[LEVEL3_LAYER_NAME].values == l3_class)
            # Mask using level4 and level3
            else:
                # Set up dictionary of all variables in input xarray
                expr_vars = {}
                for var_name in out_values_xarray.data_vars:
                    expr_vars[var_name] = out_values_xarray[var_name]

                for expression in self.valid_level4_layers_filters:
                    # Apply band maths to create subset     
                    expr_subset = eval(expression, {"numpy" : numpy}, expr_vars)
                    if subset is None:
                        subset = expr_subset
                    else:
                        subset = numpy.where(expr_subset, expr_subset, subset)

                # After all the level 4 values have been checked look at the level 3 class.
                # If it matches the level 3 class values remain unchanged, it it doesn't set
                # to False
                subset = numpy.where(out_values_xarray[LEVEL3_LAYER_NAME].values == l3_class,
                                     subset, False)

            # Update the mask based on selection.
            # subset is an array where True means it matches the level 3 and level 4 classes we want
            # mask is an array where True means the value is to be masked out.
            # So we update the mask to set the values we want to False leaving anything which hasn't
            # matched a level 3 or level 4 class we are interested in as True.
            mask[subset] = False

        return mask

class Level4ClassificationCatergoricalLayer(Level4ClassificationLayer):
    """
    Base class for a single output cater categorical layer in level 4 classification.
    """

    __metaclass__ = ABCMeta

    def transform(self, in_values_xarray, out_values_xarray=None):
        """
        Abstract function to transform input values and return output
        for layer.
        """
        # Check input values
        self.check_inputs(in_values_xarray)

        # Set up numpy array for output
        out_vals = numpy.empty(in_values_xarray[self.input_layer_name].shape,
                               dtype=numpy.uint8)

        # Copy in values
        out_vals = copy.copy(in_values_xarray[self.input_layer_name].values)

        # Set up output array containing copy of input values
        out_xarray = xarray.Dataset(
            {self.output_layer_name : (in_values_xarray[self.input_layer_name].dims, out_vals)},
            coords=in_values_xarray.coords)

        # Check don't have level 4 restriction but not level 3.
        if self.valid_level3_classes is None and self.valid_level4_layers_filters is not None:
            raise Exception("Can't provide valid_level4_layers_filters without "
                            "valid_level3_classes")
        # If not apply a series of subsets to determine the values to mask out.
        if self.valid_level3_classes is not None:
            mask = self._get_layer_mask(xarray.merge([out_values_xarray, out_xarray]))
            # Apply the mask. Any values which didn't match the classes we are interested in get
            # set to 0.
            out_xarray[self.output_layer_name].values[mask] = 0

        return out_xarray

    def check_inputs(self, in_values_xarray):
        """
        Check inputs are all expected values.
        """
        if self.input_layer_name is None:
            raise Exception("The input layer name (input_layer_name) is not defined for the"
                            " class")
        if self.output_codes_descriptions is None:
            raise Exception("The output codes and descriptions dictionary"
                            " (output_codes_descriptions) is not defined for the"
                            " class")

        # Get a list of unique values in input layer
        unique_values = numpy.unique(in_values_xarray[self.input_layer_name].values)

        for input_val in unique_values:
            if input_val not in self.output_codes_descriptions.keys() \
              and not numpy.isnan(input_val) and int(input_val) != 0:
                raise ValueError("The input layer {} contains an unexpected value: {}\n"
                                 "Expected values are: {}"
                                 "".format(self.input_layer_name,
                                           input_val,
                                           self.output_codes_descriptions.keys()))

    def get_valid_input_values(self):
        """
        Get valid input values for layer.

        :returns: a list of values
        :rtype: list
        """
        if self.output_codes_descriptions is None:
            raise Exception("The output codes and descriptions dictionary"
                            " (output_codes_descriptions) is not defined for the"
                            " class")

        return list(self.output_codes_descriptions.keys())

class Level4ClassificationContinuousLayer(Level4ClassificationLayer):
    """
    Base class for a single output cater categorical layer in level 4 classification.
    """

    __metaclass__ = ABCMeta

    def transform(self, in_values_xarray, out_values_xarray=None):
        """
        Abstract function to transform input values and return output
        for layer.
        """
        # Check input values
        self.check_inputs(in_values_xarray)

        # Set up numpy array for output
        out_vals = numpy.zeros(in_values_xarray[self.input_layer_name].shape,
                               dtype=numpy.uint8)

        # Set up output xarray
        out_xarray = xarray.Dataset(
            {self.output_layer_name : (in_values_xarray[self.input_layer_name].dims, out_vals)},
            coords=in_values_xarray.coords)

        # Assign codes based on minimum and maximum values
        for code, min_max in self.class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            if min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0))
            out_xarray[self.output_layer_name].values[subset] = code

        # Set 0 or NaN numbers in input to 0 in output.
        subset = ((in_values_xarray[self.input_layer_name].values == 0) | (numpy.isnan(in_values_xarray[self.input_layer_name].values)))
        out_xarray[self.output_layer_name].values[subset] = 0

        # Check don't have level 4 restriction but not level 3.
        if self.valid_level3_classes is None and self.valid_level4_layers_filters is not None:
            raise Exception("Can't provide valid_level4_layers_filters without valid_level3_classes")
        # Masking based on level3 classes (if needed)
        elif self.valid_level3_classes is not None:
            # Get a mask based on valid level3 and level4 classes
            mask = self._get_layer_mask(xarray.merge([out_values_xarray, out_xarray]))

            # Apply the mask. Any values which didn't match the classes we are interested in get
            # set to 0.
            out_xarray[self.output_layer_name].values[mask] = 0

        return out_xarray

    def check_inputs(self, in_values_xarray):
        """
        Check inputs are all expected values by using class_boundries.
        """
        if self.input_layer_name is None:
            raise Exception("The input layer name (input_layer_name) is not defined for the"
                            " class")

        if self.class_boundaries is None:
            raise Exception("The class boundaries dictionary (class_boundaries) is not"
                            " defined for the class")

        class_values = []
        no_min = False
        no_max = False

        for code, min_max in self.class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            elif min_max[0] is None:
                no_min = True
                class_values.append(min_max[1])
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                no_max = True
                class_values.append(min_max[0])
            else:
                class_values.extend([min_max[0], min_max[1]])

        if not no_min:
            if numpy.any(((in_values_xarray[self.input_layer_name].values < min(class_values)) &
                         (in_values_xarray[self.input_layer_name].values != 0))):
                raise ValueError("The input layer {} contains a value below the minimum"
                                 " threshold of {}".format(self.input_layer_name,
                                                           min(class_values)))
        if not no_max:
            if numpy.any(in_values_xarray[self.input_layer_name].values > max(class_values)):
                raise ValueError("The input layer {} contains a value above the maximum"
                                 " threshold of {}".format(self.input_layer_name,
                                                           max(class_values)))

    def get_valid_input_values(self):
        """
        Get valid input values for layer. As looking at continuous data rather
        than returning the entire range between the minimum and maximum value
        take a value between each set of bounds.

        When an upper bound isn't set use lower bound + (lower bound / 2).
        Where a lower bound isn't set use upper bound - (upper bound / 2).

        :returns: a list of values
        :rtype: list

        """
        if self.class_boundaries is None:
            raise Exception("The class boundaries dictionary (class_boundaries) is not"
                            " defined for the class")

        valid_input_values = []

        for code, min_max in self.class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            elif min_max[0] is None:
                valid_input_values.append(min_max[1] - (min_max[1] / 2))
            elif min_max[1] is None:
                valid_input_values.append(min_max[0] + (min_max[0] / 2))
            else:
                valid_input_values.append((min_max[0] + min_max[1]) / 2)

        return valid_input_values

def get_colours_mask(classification_array, layer_val_list):
    """
    Get mask of valid pixels

    :param xarray.Dataset classification_array: xarray dataset containing classified level 4 data layers.
    :param list layer_val_list: A list containing tuples of level4_layer, value
    :returns: boolean numpy array. True matches all the input layers and values
    :rtype: numpy.array

    """
    subset = None

    # Go through all valid level 3 classes.
    for layer_name, layer_val in layer_val_list:
        # If value is NaN (missing rather than 0) then skip
        # Try too be robust to formatting problems with colour scheme
        # CSV file
        valid_number = False
        try:
            if not numpy.isnan(float(layer_val)):
                valid_number = True
        except (TypeError, ValueError):
            # If it contains a string with just spaces (which ', ,' will be
            # parsed as). Then assume this should be a no data value.
            if isinstance(layer_val, str) and layer_val.strip() == "":
                pass
            # No idea what is the value is in this case so best to just
            # raise an exception.
            else:
                raise TypeError("Was expecting an integer value for {}. Value of '{}'"
                                " is type {}".format(layer_name, str(layer_val), str(type(layer_val))))
        if valid_number:
            try:
                if subset is None:
                    # For first layer assign True if it matches first condition
                    subset = (classification_array[layer_name].values == int(layer_val))
                    subset = (classification_array[layer_name].values == layer_val)
                else:
                    # For subsequent layers assign True if it has matched previous conditions
                    # and matches current one, else assigne to False
                    subset = numpy.where((classification_array[layer_name].values == layer_val) & \
                                         subset, True, False)
            except KeyError:
                # If required layer is not in classification
                logging.warning("Missing layer {} needed for colours, skipping"
                                "".format(layer_name))
                # Return a value of false, then this is used as a mask won't match
                # any values
                return False

    return subset

