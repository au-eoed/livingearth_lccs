"""
LCCS Level 4 Classification Layers

Layers needed to perform FAO LCCS version 2 Level 4 classification.

Di Gregorio, A (2005). Land Cover Classification System: Classification concepts and user manual. Software
version 2. Food and Agriculture Organization of the United Nations

Some layers differ from FAO LCCS version 2 to ensure appropriate application using EO data.

IMPORTANT: All level 4 codes currently align to semi natural vegetation (112).
These are documented in the docstring for each class to show deviation from FAO.
This was deemed the most efficient way as the changing of codes to align with FAO is nonsensical.

For example, all lifeform codes (i.e. A1-A9) in l4_layers_lccs.py are semi natural vegetation (112) codes,
as the descriptions for 111, 123, 124 are the same.

Although a simple solution would be to remove all FAO codes and just have descriptors,
these serve as a validation of descriptors and are currently required for assigning colour scheme.

NOTE: Several level 4 classes have dependencies on other level 4 classes (i.e. MlifefrmVegCatL4m
has dependency on LifeformVegCatL4a). These are differentiated in docstrings from Permitted additional attributes
specifically because, although these get implementing the same way in the valid_level4_layers_filters, dependencies
must be adhered to regardless of data availability. Permitted additional attributes are recorded based on
realistic land cover classes in the landscape to ensure the LCCS system is coherent. However, this may
cause issues depending on data availability and hence have been recorded separately so the user can
manipulate these if required (i.e. data is not available for LifeformVegCatL4a however CanopyhtVegCatL4d
is avaliable and the user wants to include this despite LifeformVegCatL4a being a filter on CanopyhtVegCatL4d)

Each class has a 'Tier' description associated with its hierarchical position in level 4 as a descriptor. These
have been indicated in the docstring for each class (see Di Gregorio, 2005 for details).
For the tier system to function, additional categories were added to some level 4 classes to ensure
that categories from classes higher in the hierarchy were not excluded from additional descriptions
if they were not applicable for the particular class. For example, LifeformVegCatL4a == 2 (herbaceous)
cannot have LeaftypeVegCatL4a, however LeaftypeVegCatL4a (Tier 2) is required for SpatdistVegCatL4a (Tier 3).
Therefore additional blank ("") categories have been added where required, and are well documented for each
class below where they have been applied. These classes include LeaftypeVegCatL4a, PhenologVegCatL4a and
SpatdistVegCatL4a.
"""

import logging

import numpy
import xarray

from . import l4_base

class LCCSLevel3(l4_base.Level4ClassificationCatergoricalLayer):
    """
    FAO LCCS Level 3 class
    """
    def __init__(self):
        self.input_layer_name = l4_base.LEVEL3_LAYER_NAME
        self.output_layer_name = l4_base.LEVEL3_LAYER_NAME
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {111 : ("A11", "Cultivated Terrestrial Vegetated:"),
                                          112 : ("A12", "Natural Terrestrial Vegetated:"),
                                          123 : ("A23", "Cultivated Aquatic Vegetated:"),
                                          124 : ("A24", "Natural Aquatic Vegetated:"),
                                          215 : ("B15", "Artificial Surface:"),
                                          216 : ("B16", "Natural Surface:"),
                                          220 : ("B20", "Water:"),
                                          227 : ("B27", "Artificial Water:"),
                                          228 : ("B28", "Natural Water:")}
        # Set valid level3 classes to None (use all)
        self.valid_level3_classes = None
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN



class LifeformVegCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Lifeform (lifeform_veg_cat_l4a) of the main strata
    All codes allign to semi natural vegetation (112)

    New codes for other vegetation classes (what they are in FAO, what allign to for (112)
    Any not listed below are identical descriptors and codes as (112)

    Cultivated terrestrial vegetation (111): Trees (A1) --> (A3)
                                             Shrub (A2) --> (A4)
                                             Herbaceous (A3) --> (A2)
                                             Graminoids (A4) --> (A6)
                                             Non-graminoids (A5) --> (A5)
                                             Urban vegetated (A6) --> UrbanvegUrbCatL4a (A6)

        Cultivated aquatic vegetation (123): Graminoid (A1) --> (A6)
                                             Non-graminoids (A2) --> (A5)
                                             Woody (A3) --> (A1)

           Natural aquatic vegetation (124): Rooted forbs (A8) --> MlifefrmVegCatL4m (A8)
                                             Free floating forbs (A9) --> MlifefrmVegCatL4m (A9)
                                             Lichens (A10) --> (A8)
                                             Mosses (A11) --> (A9)

    Tier 1 class (111, 112, 123, 124)

    Dependencies for class
    if lifeform_veg_cat_l4a == 1 | 2 | 5 | 6
        level3 == 111 | 112 | 123 | 124
    if lifeform_veg_cat_l4a == 3 | 4
        level3 == 111 | 112 | 124
    if lifeform_veg_cat_l4a == 7 | 8 | 9
        level3 == 112 | 124

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "lifeform_veg_cat"
        self.output_layer_name = "lifeform_veg_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("A1", "Woody"),
                                          2 : ("A2", "Herbaceous"),
                                          3 : ("A3", "Trees"),
                                          4 : ("A4", "Shrubs"),
                                          5 : ("A5", "Forbs"),
                                          6 : ("A6", "Graminoids"),
                                          7 : ("A7", "Lichens/mosses"),
                                          8 : ("A8", "Lichens"),
                                          9 : ("A9", "Mosses")}
        self.valid_level3_classes = [111, 112, 123, 124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 2) | \
             (lifeform_veg_cat_l4a == 5) | (lifeform_veg_cat_l4a == 6)) & \
             ((level3 == 111) | (level3 == 112) | (level3 == 123) | (level3 == 124))",
            "((lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4)) & \
             ((level3 == 111) | (level3 == 112) | (level3 == 124))",
            "((lifeform_veg_cat_l4a >= 7) & (lifeform_veg_cat_l4a <= 9)) & \
             ((level3 == 112) | (level3 == 124))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class MlifefrmVegCatL4m(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Lifeform of aquatic forbs (Mlifefrm_veg_cat_l4m)

    Tier 1 class (124)

    Dependencies for class
    lifeform_veg_cat_l4a == 5

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "mlifefrm_veg_cat"
        self.output_layer_name = "mlifefrm_veg_cat_l4m"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {8 : ("A8", "Rooted forbs"),
                                          9 : ("A9", "Free floating forbs")}
        self.valid_level3_classes = [124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["lifeform_veg_cat_l4a == 5"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MODIFIER


class CanopycoVegCatL4d(l4_base.Level4ClassificationContinuousLayer):
    """
    Canopy cover (canopyco_veg_cat_l4d) DERIVATIVE

    Class boundaries have been altered to middle of range i.e. 10-20 == 15

    Excludes the following classes from the FAO LCCS class:
    * A11: open (10-20 to 60-70 %)
    * A14: sparse (1 to 10-20 %)
    * A20: closed to open (15-100 %)
    * A21: closed to open (40-100 %)

    Tier 1 class (111, 112, 123, 124)

    Dependencies for class
    none

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "canopyco_veg_con"
        self.output_layer_name = "canopyco_veg_cat_l4d"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {10 : ("A10", "Closed (> 65 %)"),
                                          12 : ("A12", "Open (40 to 65 %)"),
                                          13 : ("A13", "Open (15 to 40 %)"),
                                          15 : ("A15", "Sparse (4 to 15 %)"),
                                          16 : ("A16", "Scattered (1 to 4 %)")}
        self.class_boundaries = {10 : (65, None),
                                 12 : (40, 65),
                                 13 : (15, 40),
                                 15 : (4, 15),
                                 16 : (1, 4),
                                 0  : (0, 1)}
        self.valid_level3_classes = [111, 112, 123, 124]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.DERIVATIVE


class CanopyhtVegCatL4d(l4_base.Level4ClassificationContinuousLayer):
    """
    Canopy height (canopyht_veg_cat_l4d)

    Modifed from FAO, permitting canopyht_veg_cat_l4d layer to not require
    lifeform_veg_cat_l4a (undefined_lifeform). This decision was due to likely
    avaliability of canopyht_veg_cat_l4d for EO where lifeform_veg_cat_l4a may not
    be generated as rountinely.

    NOTE: FAO LCCS describe woody as 2 to 7 m, however EO data often can differentiate only
    woody (i.e. tree and shrub) and non woody (i.e. herbaceous). There the definition
    used here it so give a broad class that encompasses tree and shrub as woody.
    Therefore categories modified from FAO so that woody (encompassing tree and shrub)
    did not have overlapping height categories.
    Original FAO B7: 3 to 7 m --> modified FAO B7: 5 to 7 m
    Therefore height categories are woody (B5 - B10), tree (B5 - B8) and
    shrub (B8 - B10). FAO defines trees as >= 3 m, however modified FAO
    for EO implementation defines trees as >= 2 m (similar to forestry and
    ecology definitions)

    Tier 1 class (111, 112, 123, 124)

    Dependencies for class
    none

    Permitted additional attributes
    if canopyht_veg_cat_l4d == 5 | 6 | 7
        lifeform_veg_cat_l4a == 0| 1 | 3
    if canopyht_veg_cat_l4d == 8
        lifeform_veg_cat_l4a == 0| 1 | 3 | 4
    if canopyht_veg_cat_l4d == 9 | 10
        lifeform_veg_cat_l4a == 0 | 1 | 4
    if canopyht_veg_cat_l4d == 11
        lifeform_veg_cat_l4a == 2 | 5 | 6
    if canopyht_veg_cat_l4d == 12
        lifeform_veg_cat_l4a == 2 | 5 | 6 | 7 | 9
    if canopyht_veg_cat_l4d == 13
        lifeform_veg_cat_l4a == 2 | 5 | 6 | 7 | 8 | 9
    """
    def __init__(self):
        self.input_layer_name = "canopyht_veg_con"
        self.output_layer_name = "canopyht_veg_cat_l4d"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {5 : ("B5", "(> 14 m)"),
                                          6 : ("B6", "(7 to 14 m)"),
                                          7 : ("B7", "(5 to 7 m)"),
                                          8 : ("B8", "(2 to 5 m)"),
                                          9 : ("B9", "(0.5 to 2 m)"),
                                          10 : ("B10", "(< 0.5 m)"),
                                          11 : ("B11", "(0.8 to 3 m)"),
                                          12 : ("B12", "(0.3 to 0.8 m)"),
                                          13 : ("B13", "(0.03 to 0.3 m)")}

        self.undefined_lifeform_class_boundaries = {5 : (14, None),
                                                    6 : (7, 14),
                                                    7 : (5, 7),
                                                    8 : (2, 5),
                                                    9 : (0.5, 2),
                                                    10 : (None, 0.5)}

        self.woody_class_boundaries = {5 : (14, None),
                                       6 : (7, 14),
                                       7 : (5, 7),
                                       8 : (2, 5),
                                       9 : (0.5, 2),
                                       10 : (None, 0.5)}

        self.trees_class_boundaries = {5 : (14, None),
                                       6 : (7, 14),
                                       7: (5, 7),
                                       8: (2, 5)}

        self.shrubs_class_boundaries = {8 : (2, 5),
                                        9 : (0.5, 2),
                                        10 : (None, 0.5)}

        self.herbaceous_class_boundaries = {11 : (0.8, 3),
                                            12 : (0.3, 0.8),
                                            13 : (0.03, 0.3)}

        self.forbs_class_boundaries = {11 : (0.8, 3),
                                       12 : (0.3, 0.8),
                                       13 : (0.03, 0.3)}

        self.graminoids_class_boundaries = {11 : (0.8, 3),
                                            12 : (0.3, 0.8),
                                            13 : (0.03, 0.3)}

        self.lichens_mosses_class_boundaries = {12 : (0.3, 0.8),
                                                13 : (0.03, 0.3)}

        self.lichens_class_boundaries = {13 : (0.03, 0.3)}

        self.mosses_class_boundaries = {12 : (0.3, 0.8),
                                        13 : (0.03, 0.3)}

        # Create a single dictionary with all class boundaries
        # used to check valid inputs.
        self.class_boundaries = {}
        self.class_boundaries.update(self.undefined_lifeform_class_boundaries)
        self.class_boundaries.update(self.woody_class_boundaries)
        self.class_boundaries.update(self.trees_class_boundaries)
        self.class_boundaries.update(self.shrubs_class_boundaries)
        self.class_boundaries.update(self.herbaceous_class_boundaries)
        self.class_boundaries.update(self.forbs_class_boundaries)
        self.class_boundaries.update(self.graminoids_class_boundaries)
        self.class_boundaries.update(self.lichens_mosses_class_boundaries)
        self.class_boundaries.update(self.lichens_class_boundaries)
        self.class_boundaries.update(self.mosses_class_boundaries)

        self.valid_level3_classes = [111, 112, 123, 124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((canopyht_veg_cat_l4d >= 5) & (canopyht_veg_cat_l4d <= 7)) & \
             ((lifeform_veg_cat_l4a == 0) | (lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3))",
            "(canopyht_veg_cat_l4d == 8) & \
             ((lifeform_veg_cat_l4a == 0) | (lifeform_veg_cat_l4a == 1) | \
             (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4))",
            "((canopyht_veg_cat_l4d == 9) | (canopyht_veg_cat_l4d == 10)) & \
             ((lifeform_veg_cat_l4a == 0) | (lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 4))",
            "(canopyht_veg_cat_l4d >= 11) & \
             ((lifeform_veg_cat_l4a == 2) | (lifeform_veg_cat_l4a == 5) | (lifeform_veg_cat_l4a == 6))",
            "(canopyht_veg_cat_l4d == 12) & \
             ((lifeform_veg_cat_l4a == 2) | ((lifeform_veg_cat_l4a >= 5) & \
             (lifeform_veg_cat_l4a <= 7)) | (lifeform_veg_cat_l4a == 9))",
            "(canopyht_veg_cat_l4d == 13) & \
             ((lifeform_veg_cat_l4a == 2) | ((lifeform_veg_cat_l4a >= 5) & (lifeform_veg_cat_l4a <= 9)))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.DERIVATIVE


    def transform(self, in_values_xarray, out_values_xarray=None):
        """
        Transform function. Needs to be specific transform to include other classes.
        Note that the deviation of the class boundaries from the original FAO LCCS
        """

        # Check input values
        self.check_inputs(in_values_xarray)

        # Set up numpy array for output
        out_vals = numpy.zeros(in_values_xarray[self.input_layer_name].shape,
                               dtype=numpy.uint8)

        # Set up output xarray
        out_xarray = xarray.Dataset(
            {self.output_layer_name: (in_values_xarray[self.input_layer_name].dims, out_vals)},
            coords=in_values_xarray.coords)

        # Calculate for undefined_lifeform (code=0 in lifeform_veg_cat; i.e, undefined_lifeform)
        for code, min_max in self.undefined_lifeform_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 0))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 0))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 0))
            out_xarray[self.output_layer_name].values[subset] = code

        # Set up output xarray
        out_xarray = xarray.Dataset(
            {self.output_layer_name : (in_values_xarray[self.input_layer_name].dims, out_vals)},
            coords=in_values_xarray.coords)

        # Calculate for woody (code=1 in lifeform_veg_cat; i.e, woody)
        for code, min_max in self.woody_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 1))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 1))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 1))
            out_xarray[self.output_layer_name].values[subset] = code

        # Calculate for trees (code=3 in lifeform_veg_cat; i.e, tree)
        for code, min_max in self.trees_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 3))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 3))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 3))
            out_xarray[self.output_layer_name].values[subset] = code

        # Calculate for shrubs (code=4 in lifeform_veg_cat; i.e, shrub)
        for code, min_max in self.shrubs_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 4))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 4))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 4))
            out_xarray[self.output_layer_name].values[subset] = code

        # Calculate for herbaceous (code=2 in lifeform_veg_cat; i.e, herbaceous)
        for code, min_max in self.herbaceous_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 2))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 2))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 2))
            out_xarray[self.output_layer_name].values[subset] = code

        # Calculate for forbs (code=5 in lifeform_veg_cat; i.e, forbs)
        for code, min_max in self.forbs_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 5))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 5))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 5))
            out_xarray[self.output_layer_name].values[subset] = code

        # Calculate for graminoids (code=6 in lifeform_veg_cat; i.e, graminoids)
        for code, min_max in self.graminoids_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 6))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 6))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 6))
            out_xarray[self.output_layer_name].values[subset] = code

        # Calculate for lichens/mosses (code=7 in lifeform_veg_cat; i.e, lichens/mosses)
        for code, min_max in self.lichens_mosses_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 7))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 7))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 7))
            out_xarray[self.output_layer_name].values[subset] = code

        # Calculate for lichens (code=8 in lifeform_veg_cat; i.e, lichens)
        for code, min_max in self.lichens_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 8))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 8))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 8))
            out_xarray[self.output_layer_name].values[subset] = code

        # Calculate for mosses (code=9 in lifeform_veg_cat; i.e, mosses)
        for code, min_max in self.mosses_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 9))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 9))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (out_values_xarray["lifeform_veg_cat_l4a"].values == 9))
            out_xarray[self.output_layer_name].values[subset] = code


        # Get mask
        mask = self._get_layer_mask(xarray.merge([out_values_xarray, out_xarray]))
        # Apply the mask. Any values which didn't match the classes we are interested in get
        # set to 0.
        out_xarray[self.output_layer_name].values[mask] = 0

        return out_xarray


class WaterdayAgrCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Daily water supply for cultivated aquatic vegetation (waterday_agr_cat_l4a)

    Tier 2 class (123)

    Dependencies for class
    lifeform_veg_cat_l4a > 0
    canopyco_veg_cat_l4d > 0
    canopyht_veg_cat_l4d > 0

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "waterday_agr_cat"
        self.output_layer_name = "waterday_agr_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("C1", "Water (persistent for whole day)"),
                                          2 : ("C2", "Water (with daily variations)"),
                                          3 : ("C3", "Waterlogged")}
        self.valid_level3_classes = [123]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "(lifeform_veg_cat_l4a > 0) & (canopyco_veg_cat_l4d > 0) & (canopyht_veg_cat_l4d > 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class WaterseaVegCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Water seasonality for natural aquatic vegetation (watersea_veg_cat_l4a)

    Tier 2 class (124)

    Dependencies for class
    lifeform_veg_cat_l4a > 0
    canopyco_veg_cat_l4d > 0
    canopyht_veg_cat_l4d > 0

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "watersea_veg_cat"
        self.output_layer_name = "watersea_veg_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("C1", "Water > 3 months (semi-) permenant"),
                                          2 : ("C2", "Water < 3 months (temporary or seasonal)"),
                                          3 : ("C3", "Waterlogged"),
                                          4 : ("C4", "Water > 3 months (persistent all day)"),
                                          5 : ("C5", "Water > 3 months (with daily variations)")}
        self.valid_level3_classes = [124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "(lifeform_veg_cat_l4a > 0) & (canopyco_veg_cat_l4d > 0) & (canopyht_veg_cat_l4d > 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class LeaftypeVegCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Leaf type (leaftype_veg_cat_l4a)

    Tier 2 class (111, 112), Tier 3 class (124)
    Addition of category (D4) for all excluded categories in Tier 1 (111, 112), Tier 2 (124)

    Dependencies for class
    if leaftype_veg_cat_l4a == 1 | 2
        level3 == 111 | 112
        lifeform_veg_cat_l4a == 1 | 3 | 4
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if leaftype_veg_cat_l4a == 3
        level3 == 112
        lifeform_veg_cat_l4a == 1 | 3 | 4
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if leaftype_veg_cat_l4a == 1 | 2 | 3
        level3 == 124
        lifeform_veg_cat_l4a == 1 | 3 | 4
        watersea_veg_cat_l4a > 0

    if leaftype_veg_cat_l4a == 4
        level3 == 111 | 112
        lifeform_veg_cat_l4a == 2 | 5 | 6 | 7 | 8 | 9
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if leaftype_veg_cat_l4a == 4
        level3 == 124
        lifeform_veg_cat_l4a == 2 | 5 | 6 | 7 | 8 | 9
        watersea_veg_cat_l4a > 0

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "leaftype_veg_cat"
        self.output_layer_name = "leaftype_veg_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("D1", "Broad-leaved"),
                                          2 : ("D2", "Needle-leaved"),
                                          3 : ("D3", "Aphyllous"),
                                          4 : ("D4", "")} # all excluded categories
        self.valid_level3_classes = [111, 112, 124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((leaftype_veg_cat_l4a == 1) | (leaftype_veg_cat_l4a == 2)) & \
             ((level3 == 111) | (level3 == 112)) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4)) & \
             (canopyco_veg_cat_l4d > 0) & (canopyht_veg_cat_l4d > 0)",
            "(leaftype_veg_cat_l4a == 3) & \
             (level3 == 112) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4)) & \
             (canopyco_veg_cat_l4d > 0) & (canopyht_veg_cat_l4d > 0)",
            "((leaftype_veg_cat_l4a == 1) | (leaftype_veg_cat_l4a == 2) | (leaftype_veg_cat_l4a == 3)) & \
             (level3 == 124) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4)) & \
             (watersea_veg_cat_l4a > 0)",
            "(leaftype_veg_cat_l4a == 4) & \
             ((level3 == 111) | (level3 == 112)) & \
             ((lifeform_veg_cat_l4a == 2) | ((lifeform_veg_cat_l4a >= 5) & (lifeform_veg_cat_l4a <= 9))) & \
             (canopyco_veg_cat_l4d > 0) & (canopyht_veg_cat_l4d > 0)",
            "(leaftype_veg_cat_l4a == 4) & \
             (level3 == 124) & \
             ((lifeform_veg_cat_l4a == 2) | ((lifeform_veg_cat_l4a >= 5) & (lifeform_veg_cat_l4a <= 9))) & \
             (watersea_veg_cat_l4a > 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class PhenologVegCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Phenology (phenolog_veg_cat_l4a)

    Tier 2 class (111, 112), Tier 3 class (124)
    Addition of category (E6) for all excluded categories in Tier 1 (111, 112), Tier 2 (124)

    Dependencies for class
    if phenolog_veg_cat_l4a == 1 | 2 | 3
        level3 == 111 | 112
        lifeform_veg_cat_l4a == 1 | 3 | 4
        leaftype_veg_cat_l4a == 1 | 2
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if phenolog_veg_cat_l4a == 1 | 2 | 3
        level3 == 124
        lifeform_veg_cat_l4a == 1 | 3 | 4
        leaftype_veg_cat_l4a == 1 | 2
        watersea_veg_cat_l4a > 0
    if phenolog_veg_cat_l4a == 5
        level3 == 112
        lifeform_veg_cat_l4a == 2 | 5 | 6
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if phenolog_veg_cat_l4a == 5
        level3 == 124
        lifeform_veg_cat_l4a == 2 | 5 | 6
        watersea_veg_cat_l4a > 0

    if phenolog_veg_cat_l4a == 6
        level3 == 111 | 112
        lifeform_veg_cat_l4a == 1 | 3 | 4
        leaftype_veg_cat_l4a == 0 | 3
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if phenolog_veg_cat_l4a == 6
        level3 == 124
        lifeform_veg_cat_l4a == 1 | 3 | 4
        leaftype_veg_cat_l4a == 0 | 3
        watersea_veg_cat_l4a > 0
    if phenolog_veg_cat_l4a == 6
        level3 == 111 | 112
        lifeform_veg_cat_l4a == 2 | 5 | 6 | 7 | 8 | 9
        canopyco_veg_cat_l4d > 0
        canopyht_veg_cat_l4d > 0
    if phenolog_veg_cat_l4a == 6
        level3 == 124
        lifeform_veg_cat_l4a == 2 | 5 | 6 | 7 | 8 | 9
        watersea_veg_cat_l4a > 0

    Permitted additional attributes
    all
   """
    def __init__(self):
        self.input_layer_name = "phenolog_veg_cat"
        self.output_layer_name = "phenolog_veg_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)

        self.output_codes_descriptions = {1 : ("E1", "Evergreen"),
                                          2 : ("E2", "Deciduous"),
                                          3 : ("E3", "Mixed"),
                                          5 : ("E5", "Mixed (Forbs, graminoids)"),
                                          6 : ("E6", "")} # all excluded categories
        self.valid_level3_classes = [111, 112, 124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((phenolog_veg_cat_l4a >= 1) & (phenolog_veg_cat_l4a <= 3)) & \
             ((level3 == 111) | (level3 == 112)) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4)) & \
             ((leaftype_veg_cat_l4a == 1) | (leaftype_veg_cat_l4a == 2)) & \
             (canopyco_veg_cat_l4d > 0) & (canopyht_veg_cat_l4d > 0)",
            "((phenolog_veg_cat_l4a >= 1) & (phenolog_veg_cat_l4a <= 3)) & \
             (level3 == 124) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4)) & \
             ((leaftype_veg_cat_l4a == 1) | (leaftype_veg_cat_l4a == 2)) & (watersea_veg_cat_l4a > 0)",
            "(phenolog_veg_cat_l4a == 5) & \
             (level3 == 112) & \
             ((lifeform_veg_cat_l4a == 2) | (lifeform_veg_cat_l4a == 5) | (lifeform_veg_cat_l4a == 6)) & \
             (canopyco_veg_cat_l4d > 0) & (canopyht_veg_cat_l4d > 0)",
            "(phenolog_veg_cat_l4a == 5) & \
             (level3 == 124) & \
             ((lifeform_veg_cat_l4a == 2) | (lifeform_veg_cat_l4a == 5) | (lifeform_veg_cat_l4a == 6)) & \
             (watersea_veg_cat_l4a > 0)",
            "(phenolog_veg_cat_l4a == 6) & \
             ((level3 == 111) | (level3 == 112)) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4)) & \
             ((leaftype_veg_cat_l4a == 0) | (leaftype_veg_cat_l4a == 3)) & \
             (canopyco_veg_cat_l4d > 0) & (canopyht_veg_cat_l4d > 0)",
            "(phenolog_veg_cat_l4a == 6) & \
             (level3 == 124) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4)) & \
             ((leaftype_veg_cat_l4a == 0) | (leaftype_veg_cat_l4a == 3)) & (watersea_veg_cat_l4a > 0)",
            "(phenolog_veg_cat_l4a == 6) & \
             ((level3 == 111) | (level3 == 112)) & \
             ((lifeform_veg_cat_l4a == 2) | ((lifeform_veg_cat_l4a >= 5) & (lifeform_veg_cat_l4a <= 9))) & \
             (canopyco_veg_cat_l4d > 0) & (canopyht_veg_cat_l4d > 0)",
            "(phenolog_veg_cat_l4a == 6) & \
             (level3 == 124) & \
             ((lifeform_veg_cat_l4a == 2) | ((lifeform_veg_cat_l4a >= 5) & (lifeform_veg_cat_l4a <= 9))) & \
             (watersea_veg_cat_l4a > 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN

 
class MphenlogVegCatL4m(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Phenology (Mphenlog_veg_cat_l4m) MODIFIER

    Removed classes from FAO Semi-evergreen (E3) and Semi-deciduous (E3),
    just calling these all "Semi-" (E4).
    Modifed from FAO, allowing phenolog_veg_cat_l4a == 6 (i.e. empty phenolog_veg_cat_l4a)
    to be permitted for annual and perennial (i.e. acknowledging that herbs, forbs, graminoids
    can be annual or perennial without being of mixed phenology as in phenolog_veg_cat_l4a)

    Tier 2 class (112), Tier 3 class (124)

    Dependencies for class
    if mphenlog_veg_cat_l4m == 4
        phenolog_veg_cat_l4a == 1 | 2
        leaftype_veg_cat_l4a == 1
    if mphenlog_veg_cat_l4m == 6 | 7
        phenolog_veg_cat_l4a == 5

    Permitted additional attributes
    all
    if mphenlog_veg_cat_l4m == 6 | 7
        lifeform_veg_cat_l4a == 2 | 5 | 6
        phenolog_veg_cat_l4a == 6
    """
    def __init__(self):
        self.input_layer_name = "mphenlog_veg_cat"
        self.output_layer_name = "mphenlog_veg_cat_l4m"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {4 : ("E4", "(Semi-)"),
                                          6 : ("E6", "(Annual)"),
                                          7 : ("E7", "(Perennial)")}
        self.valid_level3_classes = [112, 124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "(mphenlog_veg_cat_l4m == 4) & \
             ((phenolog_veg_cat_l4a == 1) | (phenolog_veg_cat_l4a == 2)) & \
             (leaftype_veg_cat_l4a == 1)",
            "((mphenlog_veg_cat_l4m == 6) | (mphenlog_veg_cat_l4m == 7)) & \
             ((lifeform_veg_cat_l4a == 2) | (lifeform_veg_cat_l4a == 5) | (lifeform_veg_cat_l4a == 6)) & \
             ((phenolog_veg_cat_l4a == 5) | (phenolog_veg_cat_l4a == 6))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MODIFIER


class UrbanvegUrbCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Urban vegetation (urbanveg_urb_cat_l4a)

    Modified from FAO, whereby lifeform information is the only requirement.
    This means that 111 classes can have descriptors of canopyco_veg_cat_l4d
    and canopyht_veg_cat_l4d, however these are not requirements unless specified for Parks.
    No further descriptors are possible for 111 past Tier 1 for urbanveg_urb_cat_l4a
    as these are deemed unnecessary landscape descriptions.

    Tier 2 class (111)

    Dependencies for class
    if urbanveg_urb_cat_l4a == 6
        lifeform_veg_cat_l4a == 1 | 2 | 3 | 4 | 5 | 6
    if urbanveg_urb_cat_l4a == 11
        lifeform_veg_cat_l4a == 1 | 3
        canopyht_veg_cat_l4d == 0 | 5 | 6 | 7 | 8
    if urbanveg_urb_cat_l4a == 12
        lifeform_veg_cat_l4a == 1 | 3 | 4
    if urbanveg_urb_cat_l4a == 13
        lifeform_veg_cat_l4a == 2 | 6
        canopyht_veg_cat_l4d == 0

    Permitted additional attributes (in this case to stop urbanveg_urb_cat_l4a being attributed
    to other 111 classes)
    leaftype_veg_cat_l4a == 0
    phenolog_veg_cat_l4a == 0
    """
    def __init__(self):
        self.input_layer_name = "urbanveg_urb_cat"
        self.output_layer_name = "urbanveg_urb_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = { 6 : ("A6", "Urban vegetated"),
                                          11 : ("A11", "Parks"),
                                          12 : ("A12", "Parkland"),
                                          13 : ("A13", "Lawns")}
        self.valid_level3_classes = [111]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "(urbanveg_urb_cat_l4a == 6) & \
             ((lifeform_veg_cat_l4a >= 1) & (lifeform_veg_cat_l4a <= 6)) & \
             (leaftype_veg_cat_l4a == 0) & (phenolog_veg_cat_l4a == 0)",
            "(urbanveg_urb_cat_l4a == 11) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3)) & \
             ((canopyht_veg_cat_l4d == 0) | (canopyht_veg_cat_l4d == 5) | (canopyht_veg_cat_l4d == 6) | \
             (canopyht_veg_cat_l4d == 7) | (canopyht_veg_cat_l4d == 8)) & \
             (leaftype_veg_cat_l4a == 0) & (phenolog_veg_cat_l4a == 0)",
            "(urbanveg_urb_cat_l4a == 12) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4)) & \
             (leaftype_veg_cat_l4a == 0) & (phenolog_veg_cat_l4a == 0)",
            "(urbanveg_urb_cat_l4a == 13) & \
             ((lifeform_veg_cat_l4a == 2) | (lifeform_veg_cat_l4a == 6)) & \
             (canopyht_veg_cat_l4d == 0) & \
             (leaftype_veg_cat_l4a == 0) & (phenolog_veg_cat_l4a == 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class SpatdistVegCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Landscape descriptors of spatial distribution (spatdist_veg_cat_l4a)

    Changed from spatdist_agr_cat_l4a to spatdist_veg_cat_l4a as this class
    relates to natural vegetation as well as agriculture.
    This combines the spatial distribution for natural vegetation (112)
    and cultivated (111, 123) to one class.

    Specified in the LCCS guidelines
    'The user has the possibility to skip this classifier if it is felt to be irrelevant information.'

    Tier 3 class (111, 112, 123)
    Addition of category (B8) for all excluded categories in Tier 3

    Dependencies for class
    if spatdist_veg_cat_l4a = 1
        level3 == 111 | 112
        canopyco_veg_cat_l4d == 10 | 12 | 13
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatdist_veg_cat_l4a = 1
        level3 == 123
        canopyco_veg_cat_l4d == 10 | 12 | 13
        waterday_agr_cat_l4a > 0
    if spatdist_veg_cat_l4a = 2 | 4 | 5
        level3 == 112
        canopyco_veg_cat_l4d == 10 | 12 | 13
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatdist_veg_cat_l4a = 3
        level3 == 112
        canopyco_veg_cat_l4d == 15
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatdist_veg_cat_l4a = 6 | 7
        level3 == 111
        canopyco_veg_cat_l4d == 16
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatdist_veg_cat_l4a = 6 | 7
        level3 == 123
        canopyco_veg_cat_l4d == 16
        waterday_agr_cat_l4a > 0

    if spatdist_veg_cat_l4a = 8
        level3 == 111
        canopyco_veg_cat_l4d == 10 | 12 | 13 | 15
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatdist_veg_cat_l4a = 8
        level3 == 112
        canopyco_veg_cat_l4d == 16
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatdist_veg_cat_l4a = 8
        level3 == 123
        canopyco_veg_cat_l4d == 10 | 12 | 13 | 15
        waterday_agr_cat_l4a > 0

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "spatdist_veg_cat"
        self.output_layer_name = "spatdist_veg_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("C1", "Continuous"),
                                          2 : ("C2", "Fragmented"),
                                          3 : ("C3", "Parklike patches"),
                                          4 : ("C4", "Striped"),
                                          5 : ("C5", "Cellular"),
                                          6 : ("B6", "Scattered (clustered)"),
                                          7 : ("B7", "Scattered (isolated)"),
                                          8 : ("B8", "")} # all excluded categories
        self.valid_level3_classes = [111, 112, 123]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "(spatdist_veg_cat_l4a == 1) & \
             ((level3 == 111) | (level3 == 112)) & \
             ((canopyco_veg_cat_l4d == 10) | (canopyco_veg_cat_l4d == 12) | (canopyco_veg_cat_l4d == 13)) & \
             (leaftype_veg_cat_l4a > 0) & (phenolog_veg_cat_l4a > 0)",
            "(spatdist_veg_cat_l4a == 1) & \
             (level3 == 123) & \
             ((canopyco_veg_cat_l4d == 10) | (canopyco_veg_cat_l4d == 12) | (canopyco_veg_cat_l4d == 13)) & \
             (waterday_agr_cat_l4a > 0)",
            "((spatdist_veg_cat_l4a == 2) | (spatdist_veg_cat_l4a == 4) | (spatdist_veg_cat_l4a == 5)) & \
             (level3 == 112) & \
             ((canopyco_veg_cat_l4d == 10) | (canopyco_veg_cat_l4d == 12) | (canopyco_veg_cat_l4d == 13)) & \
             (leaftype_veg_cat_l4a > 0) & (phenolog_veg_cat_l4a > 0)",
            "(spatdist_veg_cat_l4a == 3) & \
             (level3 == 112) & \
             (canopyco_veg_cat_l4d == 15) & (leaftype_veg_cat_l4a > 0) & (phenolog_veg_cat_l4a > 0)",
            "((spatdist_veg_cat_l4a == 6) | (spatdist_veg_cat_l4a == 7)) & \
             (level3 == 111) & \
             (canopyco_veg_cat_l4d == 16) & (leaftype_veg_cat_l4a > 0) & (phenolog_veg_cat_l4a > 0)",
            "((spatdist_veg_cat_l4a == 6) | (spatdist_veg_cat_l4a == 7)) & \
             (level3 == 123) & \
             (canopyco_veg_cat_l4d == 16) & \
             (waterday_agr_cat_l4a > 0)",
            "(spatdist_veg_cat_l4a == 8) & \
             (level3 == 111) & \
             ((canopyco_veg_cat_l4d == 10) | (canopyco_veg_cat_l4d == 12) | \
             (canopyco_veg_cat_l4d == 13) | (canopyco_veg_cat_l4d == 15)) & \
             (leaftype_veg_cat_l4a > 0) & (phenolog_veg_cat_l4a > 0)",
            "(spatdist_veg_cat_l4a == 8) & \
             (level3 == 112) & \
             (canopyco_veg_cat_l4d == 16) & \
             (leaftype_veg_cat_l4a > 0) & (phenolog_veg_cat_l4a > 0)",
            "(spatdist_veg_cat_l4a == 8) & \
             (level3 == 123) & \
             ((canopyco_veg_cat_l4d == 10) | (canopyco_veg_cat_l4d == 12) | \
             (canopyco_veg_cat_l4d == 13) | (canopyco_veg_cat_l4d == 15)) & \
             (waterday_agr_cat_l4a > 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class Strat2ndVegCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Second layer present or absent (strat2nd_veg_cat_l4a)

    For 2nd layer attributes, using an EO definition of layer == height, modified from FAO which considers
    effects of cover in relation to lifeform and height.
    Therefore must have canopyht_veg_cat_l4d to be permitted attribute (inherent in Level 4 hierarchy at Tier 1).
    In addition, height must be sensible for EO to capture second layer, therefore CanopyhtVegCatL4d > 0.5 m.

    Tier 4 class (112, 124)
    No further attributes are possible for 112 and 124, therefore addition of category for all excluded is not required.

    Dependencies for class
    if strat2nd_veg_cat_l4a == 1 | 2
        level3 == 112
        spatdist_veg_cat_l4a > 0
    if strat2nd_veg_cat_l4a == 1 | 2
        level3 == 124
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0

    Permitted additional attributes
    lifeform_veg_cat_l4a == 1 | 3 | 4
    canopyht_veg_cat_l4d == 5 | 6 | 7 | 8 | 9
    """
    def __init__(self):
        self.input_layer_name = "strat2nd_veg_cat"
        self.output_layer_name = "strat2nd_veg_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("F1", "Second layer absent"),
                                          2 : ("F2", "Second layer present")}
        self.valid_level3_classes = [112, 124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((strat2nd_veg_cat_l4a == 1) | (strat2nd_veg_cat_l4a == 2)) & \
             (level3 == 112) & \
             (spatdist_veg_cat_l4a > 0) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4)) & \
             ((canopyht_veg_cat_l4d >= 5) & (canopyht_veg_cat_l4d <= 9))",
            "((strat2nd_veg_cat_l4a == 1) | (strat2nd_veg_cat_l4a == 2)) & \
             (level3 == 124) & \
             (leaftype_veg_cat_l4a > 0) & (phenolog_veg_cat_l4a > 0) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4)) & \
             ((canopyht_veg_cat_l4d >= 5) & (canopyht_veg_cat_l4d <= 9))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class Lifef2ndVegCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Lifeform of the second layer (lifef2nd_veg_cat_l4a)

    Only permitting attributes of Lifef2ndVegCatL4a where LifeformVegCatL4a is present.
    Specifically, Lifef2ndVegCatL4a must be of equal or short stature than LifeformVegCatL4a,
    implemented here based on CanopyhtVegCatL4d (i.e. if Lifef2ndVegCatL4a is trees
    then LifeformVegCatL4a must be either woody or trees).
    In addition, LifeformVegCatL4a woody height must be greater than lowest tree height
    (2 m) if Lifef2ndVegCatL4a == 4.

    Tier 4 class (112, 124)

    Dependencies for class
    strat2nd_veg_cat_l4a == 2

    Permitted additional attributes
    if lifef2nd_veg_cat_l4a == 3
        lifeform_veg_cat_l4a == 1
    if lifef2nd_veg_cat_l4a == 4
        lifeform_veg_cat_l4a == 1 | 3
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8
    if lifef2nd_veg_cat_l4a == 5 | 6
        lifeform_veg_cat_l4a == 1 | 3 | 4
    """
    def __init__(self):
        self.input_layer_name = "lifef2nd_veg_cat"
        self.output_layer_name = "lifef2nd_veg_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {3 : ("F3", "Woody"),
                                          4 : ("F4", "Trees"),
                                          5 : ("F5", "Shrubs"),
                                          6 : ("F6", "Herbaceous")}
        self.valid_level3_classes = [112, 124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "(lifef2nd_veg_cat_l4a == 3) & \
             (strat2nd_veg_cat_l4a == 2) & (lifeform_veg_cat_l4a == 1)",
            "(lifef2nd_veg_cat_l4a == 4) & \
             (strat2nd_veg_cat_l4a == 2) & ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3)) & \
             ((canopyht_veg_cat_l4d >= 5) & (canopyht_veg_cat_l4d <= 8))",
            "((lifef2nd_veg_cat_l4a == 5) | (lifef2nd_veg_cat_l4a == 6)) & \
             (strat2nd_veg_cat_l4a == 2) & ((lifeform_veg_cat_l4a == 1) | \
             (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class Cover2ndVegCatL4d(l4_base.Level4ClassificationContinuousLayer):
    """
    Cover of the 2nd layer (cover2nd_veg_cat_l4d) DERIVATIVE

    The TERN Structural Classification (Scarth et al. 2019) generates
    continuous layers on sub-canopy cover but not by layer number.

    FAO LCCS has greater restrictions on Cover2ndVegCatL4d, however some classes specified as
    'impossible' in the LCCS documentation are actually easily derived from Lidar.
    Note: the large reason for divergence here is that we interpret 2nd strata to be related to
    height, not a cover/height ecological definition as FAO attempt to integrate.

    Class boundaries have been altered to middle of range i.e. 10-20 == 15

    Doesn't include FAO LCCS class F7: Open to Closed (15 to 100 %)

    Only permitting attributes of Cover2ndVegCatL4d where CanopycoVegCatL4a is present
    (inherent in Level 4 hierarchy at Tier 1).

    Tier 4 class (112, 124)

    Dependencies for class
    strat2nd_veg_cat_l4a == 2

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "cover2nd_veg_con"
        self.output_layer_name = "cover2nd_veg_cat_l4d"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = { 8 : ("F8", "Closed (> 65 %)"),
                                           9 : ("F9", "Open (15 to 65 %)"),
                                           10 : ("F10", "Sparse (1 to 15 %)")}
        self.class_boundaries = {0 : (0, 1),
                                 10 : (1, 15),
                                 9 : (15, 65),
                                 8 : (65, None)}
        self.valid_level3_classes = [112, 124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["strat2nd_veg_cat_l4a == 2"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.DERIVATIVE


class Heigh2ndVegCatL4d(l4_base.Level4ClassificationContinuousLayer):
    """
    Height of the 2nd layer (heigh2nd_veg_cat_l4d)

    The TERN Structural Classification (Scarth et al. 2019) generates continuous
    layers on sub-canopy cover but not by layer number.

    Modifed from FAO, permitting heigh2nd_veg_cat_l4d layer to not require
    lifef2nd_veg_cat_l4a (undefined_lifef2nd). This decision was due to likely
    avaliability of heigh2nd_veg_cat_l4d for EO where lifef2nd_veg_cat_l4a may not
    be generated as rountinely.

    Only permitting attributes of Heigh2ndVegCatL4d where CanopyhtVegCatL4a is present
    (inherent in Level 4 hierarchy at Tier 1).
    Furthermore, height of 2nd layer must be smaller than value of CanopyhtVegCatL4a,
    where overlapping classes are excluded to ensure ambiguity of height classes is minimised.

    NOTE: categories modified from FAO so that woody (encompassing tree and shrub)
    did not have overlapping height categories.
    Original FAO B7: 3 -7 m --> modified FAO B7: 5 -7 m
    Therefore height categories are woody (B5 - B10), tree (B5 - B8) and
    shrub (B8 - B10). FAO defines trees as >= 3 m, however modified FAO
    for EO implementation defines trees as >= 2 m (similar to forestry and
    ecology definitions)

    Tier 4 class (112, 124)

    Dependencies for class
    strat2nd_veg_cat_l4a == 2

    Permitted additional attributes
    if heigh2nd_veg_cat_l4d == 5 | 6
        canopyht_veg_cat_l4d == 5
        lifef2nd_veg_cat_l4a == 0 | 3 | 4
    if heigh2nd_veg_cat_l4d == 7
        canopyht_veg_cat_l4d == 5 | 6
        lifef2nd_veg_cat_l4a == 0 | 3 | 4
    if heigh2nd_veg_cat_l4d == 8
        canopyht_veg_cat_l4d == 5 | 6 | 7
        lifef2nd_veg_cat_l4a == 0 | 3 | 4 | 5
    if heigh2nd_veg_cat_l4d == 9
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8
        lifef2nd_veg_cat_l4a == 0 | 3 | 5
    if heigh2nd_veg_cat_l4d == 10
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8 | 9
        lifef2nd_veg_cat_l4a == 0 | 3 | 5
    if heigh2nd_veg_cat_l4d == 11
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8 | 9
        lifef2nd_veg_cat_l4a == 6
    if heigh2nd_veg_cat_l4d == 12
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8 | 9 | 11 | 12
        lifef2nd_veg_cat_l4a == 6
    """
    def __init__(self):
        self.input_layer_name = "heigh2nd_veg_con"
        self.output_layer_name = "heigh2nd_veg_cat_l4d"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = { 5 : ("G5", "(> 14 m)"),
                                           6 : ("G6", "(7 to 14 m)"),
                                           7 : ("G7", "(5 to 7 m)"),
                                           8 : ("G8", "(2 to 5 m)"),
                                           9 : ("G9", "(0.5 to 2 m)"),
                                          10 : ("G10", "(< 0.5 m)"),
                                          11 : ("G11", "(0.3 to 3 m)"),
                                          12 : ("G12", "(0.03 to 0.3 m)")}

        self.undefined_lifef2nd_class_boundaries = {5 : (14, None),
                                                    6 : (7, 14),
                                                    7 : (5, 7),
                                                    8 : (2, 5),
                                                    9 : (0.5, 2),
                                                    10: (None, 0.5)}

        self.woody_class_boundaries = {5 : (14, None),
                                       6 : (7, 14),
                                       7 : (5, 7),
                                       8 : (2, 5),
                                       9 : (0.5, 2),
                                       10 : (None, 0.5)}

        self.trees_class_boundaries = {5 : (14, None),
                                       6 : (7, 14),
                                       7 : (5, 7),
                                       8 : (2, 5)}

        self.shrubs_class_boundaries = {8 : (2, 5),
                                        9 : (0.5, 2),
                                        10 : (None, 0.5)}

        self.herbaceous_class_boundaries = {11 : (0.3, 3),
                                            12 : (0.03, 0.3)}

        # Create a single dictionary with all class boundaries
        # used to check valid inputs.
        self.class_boundaries = {}
        self.class_boundaries.update(self.undefined_lifef2nd_class_boundaries)
        self.class_boundaries.update(self.woody_class_boundaries)
        self.class_boundaries.update(self.trees_class_boundaries)
        self.class_boundaries.update(self.shrubs_class_boundaries)
        self.class_boundaries.update(self.herbaceous_class_boundaries)

        self.valid_level3_classes = [112, 124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((heigh2nd_veg_cat_l4d == 5) | (heigh2nd_veg_cat_l4d == 6)) & \
             (strat2nd_veg_cat_l4a == 2) & \
             (canopyht_veg_cat_l4d == 5) & ((lifef2nd_veg_cat_l4a == 0) | \
             (lifef2nd_veg_cat_l4a == 3) | (lifef2nd_veg_cat_l4a == 4))",
            "(heigh2nd_veg_cat_l4d == 7) & \
             (strat2nd_veg_cat_l4a == 2) & ((canopyht_veg_cat_l4d == 5) | (canopyht_veg_cat_l4d == 6)) & \
             ((lifef2nd_veg_cat_l4a == 0) | (lifef2nd_veg_cat_l4a == 3) | (lifef2nd_veg_cat_l4a == 4))",
            "(heigh2nd_veg_cat_l4d == 8) & \
             (strat2nd_veg_cat_l4a == 2) & ((canopyht_veg_cat_l4d >= 5) & (canopyht_veg_cat_l4d <= 7)) & \
             ((lifef2nd_veg_cat_l4a == 0) | ((lifef2nd_veg_cat_l4a >= 3) & (lifef2nd_veg_cat_l4a <= 5)))",
            "(heigh2nd_veg_cat_l4d == 9) & \
             (strat2nd_veg_cat_l4a == 2) & ((canopyht_veg_cat_l4d >= 5) & (canopyht_veg_cat_l4d <= 8)) & \
             ((lifef2nd_veg_cat_l4a == 0) | (lifef2nd_veg_cat_l4a == 3) | (lifef2nd_veg_cat_l4a == 5))",
            "(heigh2nd_veg_cat_l4d == 10) & \
             (strat2nd_veg_cat_l4a == 2) & ((canopyht_veg_cat_l4d >= 5) & (canopyht_veg_cat_l4d <= 9)) & \
             ((lifef2nd_veg_cat_l4a == 0) | (lifef2nd_veg_cat_l4a == 3) | (lifef2nd_veg_cat_l4a == 5))",
            "(heigh2nd_veg_cat_l4d == 11) & \
             (strat2nd_veg_cat_l4a == 2) & ((canopyht_veg_cat_l4d >= 5) & (canopyht_veg_cat_l4d <= 9)) & \
             (lifef2nd_veg_cat_l4a == 6)",
            "(heigh2nd_veg_cat_l4d == 12) & \
             (strat2nd_veg_cat_l4a == 2) & \
             (((canopyht_veg_cat_l4d >= 5) & (canopyht_veg_cat_l4d <= 9)) | \
             ((canopyht_veg_cat_l4d >= 11) & (canopyht_veg_cat_l4d <= 12))) & \
             (lifef2nd_veg_cat_l4a == 6)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.DERIVATIVE

    def transform(self, in_values_xarray, out_values_xarray):
        """
        Transform function. Needs to be specific transform to include other classes.
        Note that the deviation of the class boundaries from the original FAO LCCS
        """

        # Check input values
        self.check_inputs(in_values_xarray)

        try:
            in_values_xarray["lifef2nd_veg_cat"]
        except KeyError:
            logging.debug("Also need to have lifef2nd_veg_cat to calculate "
                            "strat2nd_veg_cat_l4a")
            return None

        # Set up numpy array for output
        out_vals = numpy.zeros(in_values_xarray[self.input_layer_name].shape,
                               dtype=numpy.uint8)

        # Set up output xarray
        out_xarray = xarray.Dataset(
            {self.output_layer_name : (in_values_xarray[self.input_layer_name].dims, out_vals)},
            coords=in_values_xarray.coords)

        # Calculate for undefined lifef2nd (code=0 in lifef2nd_veg_cat; i.e, undefined lifef2nd)
        for code, min_max in self.undefined_lifef2nd_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 0))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 0))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 0))
            out_xarray[self.output_layer_name].values[subset] = code

        # Calculate for woody (code=3 in lifef2nd_veg_cat; i.e, the woody understorey)
        for code, min_max in self.woody_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 3))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 3))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 3))
            out_xarray[self.output_layer_name].values[subset] = code

        # Calculate for trees (code=4 in lifef2nd_veg_cat; i.e, the tree understorey)
        for code, min_max in self.trees_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 4))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 4))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 4))
            out_xarray[self.output_layer_name].values[subset] = code

        # Calculate for shrubs (code=5 in lifef2nd_veg_cat; i.e, the shrub understorey)
        for code, min_max in self.shrubs_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 5))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 5))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 5))
            out_xarray[self.output_layer_name].values[subset] = code

        # Calculate for herbaceous (code=6 in lifef2nd_veg_cat; i.e, the herbaceous understorey)
        for code, min_max in self.herbaceous_class_boundaries.items():
            if min_max[0] is None and min_max[1] is None:
                raise Exception("Must define at least a minimum or maximum. Both are set to"
                                " None for code {} in layer {}".format(code,
                                                                       self.input_layer_name))
            # If no minimum is defined just use upper limit
            elif min_max[0] is None:
                subset = ((in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 6))
            # If no maximum is defined just use lower limit
            elif min_max[1] is None:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 6))
            else:
                subset = ((in_values_xarray[self.input_layer_name].values >= min_max[0]) &
                          (in_values_xarray[self.input_layer_name].values < min_max[1]) &
                          (out_xarray[self.output_layer_name].values == 0) &
                          (in_values_xarray["lifef2nd_veg_cat"].values == 6))
            out_xarray[self.output_layer_name].values[subset] = code

        mask = self._get_layer_mask(xarray.merge([out_values_xarray, out_xarray]))

        # Apply the mask. Any values which didn't match the classes we are interested in get
        # set to 0.
        out_xarray[self.output_layer_name].values[mask] = 0

        return out_xarray


class SpatsizeAgrCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Spatial size (spatsize_agr_cat_l4a)

    FAO guidelines
    'This classifier can be skipped because size is a very subjective parameter.'

    Tier 3 class (111, 123)

    Dependencies for class
    if spatsize_agr_cat_l4a = 1 | 2 | 3 | 4
        level3 == 111
        leaftype_veg_cat_l4a > 0
        phenolog_veg_cat_l4a > 0
    if spatsize_agr_cat_l4a = 1 | 2 | 3 | 4
        level3 == 123
        waterday_agr_cat_l4a > 0

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "spatsize_agr_cat"
        self.output_layer_name = "spatsize_agr_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("B1", "Medium to large (> 2 ha) field(s)"),
                                          2 : ("B2", "Small (< 2 ha) field(s)"),
                                          3 : ("B3", "Large (> 5 ha) field(s)"),
                                          4 : ("B4", "Medium (2-5 ha) field(s)")}
        self.valid_level3_classes = [111, 123]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((spatsize_agr_cat_l4a >= 1) & (spatsize_agr_cat_l4a <= 4)) & \
             (level3 == 111) & \
             (leaftype_veg_cat_l4a > 0) & (phenolog_veg_cat_l4a > 0)",
            "((spatsize_agr_cat_l4a >= 1) & (spatsize_agr_cat_l4a <= 4)) & \
             (level3 == 123) & \
             (waterday_agr_cat_l4a > 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class CropcombAgrCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Crop combinations for cultivated terrestrial vegetation (cropcomb_agr_cat_l4a)
    Forbs and graminoids are permitted LifeformVegCatL4a for 111, however as Croplfc2AgrCatL4a only
    specifies herbaceous we are determining that forbs and graminoids cannot have specified additional
    crops (aligning with height for layer stratification in natural veg).

    Tier 4 class (111)

    Dependencies for class
    spatdist_veg_cat_l4a > 0
    spatsize_agr_cat_l4a > 0

    Permitted additional attributes
    if cropcomb_agr_cat_l4a == 1 | 2
        lifeform_veg_cat_l4a == 1 | 2 | 3 | 4 | 5 | 6
    if cropcomb_agr_cat_l4a == 3 | 4
        lifeform_veg_cat_l4a == 1 | 2 | 3 | 4
    """
    def __init__(self):
        self.input_layer_name = "cropcomb_agr_cat"
        self.output_layer_name = "cropcomb_agr_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("C1", "Single crop"),
                                          2 : ("C2", "Multiple crop"),
                                          3 : ("C3", "Multiple crop; one additional crop"),
                                          4 : ("C4", "Multiple crop; two additional crops")}
        self.valid_level3_classes = [111]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((cropcomb_agr_cat_l4a == 1) | (cropcomb_agr_cat_l4a == 2)) & \
             (spatdist_veg_cat_l4a > 0) & (spatsize_agr_cat_l4a > 0) & \
             ((lifeform_veg_cat_l4a >= 1) & (lifeform_veg_cat_l4a <= 6))",
            "((cropcomb_agr_cat_l4a == 3) | (cropcomb_agr_cat_l4a == 4)) & \
             (spatdist_veg_cat_l4a > 0) & (spatsize_agr_cat_l4a > 0) & \
             ((lifeform_veg_cat_l4a >= 1) & (lifeform_veg_cat_l4a <= 4))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class Croplfc2AgrCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Crop lifeform for crop combinations C3 and C4 (croplfc2_agr_cat_l4a)

    Naming convention modified from FAO:

    * One/two additional crop; Multiple crop --> trees (second crop)
    * One/two additional crop; Multiple crop --> shrubs (second crop)
    * One/two additional crop; Multiple crop --> herbaceous (terrestrial; second crop)
    * One/two additional crop; Multiple crop --> herbaceous (aquatic; second crop)

    For 2nd lifeform attributes to crops, using an EO definition of layer == height, in line with previous classes.
    This is modified from FAO which considers lifeform to refer to the dominant crop present.
    Therefore must have Croplfc2AgrCatL4a of equal or smaller stature than LifeformVegCatL4a,
    hence CanopyhtVegCatL4d > 0.5 m and where herbaceous CanopyhtVegCatL4d > 0.3 m.

    Tier 4 class (111)

    Dependencies for class
    cropcomb_agr_cat_l4a == 3 | 4

    Permitted additional attributes
    if croplfc2_agr_cat_l4a == 5
        lifeform_veg_cat_l4a == 1 | 3
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8
    if croplfc2_agr_cat_l4a == 6
        lifeform_veg_cat_l4a == 1 | 3 | 4
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8 | 9
    if croplfc2_agr_cat_l4a == 7 | 8
        lifeform_veg_cat_l4a == 1 | 2 | 3 | 4
        canopyht_veg_cat_l4d == 5 | 6 | 7 | 8 | 9 | 11 | 12
    """
    def __init__(self):
        self.input_layer_name = "croplfc2_agr_cat"
        self.output_layer_name = "croplfc2_agr_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {5 : ("C5", "Trees (second crop)"),
                                          6 : ("C6", "Shrubs (second crop)"),
                                          7 : ("C7", "Herbaceous (terrestrial; second crop)"),
                                          8 : ("C8", "Herbaceous (aquatic; second crop)")}
        self.valid_level3_classes = [111]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "(croplfc2_agr_cat_l4a == 5) & \
             ((cropcomb_agr_cat_l4a == 3) | (cropcomb_agr_cat_l4a == 4)) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3)) & \
             ((canopyht_veg_cat_l4d >= 5) & (canopyht_veg_cat_l4d <= 8))",
            "(croplfc2_agr_cat_l4a >= 6) & \
             ((cropcomb_agr_cat_l4a == 3) | (cropcomb_agr_cat_l4a == 4)) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4)) & \
             ((canopyht_veg_cat_l4d >= 5) & (canopyht_veg_cat_l4d <= 9))",
            "((croplfc2_agr_cat_l4a == 7) | (croplfc2_agr_cat_l4a == 8)) & \
             ((cropcomb_agr_cat_l4a == 3) | (cropcomb_agr_cat_l4a == 4)) & \
             ((lifeform_veg_cat_l4a >= 1) & (lifeform_veg_cat_l4a <= 4)) & \
             (((canopyht_veg_cat_l4d >= 5) & (canopyht_veg_cat_l4d <= 9)) | \
             (canopyht_veg_cat_l4d == 11) | (canopyht_veg_cat_l4d == 12))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class Croplfc3AgrCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Crop lifeform for crop combination C4 (croplfc3_agr_cat_l4a)

    Naming convention modified from FAO:

    * Two additional crops; Multiple crop; second crop --> trees (third crop)
    * Two additional crops; Multiple crop; second crop --> shrubs (third crop)
    * Two additional crops; Multiple crop; second crop --> graminoids (third crop)
    * Two additional crops; Multiple crop; second crop --> non-graminoids (third crop)

    For 3rd lifeform attributes to crops, using an EO definition of layer == height, in line with previous classes.
    This is modified from FAO which considers lifeform to refer to the dominant crop present.
    Therefore must have Croplfc3AgrCatL4a of equal or smaller stature than Croplfc2AgrCatL4a.

    Tier 4 class (111)

    Dependencies for class
    cropcomb_agr_cat_l4a == 4

    Permitted additional attributes
    if croplfc3_agr_cat_l4a == 13
        croplfc2_agr_cat_l4a == 5
    if croplfc3_agr_cat_l4a == 14
        croplfc2_agr_cat_l4a == 5 | 6
    if croplfc3_agr_cat_l4a == 15 | 16
        croplfc2_agr_cat_l4a == 7 | 8
    """
    def __init__(self):
        self.input_layer_name = "croplfc3_agr_cat"
        self.output_layer_name = "croplfc3_agr_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {13 : ("C13", "Trees (third crop)"),
                                          14 : ("C14", "Shrubs (third crop)"),
                                          15 : ("C15", "Graminoids (third crop)"),
                                          16 : ("C16", "Non-graminoids (third crop)")}
        self.valid_level3_classes = [111]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "(croplfc3_agr_cat_l4a == 13) & \
             (cropcomb_agr_cat_l4a == 4) & (croplfc2_agr_cat_l4a == 5)",
            "(croplfc3_agr_cat_l4a == 14) & \
             (cropcomb_agr_cat_l4a == 4) & ((croplfc2_agr_cat_l4a == 5) | (croplfc2_agr_cat_l4a == 6))",
            "((croplfc3_agr_cat_l4a == 15) | (croplfc3_agr_cat_l4a == 16)) & \
             (cropcomb_agr_cat_l4a == 4) & ((croplfc2_agr_cat_l4a == 7) | (croplfc2_agr_cat_l4a == 8))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class CropseqtAgrCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Crop sequences for cultivated terrestrial vegetation (cropseqt_agr_cat_l4a)

    Tier 5 class (111)

    Dependencies for class
    cropcomb_agr_cat_l4a == 2 | 3 | 4

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "cropseqt_agr_cat"
        self.output_layer_name = "cropseqt_agr_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {17 : ("C17", "Simultaneous cropping"),
                                          18 : ("C18", "Overlapping cropping"),
                                          19 : ("C19", "Sequential cropping")}
        self.valid_level3_classes = [111]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["(cropcomb_agr_cat_l4a >= 2) & (cropcomb_agr_cat_l4a <= 4)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class CropseqaAgrCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Crop sequence for cultivated aquatic vegetation (cropseqa_agr_cat)

    Naming convention modified from FAO: Cultivation practice for agriculture (cultprac_agr_cat)

    Tier 4 class (123)
    No further attributes are possible for 123.

    Dependencies for class
    spatdist_veg_cat_l4a > 0
    spatsize_agr_cat_l4a > 0

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "cropseqa_agr_cat"
        self.output_layer_name = "cropseqa_agr_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("D1", "Permanent cropping"),
                                          2 : ("D2", "Relay intercropping"),
                                          3 : ("D3", "Sequential cropping")}
        self.valid_level3_classes = [123]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["(spatdist_veg_cat_l4a > 0) & (spatsize_agr_cat_l4a > 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class WatersupAgrCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Water supply for cultivated terrestrial vegetation (watersup_agr_cat_l4a)

    Tier 5 class (111)

    Dependencies for class
    cropcomb_agr_cat_l4a > 0

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "watersup_agr_cat"
        self.output_layer_name = "watersup_agr_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("D1", "Cultural Practice (Rainfed)"),
                                          2 : ("D2", "Cultural Practice (Post-flooding)"),
                                          3 : ("D3", "Irrigated"),
                                          4 : ("D4", "Irrigated Surface"),
                                          5 : ("D5", "Irrigated Sprinkler"),
                                          6 : ("D6", "Irrigated Drip")}
        self.valid_level3_classes = [111]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["cropcomb_agr_cat_l4a > 0"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class TimefactAgrCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Time factor for cultivated terrestrial vegetation (timefact_agr_cat_l4a)

    Tier 5 class (111)
    No further attributes are possible for 111, therefore addition of category for all excluded is not required.

    Dependencies for class
    if timefact_agr_cat_l4a == 7 | 8
        cropcomb_agr_cat_l4a > 0
    if timefact_agr_cat_l4a == 9
        watersup_agr_cat_l4a == 0 | 1 | 3 | 4 | 5 | 6
        cropcomb_agr_cat_l4a > 0

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "timefact_agr_cat"
        self.output_layer_name = "timefact_agr_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {7 : ("D7", "Shifting cultivation"),
                                          8 : ("D8", "Fallow system"),
                                          9 : ("D9", "Permanent cultivation")}
        self.valid_level3_classes = [111]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((timefact_agr_cat_l4a == 7) | (timefact_agr_cat_l4a == 8)) & \
             (cropcomb_agr_cat_l4a > 0)",
            "(timefact_agr_cat_l4a == 9) & \
             ((watersup_agr_cat_l4a == 0) | (watersup_agr_cat_l4a == 1) | (watersup_agr_cat_l4a == 3) | \
             (watersup_agr_cat_l4a == 4) | (watersup_agr_cat_l4a == 5) | (watersup_agr_cat_l4a == 6)) & \
             (cropcomb_agr_cat_l4a > 0)"]

        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class WatersttWatCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Water state (waterstt_wat_cat_l4a)

    Descriptions are in parenthesis as Level 3 classes end in 'Water' so would
    get 'Water Water' without.

    Tier 1 class (220, 227, 228)

    Dependencies for class
    none

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "waterstt_wat_cat"
        self.output_layer_name = "waterstt_wat_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("A1", "(Water)"),
                                          2 : ("A2", "(Snow)"),
                                          3 : ("A3", "(Ice)")}
        self.valid_level3_classes = [220, 227, 228]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class InttidalWatCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Tidal areas (inttidal_wat_cat_l4a)

    Modified from FAO LCCS: added a new class for tidal area
    as was confusing in WaterperWatCatL4d

    Prioritised before WaterperWatCatL4d in classification as likely that tidal areas
    are more accurately mapped (if dataset available) than water persistence,
    which is usually a continuous surface input.

    Tier 2 class (220, 227, 228)

    Dependencies on class
    waterstt_wat_cat_l4a == 1

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "inttidal_wat_cat"
        self.output_layer_name = "inttidal_wat_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = { 3 : ("B3", "Tidal area")}

        self.valid_level3_classes = [220, 227, 228]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["(waterstt_wat_cat_l4a == 1)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class WaterperWatCatL4d(l4_base.Level4ClassificationContinuousLayer):
    """
    Water persistence (hydroperiod) (waterper_wat_cat_l4d) DERIVATIVE

    Modifed from FAO LCCS: new class for tidal areas (TidalperWatCatL4a),
    seperated as can have perennial/non-perennial tidal areas.

    Doesn't include FAO LCCS class B2: Non-perennial (< 9 months)

    Tier 2 class (220, 227, 228)

    Dependencies on class
    waterstt_wat_cat_l4a == 1

    Permitted additional attributes
    inttidal_wat_cat_l4a == 0
    """
    def __init__(self):
        self.input_layer_name = "waterper_wat_cin"
        self.output_layer_name = "waterper_wat_cat_l4d"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = { 1 : ("B1", "Perennial (> 9 months)"),
                                           7 : ("B7", "Non-perennial (7 to 9 months)"),
                                           8 : ("B8", "Non-perennial (4 to 6 months)"),
                                           9 : ("B9", "Non-perennial (1 to 3 months)")}
        # In days. Use months multiplied by 30 to simplify
        self.class_boundaries = {1 : (9*30, None),
                                 7 : (7*30, 9*30),
                                 8 : (4*30, 7*30),
                                 9 : (1*30, 4*30),
                                 0 : (0*30, 1*30)}
        self.valid_level3_classes = [220, 227, 228]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["(waterstt_wat_cat_l4a == 1) & (inttidal_wat_cat_l4a == 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.DERIVATIVE


class SnowcperWatCatL4d(l4_base.Level4ClassificationContinuousLayer):
    """
    Snow cover persistence (hydroperiod) (snowcper_wat_cat_l4d) DERIVATIVE

    Doesn't include FAO LCCS class B2: Non-perennial (< 9 months)

    Tier 2 class (220, 227, 228)

    Dependencies on class
    waterstt_wat_cat_l4a == 2

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "snowcper_wat_cin"
        self.output_layer_name = "snowcper_wat_cat_l4d"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = { 1 : ("B1", "Perennial (> 9 months)"),
                                           7 : ("B7", "Non-perennial (7 to 9 months)"),
                                           8 : ("B8", "Non-perennial (4 to 6 months)"),
                                           9 : ("B9", "Non-perennial (1 to 3 months)")}
        # In days. Use months multiplied by 30 to simplify
        self.class_boundaries = {1 : (9*30, None),
                                 7 : (7*30, 9*30),
                                 8 : (4*30, 7*30),
                                 9 : (1*30, 4*30),
                                 0 : (0*30, 1*30)}
        self.valid_level3_classes = [220, 227, 228]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["waterstt_wat_cat_l4a == 2"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.DERIVATIVE


class WaterdptWatCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Water depth (waterdpt_wat_cat_l4a)
    Includes all forms of water state

    Tier 3 class (220, 227, 228)

    Dependencies on class
    waterstt_wat_cat_l4a == 1 & inttidal_wat_cat_l4a == 3
    or
    waterstt_wat_cat_l4a == 1 & waterper_wat_cat_l4d > 0
    or
    waterstt_wat_cat_l4a == 2 & snowcper_wat_cat_l4d > 0
    or
    waterstt_wat_cat_l4a == 3

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "waterdpt_wat_cat"
        self.output_layer_name = "waterdpt_wat_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("C1", "Medium to deep (> 2 m)"),
                                          2 : ("C2", "Shallow (< 2 m)")}
        self.valid_level3_classes = [220, 227, 228]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "(waterstt_wat_cat_l4a == 1) & (inttidal_wat_cat_l4a == 3)",
            "(waterstt_wat_cat_l4a == 1) & (waterper_wat_cat_l4d > 0)",
            "(waterstt_wat_cat_l4a == 2) & (snowcper_wat_cat_l4d > 0)",
            "(waterstt_wat_cat_l4a == 3)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class MwatrmvtWatCatL4m(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Water movement (mwatrmvt_wat_cat_l4m) MODIFIER

    Tier 4 class (220, 227, 228)

    Dependencies for class
    if mwatrmvt_wat_cat_l4m == 4 | 5
        waterper_wat_cat_l4d > 0
        waterdpt_wat_cat_l4a > 0
    if mwatrmvt_wat_cat_l4m == 6 | 7
        waterstt_wat_cat_l4a == 3
        waterdpt_wat_cat_l4a > 0

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "mwatrmvt_wat_cat"
        self.output_layer_name = "mwatrmvt_wat_cat_l4m"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {4 : ("A4", "Flowing water"),
                                          5 : ("A5", "Standing water"),
                                          6 : ("A6", "Moving ice"),
                                          7 : ("A7", "Stationary ice")}
        self.valid_level3_classes = [220, 227, 228]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((mwatrmvt_wat_cat_l4m == 4) | (mwatrmvt_wat_cat_l4m == 5)) & \
             (waterper_wat_cat_l4d > 0) & (waterdpt_wat_cat_l4a > 0)",
            "((mwatrmvt_wat_cat_l4m == 6) | (mwatrmvt_wat_cat_l4m == 7)) & \
             (waterstt_wat_cat_l4a == 3) & (waterdpt_wat_cat_l4a > 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MODIFIER


class WsedloadWatCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Water sediment loads (wsedload_wat_cat_l4a)

    Tier 4 class (220, 227, 228)

    Dependencies on class
    inttidal_wat_cat_l4a == 3 or waterper_wat_cat_l4d > 0
    waterdpt_wat_cat > 0

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "wsedload_wat_cat"
        self.output_layer_name = "wsedload_wat_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("D1", "Almost no sediment (sediment load)"),
                                          2 : ("D2", "With sediment (sediment load)")}
        self.valid_level3_classes = [220, 227, 228]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((inttidal_wat_cat_l4a == 3) | (waterper_wat_cat_l4d > 0)) & (waterdpt_wat_cat_l4a > 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class MsubstrtWatCatL4m(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Substrate of non-perennial water/snow (msubstrt_wat_cat_l4m) MODIFIER

    Tier 4 class (220, 227, 228)

    Dependencies on class
    inttidal_wat_cat_l4a == 3 or waterper_wat_cat_l4d > 0 & != 1
    snowcper_wat_cat_l4d > 0 & != 1
    waterdpt_wat_cat > 0

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "msubstrt_wat_cat"
        self.output_layer_name = "msubstrt_wat_cat_l4m"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {4 : ("B4", "Bare rock substrate"),
                                          5 : ("B5", "Bare soil substrate"),
                                          6 : ("B6", "Sand substrate")}
        self.valid_level3_classes = [220, 227, 228]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((inttidal_wat_cat_l4a == 3) | ((waterper_wat_cat_l4d > 0) & (waterper_wat_cat_l4d != 1)) | \
             ((snowcper_wat_cat_l4d > 0) & (snowcper_wat_cat_l4d != 1))) & (waterdpt_wat_cat_l4a > 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MODIFIER


class BaresurfPhyCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Bare surface (baresurf_phy_cat_l4a)

    Tier 1 class

    Dependencies for class
    none

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "baresurf_phy_cat"
        self.output_layer_name = "baresurf_phy_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("A1", "Consolidated"),
                                          2 : ("A2", "Unconsolidated"),
                                          3 : ("A3", "Bare rock a/o coarse fragments"),
                                          4 : ("A4", "Hardpans"),
                                          5 : ("A5", "Unconsolidated bare soil and other unconsolidated material"),
                                          6 : ("A6", "Loose and shifting sands")}
        self.valid_level3_classes = [216]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class MbarematPhyCatL4m(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Bare surface materials (mbaremat_phy_cat_l4m) MODIFIER

    Tier 1 class (216)

    Dependencies for class
    baresurf_phy_cat_l4a == 3

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "mbaremat_phy_cat"
        self.output_layer_name = "mbaremat_phy_cat_l4m"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {7 : ("A7", "Bare rock"),
                                          8 : ("A8", "Gravel/stones/boulders"),
                                          14 : ("A14", "Gravel"),
                                          15 : ("A15", "Stones"),
                                          16 : ("A16", "Boulders")}
        self.valid_level3_classes = [216]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["baresurf_phy_cat_l4a == 3"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MODIFIER


class MustonesPhyCatL4m(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Unconsolidated material, gravels, stones, boulders (mustones_phy_cat_l4m) MODIFIER

    Tier 1 class (216)

    Dependencies for class
    if mustones_phy_cat_l4m == 12
        baresurf_phy_cat_l4a == 2 | 5 | 6
    if mustones_phy_cat_l4m == 13
        baresurf_phy_cat_l4a == 2 | 5

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "mustones_phy_cat"
        self.output_layer_name = "mustones_phy_cat_l4m"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {12 : ("A12", "Stony (5 to 40 %)"),
                                          13 : ("A13", "Very stony (40 to 80 %)")}
        self.valid_level3_classes = [216]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "(mustones_phy_cat_l4m == 12) & \
             ((baresurf_phy_cat_l4a == 2) | (baresurf_phy_cat_l4a == 5) | (baresurf_phy_cat_l4a == 6))",
            "(mustones_phy_cat_l4m == 13) & \
             ((baresurf_phy_cat_l4a == 2) | (baresurf_phy_cat_l4a == 5))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MODIFIER


class MhardpanPhyCatL4m(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Hardpans (mhardpan_phy_cat_l4m) MODIFIER

    Tier 1 class (216)

    Dependencies for class
    baresurf_phy_cat_l4a == 4

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "mhardpan_phy_cat"
        self.output_layer_name = "mhardpan_phy_cat_l4m"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {9 : ("A9", "Ironpan/Laterite"),
                                          10 : ("A10", "Petrocalcic"),
                                          11 : ("A11", "Petrogypsic")}
        self.valid_level3_classes = [216]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["baresurf_phy_cat_l4a == 4"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MODIFIER


class MacropatPhyCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Macropattern (macropat_phy_cat_l4a)

    Tier 2 class (216)

    Dependencies for class
    none

    Permitted additional attributes
    if macropat_phy_cat_l4a == 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10
        baresurf_phy_cat_l4a == 2 | 6
    if macropat_phy_cat_l4a == 11 | 12 | 13
        baresurf_phy_cat_l4a == 2 | 5
    """
    def __init__(self):
        self.input_layer_name = "macropat_phy_cat"
        self.output_layer_name = "macropat_phy_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("B1", "Dunes"),
                                          2 : ("B2", "Barchans"),
                                          3 : ("B3", "Parabolic dunes"),
                                          4 : ("B4", "Longitudinal dunes"),
                                          5 : ("B5", "Barchans (saturated)"),
                                          6 : ("B6", "Parabolic dunes (saturated)"),
                                          7 : ("B7", "Longitudinal dunes (saturated)"),
                                          8 : ("B8", "Barchans (unsaturated)"),
                                          9 : ("B9", "Parabolic dunes (unsaturated)"),
                                          10 : ("B10", "Longitudinal dunes (unsaturated)"),
                                          11 : ("B11", "Gilgai (soils)"),
                                          12 : ("B12", "Termite mounds (soils)"),
                                          13 : ("B13", "Salt flat")}
        self.valid_level3_classes = [216]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((macropat_phy_cat_l4a >= 1) & (macropat_phy_cat_l4a <= 10)) & \
                ((baresurf_phy_cat_l4a == 2) | (baresurf_phy_cat_l4a == 6))",
            "((macropat_phy_cat_l4a >= 11) & (macropat_phy_cat_l4a <= 13)) & \
                ((baresurf_phy_cat_l4a == 2) | (baresurf_phy_cat_l4a == 5))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class ArtisurfUrbCatL4a(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Artificial surfaces (artisurf_urb_cat_l4a)

    Tier 1 class (215)

    Dependencies for class
    none

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "artisurf_urb_cat"
        self.output_layer_name = "artisurf_urb_cat_l4a"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("A1", "Built up"),
                                          2 : ("A2", "Non-built up"),
                                          3 : ("A3", "Linear infrastructure"),
                                          4 : ("A4", "Non-linear infrastructure")}
        self.valid_level3_classes = [215]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class MtclineaUrbCatL4m(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Linearity of urban areas (mtclinea_urb_cat_l4m) MODIFIER

    Tier 1 class (215)

    Dependencies for class
    artisurf_urb_cat_l4a == 3

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "mtclinea_urb_cat"
        self.output_layer_name = "mtclinea_urb_cat_l4m"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {7 : ("A7", "Roads"),
                                          8 : ("A8", "Paved roads"),
                                          9 : ("A9", "Unpaved roads"),
                                          10 : ("A10", "Railways"),
                                          11 : ("A11", "Comm. Lines/Pipelines")}
        self.valid_level3_classes = [215]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["artisurf_urb_cat_l4a == 3"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MODIFIER


class MnolineaUrbCatL4m(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Linearity of urban areas (mnolinea_urb_cat_l4m) MODIFIER

    Tier 1 class (215)

    Dependencies for class
    artisurf_urb_cat_l4a == 4

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "mnolinea_urb_cat"
        self.output_layer_name = "mnolinea_urb_cat_l4m"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {12 : ("A12", "Industrial a/o other"),
                                          13 : ("A13", "Urban areas")}
        self.valid_level3_classes = [215]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["artisurf_urb_cat_l4a == 4"]

        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MODIFIER


class MartdensUrbCatL4m(l4_base.Level4ClassificationContinuousLayer):
    """
    Artificial density (martdens_urb_cat_l4d) MODIFIER
    Includes both Industrial a/o other and Urban areas
    >=15% density is considered artificial surfaces (artisurf_urb_cat)

    Tier 1 class (215)

    Dependencies for class
    artisurf_urb_cat_l4a == 4
    mnolinea_urb_cat_l4m == 12 | 13

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "martdens_urb_cin"
        self.output_layer_name = "martdens_urb_cat_l4d"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {14 : ("A14", "High (> 75 %) density"),
                                          15 : ("A15", "Medium (50 to 75 %) density"),
                                          16 : ("A16", "Low (30 to 50 %) density"),
                                          17 : ("A17", "Scattered (15 to 30 %) density")}
        self.class_boundaries = { 14 : (75, None),
                                  15 : (50, 75),
                                  16 : (30, 50),
                                  17 : (15, 30),
                                  0 : (None, 15)}
        self.valid_level3_classes = [215]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "(artisurf_urb_cat_l4a == 4) & ((mnolinea_urb_cat_l4m == 12) | (mnolinea_urb_cat_l4m == 13))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MODIFIER


class MnobuiltUrbCatL4m(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Non built-up (mnobuilt_urb_cat_l4m) MODIFIER

    Tier 1 class (215)

    Dependencies for class
    artisurf_urb_cat_l4a == 2

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "mnobuilt_urb_cat"
        self.output_layer_name = "mnobuilt_urb_cat_l4m"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {5 : ("A5", "Waste dump deposit"),
                                          6 : ("A6", "Excavation site")}
        self.valid_level3_classes = [215]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["artisurf_urb_cat_l4a == 2"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MODIFIER
