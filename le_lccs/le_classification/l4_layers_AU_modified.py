"""
Level 4 Classification Layers for modified scheme specific for Australia LCCS implementation

Currently used layers for Australia LCCS implementation (2020)
* indicates modification from LCCS layers base
LifeformVegCatL4a (only using Woody and Herbaceous categories)
CanopycoVegCatL4d* (modified categories to align with ICC tree/forest class definitions sep 2022)
WaterseaVegCatL4a* (requires removal of canopyht_veg_cat_l4d dependency as layer is not available)
WatersttWatCatL4a
WaterperWatCatL4d* (modified to have inputs as months)
InttidalWatCatL4a
BaregradPhyCatL4a* (new level 4 class specific for Australia implementation, not part of LCCS layers base)

Docstrings for each class below detail modifications from l4_layers_lccs.py
If no explanation given, assume identical to relevant class in l4_layers_lccs.py
"""

import logging

import numpy
import xarray

from . import l4_base

# Load standard l4 layers
from . import l4_layers_lccs

class CanopycoVegCatL4d_AU(l4_base.Level4ClassificationContinuousLayer):
    """
    Modified version of canopy cover for Australia implementation.
    utilised from Sep 2022, (previous published run on collection 2 data used
    FAO standard. Modified to comply with ICC definitions as per ABS request

     *    Closed (> 65 %)
     *    Open (40 to 65 %)
     *    Open (20 to 40 %)
     *    Sparse (4 to 20 %)
     *    Scattered (1 to 4 %)


    Tier 1 class (111, 112, 123, 124)

    Dependencies for class
    none

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "canopyco_veg_con"
        self.output_layer_name = "canopyco_veg_cat_l4d_au"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {10 : ("A10", "Closed (> 65 %)"),
                                          12 : ("A12", "Open (40 to 65 %)"),
                                          13 : ("A13", "Open (20 to 40 %)"),
                                          15 : ("A15", "Sparse (4 to 20 %)"),
                                          16 : ("A16", "Scattered (1 to 4 %)")}
        self.class_boundaries = {10 : (65, None),
                                 12 : (40, 65),
                                 13 : (20, 40),
                                 15 : (4, 20),
                                 16 : (1, 4),
                                 0  : (0, 1)}
        self.valid_level3_classes = [111, 112, 123, 124]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.DERIVATIVE



class WaterseaVegCatL4a_AU(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Modified version of water seasonality for Australia implementation.

    Removed dependency for canopyht_veg_cat_l4d as not routinely available
    for Australia implementation at present.

    Tier 2 class (124)

    Dependencies for class
    lifeform_veg_cat_l4a > 0
    canopyco_veg_cat_l4d_au > 0

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "watersea_veg_cat"
        self.output_layer_name = "watersea_veg_cat_l4a_au"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("C1", "Water > 3 months (semi-) permenant"),
                                          2 : ("C2", "Water < 3 months (temporary or seasonal)"),
                                          3 : ("C3", "Waterlogged"),
                                          4 : ("C4", "Water > 3 months (persistent all day)"),
                                          5 : ("C5", "Water > 3 months (with daily variations)")}
        self.valid_level3_classes = [124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["(lifeform_veg_cat_l4a > 0) & (canopyco_veg_cat_l4d_au > 0)"]
        # self.valid_level4_layers_filters = ["(lifeform_veg_cat_l4a > 0) & (canopyco_veg_cat_l4d_au > 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class WaterperWatCatL4d_AU(l4_base.Level4ClassificationContinuousLayer):
    """
    Water persistence (hydroperiod) (waterper_wat_cat_l4d) DERIVATIVE

    Modified version of water persistence for Australia implementation.
    Input values are in months rather than days.

    Tier 2 class (220, 227, 228)

    Dependencies on class
    waterstt_wat_cat_l4a == 1

    Permitted additional attributes
    inttidal_wat_cat_l4a == 0
    """
    def __init__(self):
        self.input_layer_name = "waterper_wat_cin"
        self.output_layer_name = "waterper_wat_cat_l4d_au"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = { 1 : ("B1", "Perennial (> 9 months)"),
                                           7 : ("B7", "Non-perennial (7 to 9 months)"),
                                           8 : ("B8", "Non-perennial (4 to 6 months)"),
                                           9 : ("B9", "Non-perennial (1 to 3 months)")}
        # In days. Use months multiplied by 30 to simplify
        self.class_boundaries = {1 : (9, None),
                                 7 : (7, 9),
                                 8 : (4, 7),
                                 9 : (1, 4),
                                 0 : (None, 1)}
        self.valid_level3_classes = [220, 227, 228]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = ["(waterstt_wat_cat_l4a == 1) & (inttidal_wat_cat_l4a == 0)"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.DERIVATIVE


class BaregradPhyCatL4d_AU(l4_base.Level4ClassificationContinuousLayer):
    """
    Bare gradation (baregrad_phy_cat_l4a)

    An additional level 4 class specific for Australia implementation.
    Current classification of level 1 (i.e. veg/non veg) provides extensive bare areas (216) at level 3.
    This is a balance of sensitivity and scalability of the level 1 approach.
    Decision was to modulate potential overclassification of bare areas by have a gradation level 4
    class to provide indication of how bare an area is as a primarily non-vegetated area.

    Input class is envisaged to be continuous data from Fraction Cover (i.e. BS endmember)
    * A10: Sparsely vegetated (BS < 20 %)
    * A12: Very sparsely vegetated (BS 20 to 60 %)
    * A15: Bare areas, unvegetated (BS > 60 %)

    Categories within baregrad_phy_cat_l4a have been taken from Albert Van Dyk's work of LCCS
    demonstration for ACT. These do not have definitions and are current open to interpretation.
    TODO: Documented definitions will be required for these categories at some point.

    This is currently the only bare areas level 4 class with available data, hence no
    dependencies are present. This will need review when more data is available for level 4
    classes of bare areas.

    Tier 1 class (216)

    Dependencies for class
    none

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "baregrad_phy_con"
        self.output_layer_name = "baregrad_phy_cat_l4d_au"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {10 : ("A10", "Sparsely vegetated"),
                                          12 : ("A12", "Very sparsely vegetated"),
                                          15 : ("A15", "Bare areas, unvegetated")}
        self.class_boundaries = {10 : (None, 20),
                                 12 : (20, 60),
                                 15 : (60, None)}
        self.valid_level3_classes = [216]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.DERIVATIVE

#: List of standard and modified layers used for AU classification
ALL_L4_LAYER_CLASSES_AU_MODIFIED = [l4_layers_lccs.LCCSLevel3,
                                    l4_layers_lccs.LifeformVegCatL4a,
                                    l4_layers_lccs.MlifefrmVegCatL4m,
                                    CanopycoVegCatL4d_AU,
                                    l4_layers_lccs.CanopyhtVegCatL4d,
                                    l4_layers_lccs.WaterdayAgrCatL4a,
                                    WaterseaVegCatL4a_AU,
                                    l4_layers_lccs.LeaftypeVegCatL4a,
                                    l4_layers_lccs.PhenologVegCatL4a,
                                    l4_layers_lccs.MphenlogVegCatL4m,
                                    l4_layers_lccs.UrbanvegUrbCatL4a,
                                    l4_layers_lccs.SpatdistVegCatL4a,
                                    l4_layers_lccs.Strat2ndVegCatL4a,
                                    l4_layers_lccs.Lifef2ndVegCatL4a,
                                    l4_layers_lccs.Cover2ndVegCatL4d,
                                    l4_layers_lccs.Heigh2ndVegCatL4d,
                                    l4_layers_lccs.SpatsizeAgrCatL4a,
                                    l4_layers_lccs.CropcombAgrCatL4a,
                                    l4_layers_lccs.Croplfc2AgrCatL4a,
                                    l4_layers_lccs.Croplfc3AgrCatL4a,
                                    l4_layers_lccs.CropseqtAgrCatL4a,
                                    l4_layers_lccs.CropseqaAgrCatL4a,
                                    l4_layers_lccs.WatersupAgrCatL4a,
                                    l4_layers_lccs.TimefactAgrCatL4a,
                                    l4_layers_lccs.WatersttWatCatL4a,
                                    l4_layers_lccs.InttidalWatCatL4a,
                                    WaterperWatCatL4d_AU,
                                    l4_layers_lccs.SnowcperWatCatL4d,
                                    l4_layers_lccs.WaterdptWatCatL4a,
                                    l4_layers_lccs.MwatrmvtWatCatL4m,
                                    l4_layers_lccs.WsedloadWatCatL4a,
                                    l4_layers_lccs.MsubstrtWatCatL4m,
                                    BaregradPhyCatL4d_AU,
                                    l4_layers_lccs.BaresurfPhyCatL4a,
                                    l4_layers_lccs.MbarematPhyCatL4m,
                                    l4_layers_lccs.MustonesPhyCatL4m,
                                    l4_layers_lccs.MhardpanPhyCatL4m,
                                    l4_layers_lccs.MacropatPhyCatL4a,
                                    l4_layers_lccs.ArtisurfUrbCatL4a,
                                    l4_layers_lccs.MtclineaUrbCatL4m,
                                    l4_layers_lccs.MnolineaUrbCatL4m,
                                    l4_layers_lccs.MartdensUrbCatL4m,
                                    l4_layers_lccs.MnobuiltUrbCatL4m]
