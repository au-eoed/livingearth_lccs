"""
Functions to produce maps showing observed change. These are used to combined change
 from multiple classes into something meaningful and useful to policy.

IMPORTANT: The first 12 classes are level 3 changes only. These are intended to be displayed
individually as each pixel can have two states of change (i.e. CTV --> AS is both increase in extent of
artificial surface and decrease in extent of cultivated terrestrial vegetation

For EBC classes (i.e. level 4 variables) some may seem to be missing. Provided in the docstrings
are explanations of why certain potential changes were omitted. Common reasoning is that the change
did not provide useful information and could be incorporated into another class.

If it seems like classes are missing or reasoning is not given please open an issue for further discussion.

NOTE: Cultivated aquatic vegetation (CAV) does not have any EBC at present due to absence from
current Australian LCCS implementation

"""

from abc import abstractmethod
import logging

import numpy
import xarray

from . import l4_layers_lccs
from . import lccs_l4

# Create empty variables here and populate after each pair
ALL_OBSERVED_CHANGE_CLASSES = []
ALL_OBSERVED_CHANGE_PAIRS = {}

class ObservedChangeBase(object):
    """
    Base class for evidence based change
    """
    def __init__(self):
        # Output code for when combining to a single change image
        # Must be unique
        self.output_code = 0
        # Direction of change. 1 indicates gain, 2 indicates loss
        self.change_direction = 0
        # Driver of change (used if exporting to a format supporting text attributes)
        self.change_driver = ""
        # Description of observed change (used if exporting to a format supporting text attributes)
        self.observed_change = ""
        # Description of reason for change (used if exporting to a format supporting text attributes)
        self.observed_change_reason = ""
        # Set colours (R, G, B, A)
        self.colours = (0, 0, 0, 0)

    @abstractmethod
    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):
        raise NotImplementedError("The class you are trying to use has"
                                  " not provided an implementation for"
                                  " this function")

    def get_change_colours(self, output_change_numpy, red, green, blue, alpha):
        """
        Get colours for class
        """
        subset = (output_change_numpy == self.output_code)
        red[subset], green[subset], blue[subset], alpha[subset] = self.colours

        return red, green, blue, alpha


class CTVExtentGain(ObservedChangeBase):
    """
    Cultivated terrestrial vegetation increase in extent

    This occurs where any other level 3 class changes to CTV (111)
    """
    def __init__(self):
        self.output_code = 1
        self.change_direction = 1
        self.change_driver = "Human-induced/ or Biotic"
        self.observed_change = "Increase in agricultural extent"
        self.observed_change_reason = "Agricultural expansion"
        self.colours = (27, 183, 12, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        output_change_numpy = numpy.where(((input_l4_change_xarray["level3_change"].values == 112111) |
                                        (input_l4_change_xarray["level3_change"].values == 123111) |
                                        (input_l4_change_xarray["level3_change"].values == 124111) |
                                        (input_l4_change_xarray["level3_change"].values == 215111) |
                                        (input_l4_change_xarray["level3_change"].values == 216111) |
                                        (input_l4_change_xarray["level3_change"].values == 220111)),
                                       out_val, output_change_numpy)
        return output_change_numpy

class CTVExtentLoss(ObservedChangeBase):
    """
    Cultivated terrestrial vegetation decrease in extent

    This occurs where CTV (111) changes to any other level 3 class
    """
    def __init__(self):
        self.output_code = 2
        self.change_direction = 2
        self.change_driver = "Human-induced, biotic or abiotic"
        self.observed_change = "Decrease in agricultural extent"
        self.observed_change_reason = "Crop loss (permanent or temporary, including through overgrazing)"
        self.colours = (251, 104, 101, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        output_change_numpy = numpy.where(((input_l4_change_xarray["level3_change"].values == 111112) |
                                        (input_l4_change_xarray["level3_change"].values == 111123) |
                                        (input_l4_change_xarray["level3_change"].values == 111124) |
                                        (input_l4_change_xarray["level3_change"].values == 111215) |
                                        (input_l4_change_xarray["level3_change"].values == 111216) |
                                        (input_l4_change_xarray["level3_change"].values == 111220)),
                                       out_val, output_change_numpy)
        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["ctv_extent"] = [CTVExtentGain, CTVExtentLoss]
ALL_OBSERVED_CHANGE_CLASSES.extend([CTVExtentGain, CTVExtentLoss])

class NTVExtentGain(ObservedChangeBase):
    """
    Natural terrestrial vegetation increase in extent

    This occurs where any other level 3 class changes to NTV (112)
    """
    def __init__(self):
        self.output_code = 3
        self.change_direction = 1
        self.change_driver = "Human-induced or Biotic"
        self.observed_change = "Increase in natural vegetation extent"
        self.observed_change_reason = "Encroachment, reforestation (natural or  plantations), regrowth, \
                                       restoration, revegetation or wildfire recovery."
        self.colours = (27, 183, 12, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        output_change_numpy = numpy.where(((input_l4_change_xarray["level3_change"].values == 111112) |
                                        (input_l4_change_xarray["level3_change"].values == 123112) |
                                        (input_l4_change_xarray["level3_change"].values == 124112) |
                                        (input_l4_change_xarray["level3_change"].values == 215112) |
                                        (input_l4_change_xarray["level3_change"].values == 216112) |
                                        (input_l4_change_xarray["level3_change"].values == 220112)),
                                       out_val, output_change_numpy)
        return output_change_numpy


class NTVExtentLoss(ObservedChangeBase):
    """
    Natural terrestrial vegetation decrease in extent

    This occurs where NTV (112) changes to any other level 3 class
    """
    def __init__(self):
        self.output_code = 4
        self.change_direction = 2
        self.change_driver = "Human-induced, biotic or abiotic"
        self.observed_change = "Decrease in natural vegetation extent"
        self.observed_change_reason = "Wildfire, desertification or deforestation."
        self.colours = (251, 104, 101, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        output_change_numpy = numpy.where(((input_l4_change_xarray["level3_change"].values == 112111) |
                                        (input_l4_change_xarray["level3_change"].values == 112123) |
                                        (input_l4_change_xarray["level3_change"].values == 112124) |
                                        (input_l4_change_xarray["level3_change"].values == 112215) |
                                        (input_l4_change_xarray["level3_change"].values == 112216) |
                                        (input_l4_change_xarray["level3_change"].values == 112220)),
                                       out_val, output_change_numpy)
        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["ntv_extent"] = [NTVExtentGain, NTVExtentLoss]
ALL_OBSERVED_CHANGE_CLASSES.extend([NTVExtentGain, NTVExtentLoss])

class NAVExtentGain(ObservedChangeBase):
    """
    Natural aquatic vegetation increase in extent

    This occurs where any other level 3 class changes to NAV (124)
    """
    def __init__(self):
        self.output_code = 5
        self.change_direction = 1
        self.change_driver = "Human-induced or Biotic"
        self.observed_change = "Increase in aquatic vegetation extent"
        self.observed_change_reason = "Algal bloom (high water temperatures) or vegetation increase \
                                    (encroachment, reforestation (natural), regrowth, restoration or revegetation)."
        self.colours = (7, 229, 170, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        output_change_numpy = numpy.where(((input_l4_change_xarray["level3_change"].values == 111124) |
                                        (input_l4_change_xarray["level3_change"].values == 112124) |
                                        (input_l4_change_xarray["level3_change"].values == 123124) |
                                        (input_l4_change_xarray["level3_change"].values == 215124) |
                                        (input_l4_change_xarray["level3_change"].values == 216124) |
                                        (input_l4_change_xarray["level3_change"].values == 220124)),
                                       out_val, output_change_numpy)
        return output_change_numpy

class NAVExtentLoss(ObservedChangeBase):
    """
    Natural aquatic vegetation decrease in extent

    This occurs where NAV (124) changes to any other level 3 class
    """
    def __init__(self):
        self.output_code = 6
        self.change_direction = 2
        self.change_driver = "Human-induced, Biotic or Abiotic"
        self.observed_change = "Decrease in aquatic vegetation extent"
        self.observed_change_reason = "Dieback (drought, sea level fluctuation or storms, prolonged inundation) \
                                    or deforestation."
        self.colours = (204, 158, 247, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        output_change_numpy = numpy.where(((input_l4_change_xarray["level3_change"].values == 124111) |
                                        (input_l4_change_xarray["level3_change"].values == 124112) |
                                        (input_l4_change_xarray["level3_change"].values == 124123) |
                                        (input_l4_change_xarray["level3_change"].values == 124215) |
                                        (input_l4_change_xarray["level3_change"].values == 124216) |
                                        (input_l4_change_xarray["level3_change"].values == 124220)),
                                       out_val, output_change_numpy)
        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["nav_extent"] = [NAVExtentGain, NAVExtentLoss]
ALL_OBSERVED_CHANGE_CLASSES.extend([NAVExtentGain, NAVExtentLoss])

class ASExtentGain(ObservedChangeBase):
    """
    Artificial surface increase in extent

    This occurs where any other level 3 class changes to AS (215)
    """
    def __init__(self):
        self.output_code = 7
        self.change_direction = 1
        self.change_driver = "Human-induced"
        self.observed_change = "Increase in artificial surface extent"
        self.observed_change_reason = "Urban expansion or construction works"
        self.colours = (248, 3, 224, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        output_change_numpy = numpy.where(((input_l4_change_xarray["level3_change"].values == 111215) |
                                        (input_l4_change_xarray["level3_change"].values == 112215) |
                                        (input_l4_change_xarray["level3_change"].values == 123215) |
                                        (input_l4_change_xarray["level3_change"].values == 124215) |
                                        (input_l4_change_xarray["level3_change"].values == 216215) |
                                        (input_l4_change_xarray["level3_change"].values == 220215)),
                                       out_val, output_change_numpy)
        return output_change_numpy

class ASExtentLoss(ObservedChangeBase):
    """
    Artificial surface decrease in extent

    This occurs where AS (215) changes to any other level 3 class
    """
    def __init__(self):
        self.output_code = 8
        self.change_direction = 2
        self.change_driver = "Human-induced"
        self.observed_change = "Decrease in artificial surface extent"
        self.observed_change_reason = "Urban decay (abandonment - buildings, mines, railways, roads), \
                                    dam wall failure or damage (fire, flooding, increased wind) or \
                                    mine abandonment (reduced resources or demand)"
        self.colours = (225, 225, 225, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        output_change_numpy = numpy.where(((input_l4_change_xarray["level3_change"].values == 215111) |
                                        (input_l4_change_xarray["level3_change"].values == 215112) |
                                        (input_l4_change_xarray["level3_change"].values == 215123) |
                                        (input_l4_change_xarray["level3_change"].values == 215124) |
                                        (input_l4_change_xarray["level3_change"].values == 215216) |
                                        (input_l4_change_xarray["level3_change"].values == 215220)),
                                       out_val, output_change_numpy)
        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["as_extent"] = [ASExtentGain, ASExtentLoss]
ALL_OBSERVED_CHANGE_CLASSES.extend([ASExtentGain, ASExtentLoss])

class BSExtentGain(ObservedChangeBase):
    """
    Natural bare surface increase in extent

    This occurs where any other level 3 class changes to BS (216)
    """
    def __init__(self):
        self.output_code = 9
        self.change_direction = 1
        self.change_driver = "Human-induced or abiotic"
        self.observed_change = "Increase in bare surface extent"
        self.observed_change_reason = "Bare soil exposure (ploughing or tillage), erosion \
                                    (flooding or increased wind) or loss of vegetation"
        self.colours = (248, 3, 224, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        output_change_numpy = numpy.where(((input_l4_change_xarray["level3_change"].values == 111216) |
                                        (input_l4_change_xarray["level3_change"].values == 112216) |
                                        (input_l4_change_xarray["level3_change"].values == 123216) |
                                        (input_l4_change_xarray["level3_change"].values == 124216) |
                                        (input_l4_change_xarray["level3_change"].values == 215216) |
                                        (input_l4_change_xarray["level3_change"].values == 220216)),
                                       out_val, output_change_numpy)
        return output_change_numpy

class BSExtentLoss(ObservedChangeBase):
    """
    Natural bare surface decrease in extent

    This occurs where BS (216) changes to any other level 3 class
    """
    def __init__(self):
        self.output_code = 10
        self.change_direction = 2
        self.change_driver = "Human-induced or abiotic"
        self.observed_change = "Decrease in bare surface extent"
        self.observed_change_reason = "Urban development \
                                    (construction - aquaculture farm, dams, mining, roads, railways, levelling), \
                                    revegetation, flooding (natural or anthropogenic)"
        self.colours = (225, 225, 225, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        output_change_numpy = numpy.where(((input_l4_change_xarray["level3_change"].values == 216111) |
                                        (input_l4_change_xarray["level3_change"].values == 216112) |
                                        (input_l4_change_xarray["level3_change"].values == 216123) |
                                        (input_l4_change_xarray["level3_change"].values == 216124) |
                                        (input_l4_change_xarray["level3_change"].values == 216215) |
                                        (input_l4_change_xarray["level3_change"].values == 216220)),
                                       out_val, output_change_numpy)
        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["bs_extent"] = [BSExtentGain, BSExtentLoss]
ALL_OBSERVED_CHANGE_CLASSES.extend([BSExtentGain, BSExtentLoss])

class WExtentGain(ObservedChangeBase):
    """
    Water body increase in extent

    This occurs where any other level 3 class changes to Water (220)
    """
    def __init__(self):
        self.output_code = 11
        self.change_direction = 1
        self.change_driver = "Human-induced or abiotic"
        self.observed_change = "Increase in water body extent"
        self.observed_change_reason = "Inundation (increased precipitation), \
                                    sea level rise (melting ice sheets/glaciers, thermal expansion) or \
                                    flooding (natural or anthropogenic)."
        self.colours = (90, 96, 248, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        output_change_numpy = numpy.where(((input_l4_change_xarray["level3_change"].values == 111220) |
                                        (input_l4_change_xarray["level3_change"].values == 112220) |
                                        (input_l4_change_xarray["level3_change"].values == 123220) |
                                        (input_l4_change_xarray["level3_change"].values == 124220) |
                                        (input_l4_change_xarray["level3_change"].values == 215220) |
                                        (input_l4_change_xarray["level3_change"].values == 216220)),
                                       out_val, output_change_numpy)
        return output_change_numpy

class WExtentLoss(ObservedChangeBase):
    """
    Water body decrease in extent

    This occurs where Water (220) changes to any other level 3 class
    """
    def __init__(self):
        self.output_code = 12
        self.change_direction = 2
        self.change_driver = "Human-induced or abiotic"
        self.observed_change = "Decrease in water body extent"
        self.observed_change_reason = "Water abstraction, dam failure or removal, draining of waterbodies, \
                                    drying, evaporation or land reclaimation."
        self.colours = (196, 156, 251, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        output_change_numpy = numpy.where(((input_l4_change_xarray["level3_change"].values == 220111) |
                                        (input_l4_change_xarray["level3_change"].values == 220112) |
                                        (input_l4_change_xarray["level3_change"].values == 220123) |
                                        (input_l4_change_xarray["level3_change"].values == 220124) |
                                        (input_l4_change_xarray["level3_change"].values == 220215) |
                                        (input_l4_change_xarray["level3_change"].values == 220216)),
                                       out_val, output_change_numpy)
        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["w_extent"] = [WExtentGain, WExtentLoss]
ALL_OBSERVED_CHANGE_CLASSES.extend([WExtentGain, WExtentLoss])

class CTVGreening(ObservedChangeBase):
    """
    Increase in photosynthetic activity of cultivated terrestrial vegetation

    Conditions:
    level 3 == 111
    lifeform == no data | no change | woody | herbaceous
    canopy cover = increase

    EBC classes for greening of woody/herbaceous CTV are included here as independent classes
    for each CTV lifeform are deemed unnessarly (as CTV lifeform is unlikely to vary in the landscape)
    """
    def __init__(self):
        self.output_code = 13
        self.change_direction = 1
        self.change_driver = "Human-induced or Biotic"
        self.observed_change = "Increased photosynthetic activity of agricultural areas"
        self.observed_change_reason = "Crop fertilisation, growth, irrigation or rainfall increase"
        self.colours = (40, 245, 13, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Required layers
        level3_change_mask = ((input_l4_change_xarray["level3_change"].values == 111111) |
                              ((input_l4_date1_xarray["level3"] == 111) & (input_l4_date2_xarray["level3"] == 111)))

        # date1 > date2 as canopyco categories are 10 (closed) to 16 (sparse)
        canopy_cover_change_mask = (input_l4_date1_xarray["canopyco_veg_cat_l4d"] > input_l4_date2_xarray["canopyco_veg_cat_l4d"])

        # Need to allow for level3_change value of 0 so look at individual values
        output_change_numpy = numpy.where((level3_change_mask & canopy_cover_change_mask),
                                        out_val, output_change_numpy)

        return output_change_numpy

class CTVBrowning(ObservedChangeBase):
    """
    Decrease in photosynthetic activity of cultivated terrestrial vegetation

    Conditions:
    level 3 == 111
    lifeform == no data | no change | woody | herbaceous
    canopy cover = decrease

    EBC classes for browning of woody/herbaceous CTV are included here as independent classes
    for each CTV lifeform are deemed unnessarly (as CTV lifeform is unlikely to vary in the landscape)
    """
    def __init__(self):
        self.output_code = 14
        self.change_direction = 2
        self.change_driver = "Biotic"
        self.observed_change = "Decreased photosynthetic activity of agricultural areas"
        self.observed_change_reason = "Seasonal phenology, drought or dieback"
        self.colours = (209, 166, 110, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Required layers
        level3_change_mask = ((input_l4_change_xarray["level3_change"].values == 111111) |
                              ((input_l4_date1_xarray["level3"] == 111) & (input_l4_date2_xarray["level3"] == 111)))

        # date1 < date2 as canopyco categories are 10 (closed) to 16 (sparse)
        canopy_cover_change_mask = (input_l4_date1_xarray["canopyco_veg_cat_l4d"] < input_l4_date2_xarray["canopyco_veg_cat_l4d"])

        # Need to allow for level3_change value of 0 so look at individual values
        output_change_numpy = numpy.where((level3_change_mask & canopy_cover_change_mask),
                                        out_val, output_change_numpy)

        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["ctv_ps"] = [CTVGreening, CTVBrowning]
ALL_OBSERVED_CHANGE_CLASSES.extend([CTVGreening, CTVBrowning])


class NAVGreening(ObservedChangeBase):
    """
    Increase in photosynthetic activity of natural aquatic vegetation

    Conditions:
    level 3 == 124
    lifeform == no data | no change | woody | herbaceous
    canopy cover = increase
    water seasonality = no data | no change | increase | decrease

    EBC classes for greening of woody/herbaceous NAV are included here as independent classes
    for each NAV lifeform are deemed unnessarly (current Australia LCCS implementation only identifies
    mangrove for NAV, therefore lifeform should always be woody)
    """
    def __init__(self):
        self.output_code = 15
        self.change_direction = 1
        self.change_driver = "Biotic"
        self.observed_change = "Increased photosynthetic activity of aquatic vegetated areas"
        self.observed_change_reason = "Vegetation growth or forest thickening"
        self.colours = (40, 245, 13, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Required layers
        level3_change_mask = ((input_l4_change_xarray["level3_change"].values == 124124) |
                              ((input_l4_date1_xarray["level3"] == 124) & (input_l4_date2_xarray["level3"] == 124)))

        # date1 > date2 as canopyco categories are 10 (closed) to 16 (sparse)
        canopy_cover_change_mask = (input_l4_date1_xarray["canopyco_veg_cat_l4d"] > input_l4_date2_xarray["canopyco_veg_cat_l4d"])

        # Need to allow for level3_change value of 0 so look at individual values
        output_change_numpy = numpy.where((level3_change_mask & canopy_cover_change_mask),
                                        out_val, output_change_numpy)

        return output_change_numpy

class NAVBrowning(ObservedChangeBase):
    """
    Decrease in photosynthetic activity of cultivated terrestrial vegetation

    Conditions:
    level 3 == 124
    lifeform == no data | no change | woody | herbaceous
    canopy cover = decrease
    water seasonality = no data | no change | increase | decrease

    EBC classes for browning of woody/herbaceous NAV are included here as independent classes
    for each NAV lifeform are deemed unnessarly (current Australia LCCS implementation only identifies
    mangrove for NAV, therefore lifeform should always be woody)
    """
    def __init__(self):
        self.output_code = 16
        self.change_direction = 2
        self.change_driver = "Biotic or abiotic"
        self.observed_change = "Decreased photosynthetic activity of aquatic vegetated areas"
        self.observed_change_reason = "Dieback or damage (e.g. wind, herbivory)"
        self.colours = (209, 166, 110, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Required layers
        level3_change_mask = ((input_l4_change_xarray["level3_change"].values == 124124) |
                              ((input_l4_date1_xarray["level3"] == 124) & (input_l4_date2_xarray["level3"] == 124)))

        # date1 < date2 as canopyco categories are 10 (closed) to 16 (sparse)
        canopy_cover_change_mask = (input_l4_date1_xarray["canopyco_veg_cat_l4d"] < input_l4_date2_xarray["canopyco_veg_cat_l4d"])

        # Need to allow for level3_change value of 0 so look at individual values
        output_change_numpy = numpy.where((level3_change_mask & canopy_cover_change_mask),
                                        out_val, output_change_numpy)

        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["nav_ps"] = [NAVGreening, NAVBrowning]
ALL_OBSERVED_CHANGE_CLASSES.extend([NAVGreening, NAVBrowning])

class NTVWoodyGreening(ObservedChangeBase):
    """
    Increase in photosynthetic activity of natural terrestrial woody vegetation

    Conditions:
    level 3 == 112
    lifeform == woody
    canopy cover = increase
    """
    def __init__(self):
        self.output_code = 17
        self.change_direction = 1
        self.change_driver = "Biotic"
        self.observed_change = "Increased photosynthetic activity of woody vegetated areas"
        self.observed_change_reason = "Vegetation growth or forest thickening"
        self.colours = (5, 213, 53, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Required layers
        level3_change_mask = ((input_l4_change_xarray["level3_change"].values == 112112) |
                              ((input_l4_date1_xarray["level3"] == 112) & (input_l4_date2_xarray["level3"] == 112)))

        lifeform_change_mask = ((input_l4_change_xarray["lifeform_veg_cat_l4a_change"].values == 101) |
                                (input_l4_date1_xarray["lifeform_veg_cat_l4a"] == 1) & (input_l4_date2_xarray["lifeform_veg_cat_l4a"] == 1))

        # date1 > date2 as canopyco categories are 10 (closed) to 16 (sparse)
        canopy_cover_change_mask = (input_l4_date1_xarray["canopyco_veg_cat_l4d"] > input_l4_date2_xarray["canopyco_veg_cat_l4d"])

        # Need to allow for level3_change value of 0 so look at individual values
        output_change_numpy = numpy.where((level3_change_mask & lifeform_change_mask & canopy_cover_change_mask),
                                        out_val, output_change_numpy)

        return output_change_numpy

class NTVWoodyBrowning(ObservedChangeBase):
    """
    Decrease in photosynthetic activity of natural terrestrial woody vegetation

    Conditions:
    level 3 == 112
    lifeform == woody
    canopy cover = decrease
    """
    def __init__(self):
        self.output_code = 18
        self.change_direction = 2
        self.change_driver = "Biotic or abiotic"
        self.observed_change = "Decreased photosynthetic activity of woody vegetated areas"
        self.observed_change_reason = "Dieback or damage (e.g. wind, herbivory)"
        self.colours = (221, 139, 72, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Required layers
        level3_change_mask = ((input_l4_change_xarray["level3_change"].values == 112112) |
                              ((input_l4_date1_xarray["level3"] == 112) & (input_l4_date2_xarray["level3"] == 112)))

        lifeform_change_mask = ((input_l4_change_xarray["lifeform_veg_cat_l4a_change"].values == 101) |
                                (input_l4_date1_xarray["lifeform_veg_cat_l4a"] == 1) & (input_l4_date2_xarray["lifeform_veg_cat_l4a"] == 1))

        # date1 < date2 as canopyco categories are 10 (closed) to 16 (sparse)
        canopy_cover_change_mask = (input_l4_date1_xarray["canopyco_veg_cat_l4d"] < input_l4_date2_xarray["canopyco_veg_cat_l4d"])

        # Need to allow for level3_change value of 0 so look at individual values
        output_change_numpy = numpy.where((level3_change_mask & lifeform_change_mask & canopy_cover_change_mask),
                                        out_val, output_change_numpy)

        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["ntv_woody_ps"] = [NTVWoodyGreening, NTVWoodyBrowning]
ALL_OBSERVED_CHANGE_CLASSES.extend([NTVWoodyGreening, NTVWoodyBrowning])

class NTVHerbaceousGreening(ObservedChangeBase):
    """
    Increase in photosynthetic activity of natural terrestrial herbaceous vegetation

    Conditions:
    level 3 == 112
    lifeform == herbaceous
    canopy cover = increase
    """
    def __init__(self):
        self.output_code = 19
        self.change_direction = 1
        self.change_driver = "Biotic"
        self.observed_change = "Increased photosynthetic activity of herbaceous vegetated areas"
        self.observed_change_reason = "Vegetation growth or forest thickening"
        self.colours = (187, 248, 4, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Required layers
        level3_change_mask = ((input_l4_change_xarray["level3_change"].values == 112112) |
                              ((input_l4_date1_xarray["level3"] == 112) & (input_l4_date2_xarray["level3"] == 112)))

        lifeform_change_mask = ((input_l4_change_xarray["lifeform_veg_cat_l4a_change"].values == 202) |
                                (input_l4_date1_xarray["lifeform_veg_cat_l4a"] == 2) & (input_l4_date2_xarray["lifeform_veg_cat_l4a"] == 2))

        # date1 > date2 as canopyco categories are 10 (closed) to 16 (sparse)
        canopy_cover_change_mask = (input_l4_date1_xarray["canopyco_veg_cat_l4d"] > input_l4_date2_xarray["canopyco_veg_cat_l4d"])

        # Need to allow for level3_change value of 0 so look at individual values
        output_change_numpy = numpy.where((level3_change_mask & lifeform_change_mask & canopy_cover_change_mask),
                                        out_val, output_change_numpy)

        return output_change_numpy

class NTVHerbaceousBrowning(ObservedChangeBase):
    """
    Decrease in photosynthetic activity of natural terrestrial herbaceous vegetation

    Conditions:
    level 3 == 112
    lifeform == herbaceous
    canopy cover = decrease
    """
    def __init__(self):
        self.output_code = 20
        self.change_direction = 2
        self.change_driver = "Biotic or abiotic"
        self.observed_change = "Decreased photosynthetic activity of herbaceous vegetated areas"
        self.observed_change_reason = "Dieback or damage (e.g. wind, herbivory)"
        self.colours = (250, 180, 239, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Required layers
        level3_change_mask = ((input_l4_change_xarray["level3_change"].values == 112112) |
                              ((input_l4_date1_xarray["level3"] == 112) & (input_l4_date2_xarray["level3"] == 112)))

        lifeform_change_mask = ((input_l4_change_xarray["lifeform_veg_cat_l4a_change"].values == 202) |
                                (input_l4_date1_xarray["lifeform_veg_cat_l4a"] == 2) & (input_l4_date2_xarray["lifeform_veg_cat_l4a"] == 2))

        # date1 < date2 as canopyco categories are 10 (closed) to 16 (sparse)
        canopy_cover_change_mask = (input_l4_date1_xarray["canopyco_veg_cat_l4d"] < input_l4_date2_xarray["canopyco_veg_cat_l4d"])

        # Need to allow for level3_change value of 0 so look at individual values
        output_change_numpy = numpy.where((level3_change_mask & lifeform_change_mask & canopy_cover_change_mask),
                                        out_val, output_change_numpy)

        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["ntv_herbaceous_ps"] = [NTVHerbaceousGreening, NTVHerbaceousBrowning]
ALL_OBSERVED_CHANGE_CLASSES.extend([NTVHerbaceousGreening, NTVHerbaceousBrowning])

class NTVWoodyExtentGain(ObservedChangeBase):
    """
    Increase in extent of natural terrestrial woody vegetation to herbaceous
    (class is not equivalent to decrease in extent of natural terrestrial herbaceous vegetation as we incorperate
    areas that have changed to woody NTV from any other level 3 class i.e. could be from BS, therefore
    gain in NTV woody extent would not be equivalent to decrease in NTV herbaceous extent)

    Conditions:
    level 3 == 112
    lifeform == herbaceous --> woody
    or
    level 3 changed to 112
    lifeform == woody
    canopy cover = no data | no change | increase | decrease

    EBC classes for greening/browning when NTV lifeform changes from herbaceous to woody are included here, as independent
    classes for each NTV greening/browning when lifeform changes are deemed unnecessary (where lifeform changes this is
    a significant landscape change and attempting to incorperate a measure of photosynthetic activity would not value
    add any useful information)
    """
    def __init__(self):
        self.output_code = 21
        self.change_direction = 1
        self.change_driver = "Human-induced, biotic or abiotic"
        self.observed_change = "Increase in natural woody vegetation extent"
        self.observed_change_reason = "Reforestation, restoration or revegetation"
        self.colours = (5, 213, 53, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Change type 1: Stays NTV, goes from herbaceous to woody
        # Level 3, no change to NTV (Need to allow for level3_change value of 0 so look at individual values)
        level3_change_mask1 = ((input_l4_change_xarray["level3_change"].values == 112112) |
                              ((input_l4_date1_xarray["level3"] == 112) & (input_l4_date2_xarray["level3"] == 112)))

        # Level 4, change from herbaceous (2) --> woody (1)
        lifeform_change_mask1 = (input_l4_change_xarray["lifeform_veg_cat_l4a_change"].values == 201)

        # Change type 2: Change from another L3 class to NTV woody
        # Level 3, anything to NTV
        level3_change_mask2 =  ((input_l4_change_xarray["level3_change"].values == 111112) |
                                (input_l4_change_xarray["level3_change"].values == 123112) |
                                (input_l4_change_xarray["level3_change"].values == 124112) |
                                (input_l4_change_xarray["level3_change"].values == 215112) |
                                (input_l4_change_xarray["level3_change"].values == 216112) |
                                (input_l4_change_xarray["level3_change"].values == 220112))

        # change to woody (1) in date2
        lifeform_change_mask2 = input_l4_date2_xarray["lifeform_veg_cat_l4a"] == 1

        output_change_numpy = numpy.where( ((level3_change_mask1 & lifeform_change_mask1) | (level3_change_mask2 & lifeform_change_mask2)),
                                        out_val, output_change_numpy)

        return output_change_numpy

class NTVWoodyExtentLoss(ObservedChangeBase):
    """
    Decrease in extent of natural terrestrial woody vegetation to herbaceous
    (class is not equivalent to increase in extent of natural terrestrial herbaceous vegetation as we incorperate
    areas that have changed from woody NTV to any other level 3 class i.e. could be to BS, therefore
    loss in NTV woody extent would not be equivalent to increase in NTV herbaceous extent)

    Conditions:
    level 3 == 112
    lifeform ==  woody --> herbaceous
    or
    level 3 == 112 & level 4 == woody --> level 3 != 112
    canopy cover = no data | no change | increase | decrease

    EBC classes for greening/browning when NTV lifeform changes from woody to other are included here, as independent
    classes for each NTV greening/browning when lifeform changes are deemed unnecessary (where lifeform changes this is
    a significant landscape change and attempting to incorperate a measure of photosynthetic activity would not value
    add any useful information)
    """
    def __init__(self):
        self.output_code = 22
        self.change_direction = 2
        self.change_driver = "Human-induced, biotic or abiotic"
        self.observed_change = "Decrease in natural woody vegetation extent"
        self.observed_change_reason = "Dieback, deforestation"
        self.colours = (221, 139, 72, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Change type 1: Stays NTV, goes from woody to herbaceous
        # Level 3, no change to NTV (Need to allow for level3_change value of 0 so look at individual values)
        level3_change_mask1 = ((input_l4_change_xarray["level3_change"].values == 112112) |
                              ((input_l4_date1_xarray["level3"] == 112) & (input_l4_date2_xarray["level3"] == 112)))

        # Level 4, change from woody (1) --> herbaceous (2)
        lifeform_change_mask1 = (input_l4_change_xarray["lifeform_veg_cat_l4a_change"].values == 102)

        # Change type 2: Change from NTV woody to a different L3 class
        # Level 3, NTV to anything
        level3_change_mask2 =  ((input_l4_change_xarray["level3_change"].values == 112111) |
                                (input_l4_change_xarray["level3_change"].values == 112123) |
                                (input_l4_change_xarray["level3_change"].values == 112124) |
                                (input_l4_change_xarray["level3_change"].values == 112215) |
                                (input_l4_change_xarray["level3_change"].values == 112216) |
                                (input_l4_change_xarray["level3_change"].values == 112220))

        # change from woody (1) in date1
        lifeform_change_mask2 = input_l4_date1_xarray["lifeform_veg_cat_l4a"] == 1

        output_change_numpy = numpy.where( ((level3_change_mask1 & lifeform_change_mask1) | (level3_change_mask2 & lifeform_change_mask2)),
                                        out_val, output_change_numpy)

        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["ntv_woody_extent"] = [NTVWoodyExtentGain, NTVWoodyExtentLoss]
ALL_OBSERVED_CHANGE_CLASSES.extend([NTVWoodyExtentGain, NTVWoodyExtentLoss])


class NTVHerbaceousExtentGain(ObservedChangeBase):
    """
    Increase in extent of natural terrestrial herbaceous vegetation
    (class is not equivalent to decrease in extent of natural terrestrial woody vegetation,
    see class NTVWoodyExtentLoss doc string)

    Conditions:
    level 3 == 112
    lifeform == woody --> herbaceous
    or
    level 3 changed to 112
    lifeform == herbaceous
    canopy cover = no data | no change | increase | decrease

    EBC classes for greening/browning when NTV lifeform changes from other to herbaceous are included here, as independent
    classes for each NTV greening/browning when lifeform changes are deemed unnecessary (where lifeform changes this is
    a significant landscape change and attempting to incorperate a measure of photosynthetic activity would not value
    add any useful information)
    """
    def __init__(self):
        self.output_code = 23
        self.change_direction = 1
        self.change_driver = "Human-induced, biotic or abiotic"
        self.observed_change = "Increase in natural herbaceous vegetation extent"
        self.observed_change_reason = "Restoration or revegetation"
        self.colours = (5, 213, 53, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Change type 1: Stays NTV, goes from woody to herbaceous
        # Level 3, no change to NTV (Need to allow for level3_change value of 0 so look at individual values)
        level3_change_mask1 = ((input_l4_change_xarray["level3_change"].values == 112112) |
                              ((input_l4_date1_xarray["level3"] == 112) & (input_l4_date2_xarray["level3"] == 112)))

        # Level 4, change from woody (1) --> herbaceous (2)
        lifeform_change_mask1 = (input_l4_change_xarray["lifeform_veg_cat_l4a_change"].values == 102)

        # Change type 2: Change from another L3 class to NTV herbaceous
        # Level 3, anything to NTV
        level3_change_mask2 =  ((input_l4_change_xarray["level3_change"].values == 111112) |
                                (input_l4_change_xarray["level3_change"].values == 123112) |
                                (input_l4_change_xarray["level3_change"].values == 124112) |
                                (input_l4_change_xarray["level3_change"].values == 215112) |
                                (input_l4_change_xarray["level3_change"].values == 216112) |
                                (input_l4_change_xarray["level3_change"].values == 220112))

        # change to herbaceous (2) in date2
        lifeform_change_mask2 = input_l4_date2_xarray["lifeform_veg_cat_l4a"] == 2

        output_change_numpy = numpy.where( ((level3_change_mask1 & lifeform_change_mask1) | (level3_change_mask2 & lifeform_change_mask2)),
                                        out_val, output_change_numpy)

        return output_change_numpy

class NTVHerbaceousExtentLoss(ObservedChangeBase):
    """
    Decrease in extent of natural terrestrial herbaceous vegetation
    (class is not equivalent to increase in extent of natural terrestrial woody vegetation,
    see class NTVWoodyExtentGain doc string)

    Conditions:
    level 3 == 112
    lifeform ==  herbaceous --> woody
    or
    level 3 == 112 & level 4 == herbaceous --> level 3 != 112
    canopy cover = no data | no change | increase | decrease

    EBC classes for greening/browning when NTV lifeform changes from herbaceous to other are included here, as independent
    classes for each NTV greening/browning when lifeform changes are deemed unnecessary (where lifeform changes this is
    a significant landscape change and attempting to incorperate a measure of photosynthetic activity would not value
    add any useful information)
    """
    def __init__(self):
        self.output_code = 24
        self.change_direction = 2
        self.change_driver = "Human-induced, biotic or abiotic"
        self.observed_change = "Decrease in natural herbaceous vegetation extent"
        self.observed_change_reason = "Dieback (drought, herbivory, insects, pathogens, sea level fluctuation or storms), \
                                    wildfires or natural phenological changes (e.g. perennials to annuals)"
        self.colours = (221, 139, 72, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Change type 1: Stays NTV, goes from herbaceous to woody
        # Level 3, no change to NTV (Need to allow for level3_change value of 0 so look at individual values)
        level3_change_mask1 = ((input_l4_change_xarray["level3_change"].values == 112112) |
                              ((input_l4_date1_xarray["level3"] == 112) & (input_l4_date2_xarray["level3"] == 112)))

        # Level 4, change from herbaceous (2) --> woody (1)
        lifeform_change_mask1 = (input_l4_change_xarray["lifeform_veg_cat_l4a_change"].values == 201)

        # Change type 2: Change from NTV herbaceous to a different L3 class
        # Level 3, NTV to anything
        level3_change_mask2 =  ((input_l4_change_xarray["level3_change"].values == 112111) |
                                (input_l4_change_xarray["level3_change"].values == 112123) |
                                (input_l4_change_xarray["level3_change"].values == 112124) |
                                (input_l4_change_xarray["level3_change"].values == 112215) |
                                (input_l4_change_xarray["level3_change"].values == 112216) |
                                (input_l4_change_xarray["level3_change"].values == 112220))

        # change from herbaceous (2) in date1
        lifeform_change_mask2 = input_l4_date1_xarray["lifeform_veg_cat_l4a"] == 2

        output_change_numpy = numpy.where( ((level3_change_mask1 & lifeform_change_mask1) | (level3_change_mask2 & lifeform_change_mask2)),
                                        out_val, output_change_numpy)

        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["ntv_herbaceous_extent"] = [NTVHerbaceousExtentGain, NTVHerbaceousExtentLoss]
ALL_OBSERVED_CHANGE_CLASSES.extend([NTVHerbaceousExtentGain, NTVHerbaceousExtentLoss])


class WPersistenceIncrease(ObservedChangeBase):
    """
    Increase in presence of water over time (i.e. increase in hydroperiod)

    Conditions:
    level 3 == 220
    water persistence = increase
    """
    def __init__(self):
        self.output_code = 25
        self.change_direction = 1
        self.change_driver = "Human induced or abiotic"
        self.observed_change = "Increase in annual hydroperiod"
        self.observed_change_reason = "Inundation (i.e. storm surge or relative sea level rise related flooding) \
                                        or anthropogenic flooding (i.e. dam construction)"
        self.colours = (11, 133, 255, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Required layers
        level3_change_mask = ((input_l4_change_xarray["level3_change"].values == 220220) |
                              ((input_l4_date1_xarray["level3"] == 220) & (input_l4_date2_xarray["level3"] == 220)))

        # date1 > date2 as waterper categories are 9 (1-3 months) to 1 (> 9 months)
        water_persistence_change_mask = (input_l4_date1_xarray["waterper_wat_cat_l4d"] > input_l4_date2_xarray["waterper_wat_cat_l4d"])

        # Need to allow for level3_change value of 0 so look at individual values
        output_change_numpy = numpy.where((level3_change_mask & water_persistence_change_mask),
                                        out_val, output_change_numpy)

        return output_change_numpy

class WPersistenceDecrease(ObservedChangeBase):
    """
    Decrease in presence of water over time (i.e. decrease in hydroperiod)

    Conditions:
    level 3 == 220
    water persistence = decrease
    """
    def __init__(self):
        self.output_code = 26
        self.change_direction = 2
        self.change_driver = "Human induced or abiotic"
        self.observed_change = "Decrease in annual hydroperiod"
        self.observed_change_reason = "Water abstraction, dam failure or removal, draining of waterbodies, \
                                        land reclamation, drying, evaporation or relative sea level fall"
        self.colours = (245, 185, 200, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Required layers
        level3_change_mask = ((input_l4_change_xarray["level3_change"].values == 220220) |
                              ((input_l4_date1_xarray["level3"] == 220) & (input_l4_date2_xarray["level3"] == 220)))

        # date1 < date2 as waterper categories are 9 (1-3 months) to 1 (> 9 months)
        water_persistence_change_mask = (input_l4_date1_xarray["waterper_wat_cat_l4d"] < input_l4_date2_xarray["waterper_wat_cat_l4d"])

        # Need to allow for level3_change value of 0 so look at individual values
        output_change_numpy = numpy.where((level3_change_mask & water_persistence_change_mask),
                                        out_val, output_change_numpy)

        return output_change_numpy

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["water_persistence"] = [WPersistenceIncrease, WPersistenceDecrease]
ALL_OBSERVED_CHANGE_CLASSES.extend([WPersistenceIncrease, WPersistenceDecrease])


class BSBrowning(ObservedChangeBase):
    """
    Decrease in photosynthetic or non photosynthetic activity of bare areas.
    As bare gradation uses BS endmember of FC, effectively this is an increase in the BS endmember.

    Conditions:
    level 3 == 216
    bare gradation = increase
    """
    def __init__(self):
        self.output_code = 27
        self.change_direction = 1
        self.change_driver = "Biotic or abiotic"
        self.observed_change = "Dieback (drought, herbivory, insects, pathogens), \
                                    wildfires or natural phenological changes"
        self.observed_change_reason = "Seasonal phenology, drought or dieback"
        # TODO: need appropriate colours for change
        self.colours = (255, 255, 255, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Required layers
        level3_change_mask = ((input_l4_change_xarray["level3_change"].values == 216216) |
                              ((input_l4_date1_xarray["level3"] == 216) & (input_l4_date2_xarray["level3"] == 216)))
       
        # date1 < date2 as baregrad categories are 10 (sparsely vegetated) to 15 (bare areas, unvegetated)
        bare_grad_change_mask = (input_l4_date1_xarray["baregrad_phy_cat_l4d_au"] < input_l4_date2_xarray["baregrad_phy_cat_l4d_au"])

        # Need to allow for level3_change value of 0 so look at individual values
        output_change_numpy = numpy.where((level3_change_mask & bare_grad_change_mask),
                                        out_val, output_change_numpy)
        return output_change_numpy

class BSGreening(ObservedChangeBase):
    """
    Increase in photosynthetic or non photosynthetic activity of bare areas.
    As bare gradation uses BS endmember of FC, effectively this is a decrease in the BS endmember.

    Conditions:
    level 3 == 216
    bare gradation = decrease
    """
    def __init__(self):
        self.output_code = 28
        self.change_direction = 2
        self.change_driver = "Biotic"
        self.observed_change = "Increased photosynthetic or non photosynthetic activity of bare areas"
        self.observed_change_reason = "Crop fertilisation, growth, irrigation or rainfall increase"
        # TODO: need appropriate colours for change
        self.colours = (255, 255, 255, 255)

    def get_change(self, input_l4_change_xarray, input_l4_date1_xarray, input_l4_date2_xarray,
                   output_change_numpy, change_direction=False):

        # See if we want a unique change code or the direction
        # for the output value
        if change_direction:
            out_val = self.change_direction
        else:
            out_val = self.output_code

        # Required layers
        level3_change_mask = ((input_l4_change_xarray["level3_change"].values == 216216) |
                              ((input_l4_date1_xarray["level3"] == 216) & (input_l4_date2_xarray["level3"] == 216)))

        # date1 > date2 as baregrad categories are 10 (sparsely vegetated) to 15 (bare areas, unvegetated)
        bare_grad_change_mask = (input_l4_date1_xarray["baregrad_phy_cat_l4d_au"] > input_l4_date2_xarray["baregrad_phy_cat_l4d_au"])

        # Need to allow for level3_change value of 0 so look at individual values
        output_change_numpy = numpy.where((level3_change_mask & bare_grad_change_mask),
                                        out_val, output_change_numpy)

        return output_change_numpy    

# Add classes to change pairs dictionary and list of all classes
ALL_OBSERVED_CHANGE_PAIRS["bs_grad"] = [BSBrowning, BSGreening]
ALL_OBSERVED_CHANGE_CLASSES.extend([BSBrowning, BSGreening])


def get_evidence_based_change_pairs(input_l4_change_xarray,
                                    input_l4_date1_xarray, input_l4_date2_xarray,
                                    change_class_pairs_list=ALL_OBSERVED_CHANGE_PAIRS):
    """
    Runs through evidence based change and creates an xarray with each change class as a separate variable
    containing values of 0 (no change), 1 (increase) or 2 (decrease).

    """
    out_change_pairs_xarray_list = []
    for change_name, change_classes in change_class_pairs_list.items():

        # Set up numpy array
        output_change_numpy = numpy.zeros(input_l4_change_xarray["level3_change"].values.shape,
                                       dtype=numpy.uint8)

        try:
            # Increase
            output_change_numpy = change_classes[0]().get_change(
                input_l4_change_xarray,
                input_l4_date1_xarray, input_l4_date2_xarray,
                output_change_numpy, change_direction=True)
            # Decrease
            output_change_numpy = change_classes[1]().get_change(
                input_l4_change_xarray,
                input_l4_date1_xarray, input_l4_date2_xarray,
                output_change_numpy, change_direction=True)

        except KeyError as err:
            logging.debug("Missing all inputs for calculating change: '{}'. {}".format(change_name, err))

        # Set up xarray with pairs
        output_change_pair_xarray = xarray.Dataset(
            {change_name : (input_l4_change_xarray["level3_change"].dims, output_change_numpy)},
            coords=input_l4_change_xarray.coords)
        out_change_pairs_xarray_list.append(output_change_pair_xarray)

    # Combine all into a single xarray
    output_change_pairs_xarray = xarray.merge(out_change_pairs_xarray_list)

    return output_change_pairs_xarray

def get_evidence_based_change(input_l4_change_xarray,
                              input_l4_date1_xarray, input_l4_date2_xarray,
                              change_classes_list=ALL_OBSERVED_CHANGE_CLASSES):
    """
    Runs through multiple evidence based change classes and returns out as a
    single xarray.

    """
    # Set up numpy array
    output_change_numpy = numpy.zeros(input_l4_change_xarray["level3_change"].values.shape,
                                   dtype=numpy.uint8)

    # Run through all change classes
    for change_class in change_classes_list:
        try:
            output_change_numpy = change_class().get_change(input_l4_change_xarray,
                                                      input_l4_date1_xarray, input_l4_date2_xarray,
                                                      output_change_numpy)
        except KeyError as err:
            logging.debug("Missing all inputs for calculating change: '{}'. Missing key {}"
                          "".format(change_class().observed_change, err))

    # Set up xarray to return
    output_change_xarray = xarray.Dataset(
        {"evidence_based_change" : (input_l4_change_xarray["level3_change"].dims, output_change_numpy)},
        coords=input_l4_change_xarray.coords)

    return output_change_xarray

def get_evidence_based_change_colours(change_xarray):
    """
    Takes evidence based change xarray and applies the colours for each class
    """

    try:
        change_numpy = change_xarray["evidence_based_change"].values
    except KeyError:
        raise KeyError("Couldn't get key 'evidence_based_change' from input"
                       " xarray. Has change detection been run first?")

    # Set up numpy arrays for output
    red = numpy.zeros_like(change_numpy, dtype=numpy.uint8)
    green = numpy.zeros_like(red)
    blue = numpy.zeros_like(red)
    alpha = numpy.zeros_like(red)


    # Go through all classes and apply colours
    for change_class in ALL_OBSERVED_CHANGE_CLASSES:
        red, green, blue, alpha = change_class().get_change_colours(change_numpy,
                                                                 red, green,
                                                                 blue, alpha)
    return red, green, blue, alpha
