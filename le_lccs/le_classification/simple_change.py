"""
Simple change detection functions

These are functions for providing change in the form class x -> class y

See the observed changed module for more advanced changed detection

"""

import logging

import numpy
import xarray

from . import l4_layers_lccs
from . import lccs_l4

def get_change_code_layer(input_classification_xarray1, input_classification_xarray2, l4_layer_class,
                          ignore_no_change=False):
    """
    Get change code by concatenating code from classification 1 with code from classification 2.

    So: 11 -> 12 = 1112

    Implemented by multiplying to first code by a scaling factor  then adding the second code to make
    more computationally efficient.

    """
    # Get layer name from class
    layer_name = l4_layer_class().output_layer_name

    if input_classification_xarray1[layer_name].shape != \
        input_classification_xarray2[layer_name].shape:
        raise Exception("Classification arrays aren't the same size")


    # Set up name for output change
    change_name = "{}_change".format(layer_name)

    # Get largest class code used
    max_class_val = numpy.max(list(l4_layer_class().output_codes_descriptions.keys()))

    # Get multiplication factor. If the maximum class value is two digits use a factor of 100.
    # If larger then need to use 1000. Assume don't have anything bigger than three digits.
    # Use floating point multiplication else hit limit of 8bit integer for input and numpy doesn't
    # deal with this well. Working with floats fixes this.
    multiplication_factor = 100.0
    out_dtype = numpy.uint16

    if max_class_val > 99:
        multiplication_factor = 1000.0
        out_dtype = numpy.uint32

    out_change_vals = (input_classification_xarray1[layer_name] * multiplication_factor) + \
                       input_classification_xarray2[layer_name]

    # Mask out values with no change by setting to 0 if this is requested.
    if ignore_no_change:
        out_change_vals = numpy.where(input_classification_xarray1[layer_name] == input_classification_xarray2[layer_name],
                                      0, out_change_vals)

    # Set up xarray to return
    change_code_xarray = xarray.Dataset(
        {change_name : (input_classification_xarray1[layer_name].dims, out_change_vals.astype(out_dtype))},
        coords=input_classification_xarray1.coords)

    return change_code_xarray


def get_change_description_layer(input_classification_xarray1, input_classification_xarray2, l4_layer_class,
                                 ignore_no_change=False):
    """
    Get change description by calculating codes and descriptions and then concatenating together.

    Tries to do in a sensible way so get outputs such as:

    "A11: Cultivated Terrestrial Vegetated to B15: Artificial Surface" or
    "Remained as A11: Cultivated Terrestrial Vegetated"

    """
    # Get layer name from class
    layer_name = l4_layer_class().output_layer_name

    if input_classification_xarray1[layer_name].shape != input_classification_xarray2[layer_name].shape:
        raise Exception("Classification arrays aren't the same size")

    change_name = "{}_change_description".format(layer_name)

    # Get output codes (e.g., A11)
    codes1 = l4_layer_class().get_output_code(input_classification_xarray1)
    codes2 = l4_layer_class().get_output_code(input_classification_xarray2)

    # Get output descriptions (e.g., Cultivated Terrestrial Vegetated)
    descriptions1 = l4_layer_class().get_output_description(input_classification_xarray1)
    descriptions2 = l4_layer_class().get_output_description(input_classification_xarray2)

    # Join together (e.g., A11: Cultivated Terrestrial Vegetated)
    # Only do this where there is a valid class
    code_description1 = numpy.where(input_classification_xarray1[layer_name] != 0,
                                    numpy.core.defchararray.add(codes1, ": "), "")
    code_description1 = numpy.where(input_classification_xarray1[layer_name] != 0,
                                    numpy.core.defchararray.add(code_description1, descriptions1), "")

    code_description2 = numpy.where(input_classification_xarray2[layer_name] != 0,
                                    numpy.core.defchararray.add(codes2, ": "), "")
    code_description2 = numpy.where(input_classification_xarray2[layer_name] != 0,
                                    numpy.core.defchararray.add(code_description2, descriptions2), "")

    # Set up string for where the classes have changed output format will be:
    # "A11: Cultivated Terrestrial Vegetated to B15: Artificial Surface"
    change_description = numpy.where(
        input_classification_xarray1[layer_name] != input_classification_xarray2[layer_name],
        numpy.core.defchararray.add(code_description1, " to "), "")
    change_description = numpy.where(
        input_classification_xarray1[layer_name] != input_classification_xarray2[layer_name],
        numpy.core.defchararray.add(change_description, code_description2), "")

    # Set up string for where classes haven't changed.
    # If ignore_no_change=True output is an empty string, else is "Remained as ..."
    if ignore_no_change:
        change_description = numpy.where(
            input_classification_xarray1[layer_name] == input_classification_xarray2[layer_name],
            "", change_description)
    else:
        change_description = numpy.where(
            (input_classification_xarray1[layer_name] == input_classification_xarray2[layer_name]) & \
            (input_classification_xarray1[layer_name] != 0),
            numpy.core.defchararray.add("Remained as ", code_description1), change_description)

    # Set up xarray to return
    change_description_xarray = xarray.Dataset(
        {change_name : (input_classification_xarray1[layer_name].dims, change_description)},
        coords=input_classification_xarray1.coords)

    return change_description_xarray


def get_change_codes(input_classification_xarray1, input_classification_xarray2,
                     l4_layer_classes_list, ignore_no_change=False):
    """
    Calculate change for a list of input layers classes. Output is a numeric code for each of the input layers.

    TODO: Need to decide if want to use multiple input xarrays or a single one with time dimension and specify time
    periods to use. For internal use makes it easier to use two but can split into separate xarrays within the
    function.

    :param xarray.Dataset input_classification_xarray1: Array containing classified data from initial period.
    :param xarray.Dataset input_classification_xarray2: Array containing classified data from second period.
    :param list l4_layer_classes_list: List containing level 4 layer classes
    :param bool ignore_no_change: Set to true to mask out classes with no change
    :returns: xarray with change codes for each dataset.
    :rtype: xarray.Dataset

    """
    output_xarray_list = []
    for l4_layer_class in l4_layer_classes_list:
        try:
            out_xarray = get_change_code_layer(input_classification_xarray1,
                                               input_classification_xarray2,
                                               l4_layer_class,
                                               ignore_no_change=ignore_no_change)
            output_xarray_list.append(out_xarray)
        except KeyError:
            logging.debug("Missing input for layer '{}'"
                          "".format(l4_layer_class().output_layer_name))

    out_change = xarray.merge(output_xarray_list)

    return out_change

def get_change_descriptions(input_classification_xarray1, input_classification_xarray2,
                            l4_layer_classes_list, ignore_no_change=False):
    """
    Calculate change for a list of input layers classes. Output is a string description for each of the input layers.

    TODO: Need to decide if want to use multiple input xarrays or a single one with time dimension and specify time
    periods to use. For internal use makes it easier to use two but can split into separate xarrays within the
    function.

    :param xarray.Dataset input_classification_xarray1: Array containing classified data from initial period.
    :param xarray.Dataset input_classification_xarray2: Array containing classified data from second period.
    :param list l4_layer_classes_list: List containing level 4 layer classes
    :param bool ignore_no_change: Set to true to mask out classes with no change
    :returns: xarray with change codes for each dataset.
    :rtype: xarray.Dataset

    """
    output_xarray_list = []
    for l4_layer_class in l4_layer_classes_list:
        try:
            out_xarray = get_change_description_layer(input_classification_xarray1,
                                                      input_classification_xarray2,
                                                      l4_layer_class,
                                                      ignore_no_change=ignore_no_change)
            output_xarray_list.append(out_xarray)
        except KeyError:
            logging.debug("Missing input for layer '{}'"
                          "".format(l4_layer_class().output_layer_name))

    out_change = xarray.merge(output_xarray_list)

    return out_change


def get_lccs_l3_change_codes(input_classification_xarray1, input_classification_xarray2,
                             ignore_no_change=False):
    """
    Helper function to get change codes for L3
    """
    return get_change_codes(input_classification_xarray1,
                            input_classification_xarray2,
                            [l4_layers_lccs.LCCSLevel3],
                            ignore_no_change=ignore_no_change)


def get_lccs_l3_change_descriptions(input_classification_xarray1, input_classification_xarray2,
                                    ignore_no_change=False):
    """
    Helper function to get change descriptions for L3
    """
    return get_change_descriptions(input_classification_xarray1,
                                   input_classification_xarray2,
                                   [l4_layers_lccs.LCCSLevel3],
                                   ignore_no_change=ignore_no_change)


def get_lccs_l4_change_codes(input_classification_xarray1, input_classification_xarray2,
                             classification_scheme="lccs", ignore_no_change=False):
    """
    Helper function to get change codes for LCCS L4
    """
    level4_classes_list = lccs_l4.LAYER_CLASSES[classification_scheme]

    return get_change_codes(input_classification_xarray1,
                            input_classification_xarray2,
                            level4_classes_list,
                            ignore_no_change=ignore_no_change)


def get_lccs_l4_change_descriptions(input_classification_xarray1, input_classification_xarray2,
                                    classification_scheme="lccs", ignore_no_change=False):
    """
    Helper function to get change description for LCCS L4
    """

    level4_classes_list = lccs_l4.LAYER_CLASSES[classification_scheme]

    return get_change_descriptions(input_classification_xarray1,
                                   input_classification_xarray2,
                                   level4_classes_list,
                                   ignore_no_change=ignore_no_change)

