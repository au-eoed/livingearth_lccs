"""
Level 4 Classification Layers for modified scheme specific for Living Wales LCCS implementation

Currently used layers for Living Wales LCCS implementation (2020)
* indicates modification from LCCS layers base
LifeformVegCatL4a (only using Woody and Herbaceous categories)
CanopycoVegCatL4d* (modified categories for a) improving accuracy and b) simplifying map display)
LeaftypeVegCatL4a* (modified to restrict to 2 types (i.e., broad- and needle-leaved) to woody areas)
PhenologVegCatL4a* (modified to restrict to 2 types (i.e., deciduous and evergreen) to woody areas)
WaterperWatCatL4d* (modified to merge water seasonality and persistence into one unique layer)

Docstrings for each class below detail modifications from l4_layers_lccs.py
If no explanation given, assume identical to relevant class in l4_layers_lccs.py
"""

import logging

import numpy
import xarray

from . import l4_base

# Load standard l4 layers
from . import l4_layers_lccs

class CanopycoVegCatL4d_LW(l4_base.Level4ClassificationContinuousLayer):
    """
    Modified version of canopy cover for Living Wales implementation. 
    Used 20% step instead of original FAO classes

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "canopyco_veg_con"
        self.output_layer_name = "canopyco_veg_cat_l4d_lw"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {10 : ("A10", "Cover (> 80 %)"),
                                          12 : ("A20", "Cover (60 to 80 %)"),
                                          13 : ("A30", "Cover (40 to 60 %)"),
                                          15 : ("A40", "Cover (20 to 40 %)"),
                                          16 : ("A50", "Cover (1 to 20 %)")}
        self.class_boundaries = {10 : (80, None),
                                 12 : (60, 80),
                                 13 : (40, 60),
                                 15 : (20, 40),
                                 16 : (1, 20),
                                 0  : (0, 1)}
        self.valid_level3_classes = [111, 112, 123, 124]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.DERIVATIVE
        
        
class LeaftypeVegCatL4a_LW(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Leaf type (leaftype_veg_cat_l4a_lw)
    
    Simplified version of the original version. 
    Modified to restrict to 2 types (i.e., broad- and needle-leaved) to woody areas

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "leaftype_veg_cat"
        self.output_layer_name = "leaftype_veg_cat_l4a_lw"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = {1 : ("D1", "Broad-leaved"),
                                          2 : ("D2", "Needle-leaved")} # all excluded categories
        self.valid_level3_classes = [111, 112, 124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((leaftype_veg_cat_l4a_lw == 1) | (leaftype_veg_cat_l4a_lw == 2)) & \
             ((level3 == 111) | (level3 == 112) | (level3 == 124)) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN


class PhenologVegCatL4a_LW(l4_base.Level4ClassificationCatergoricalLayer):
    """
    Phenology (phenolog_veg_cat_l4a_lw)
    
    Simplified version of the original version.
    Modified to restrict to 2 types (i.e., deciduous and evergreen) to woody areas
    
    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "phenolog_veg_cat"
        self.output_layer_name = "phenolog_veg_cat_l4a_lw"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)

        self.output_codes_descriptions = {1 : ("E1", "Evergreen"),
                                          2 : ("E2", "Deciduous")} # all excluded categories
        self.valid_level3_classes = [111, 112, 124]
        # Set valid level 4 classes.
        self.valid_level4_layers_filters = [
            "((phenolog_veg_cat_l4a_lw == 1) | (phenolog_veg_cat_l4a_lw == 2)) & \
             ((level3 == 111) | (level3 == 112) | (level3 == 124)) & \
             ((lifeform_veg_cat_l4a == 1) | (lifeform_veg_cat_l4a == 3) | (lifeform_veg_cat_l4a == 4))"]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.MAIN



class WaterperWatCatL4d_LW(l4_base.Level4ClassificationContinuousLayer):
    """
    Modified version of water persistence for Living Wales implementation.

    Removed dependency for inttidal_wat_cat_l4a as not routinely available
    for LivingWales implementation at present.
    
    Added more non-perennial classes (i.e., month step)
    
    Merged water seasonality and persistence into one unique layer (i.e., "waterper_wat_cat_l4d_lw")
    
    Allowed waterper_wat_cat_l4d_lw for all (semi-)natural landcovers (not just aquatic veg)

    Tier 2 class (112, 124, 216, 220, 227, 228)

    Permitted additional attributes
    all
    """
    def __init__(self):
        self.input_layer_name = "waterper_wat_cin"
        self.output_layer_name = "waterper_wat_cat_l4d_lw"
        self.input_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.input_layer_name)
        self.output_layer_uncertainty_name = self.get_uncertainty_layer_name(
            self.output_layer_name)
        self.output_codes_descriptions = { 1 : ("W1", "Perennial (>= 9 months)"),
                                           2 : ("W2", "Non-perennial (8 months)"),
                                           3 : ("W3", "Non-perennial (7 months)"),
                                           4 : ("W4", "Non-perennial (6 months)"),
                                           5 : ("W5", "Non-perennial (5 months)"),
                                           6 : ("W6", "Non-perennial (4 months)"),
                                           7 : ("W7", "Non-perennial (3 months)"),
                                           8 : ("W8", "Non-perennial (2 months)"),
                                           9 : ("W9", "Non-perennial (1 months)")}
        # In days. Use months multiplied by 30 to simplify
        self.class_boundaries = {1 : (9, None),
                                 2 : (8, 9),
                                 3 : (7, 8),
                                 4 : (6, 7),
                                 5 : (5, 6),
                                 6 : (4, 5),
                                 7 : (3, 4),
                                 8 : (2, 3),
                                 9 : (1, 2),
                                 0 : (0, 1)}
        self.valid_level3_classes = [112, 124, 216, 220, 227, 228]
        # Set type for class
        self.layer_type = l4_base.ClassificationLayerTypes.DERIVATIVE

#: List of standard and modified layers used for LW classification
ALL_L4_LAYER_CLASSES_LW_MODIFIED = [l4_layers_lccs.LCCSLevel3,
                                    l4_layers_lccs.LifeformVegCatL4a,
                                    l4_layers_lccs.MlifefrmVegCatL4m,
                                    CanopycoVegCatL4d_LW,
                                    l4_layers_lccs.CanopyhtVegCatL4d,
                                    l4_layers_lccs.WaterdayAgrCatL4a,
                                    l4_layers_lccs.WaterseaVegCatL4a,
                                    LeaftypeVegCatL4a_LW,
                                    PhenologVegCatL4a_LW,
                                    l4_layers_lccs.MphenlogVegCatL4m,
                                    l4_layers_lccs.UrbanvegUrbCatL4a,
                                    l4_layers_lccs.SpatdistVegCatL4a,
                                    l4_layers_lccs.Strat2ndVegCatL4a,
                                    l4_layers_lccs.Lifef2ndVegCatL4a,
                                    l4_layers_lccs.Cover2ndVegCatL4d,
                                    l4_layers_lccs.Heigh2ndVegCatL4d,
                                    l4_layers_lccs.SpatsizeAgrCatL4a,
                                    l4_layers_lccs.CropcombAgrCatL4a,
                                    l4_layers_lccs.Croplfc2AgrCatL4a,
                                    l4_layers_lccs.Croplfc3AgrCatL4a,
                                    l4_layers_lccs.CropseqtAgrCatL4a,
                                    l4_layers_lccs.CropseqaAgrCatL4a,
                                    l4_layers_lccs.WatersupAgrCatL4a,
                                    l4_layers_lccs.TimefactAgrCatL4a,
                                    l4_layers_lccs.WatersttWatCatL4a,
                                    l4_layers_lccs.InttidalWatCatL4a,
                                    WaterperWatCatL4d_LW,
                                    l4_layers_lccs.SnowcperWatCatL4d,
                                    l4_layers_lccs.WaterdptWatCatL4a,
                                    l4_layers_lccs.MwatrmvtWatCatL4m,
                                    l4_layers_lccs.WsedloadWatCatL4a,
                                    l4_layers_lccs.MsubstrtWatCatL4m,
                                    l4_layers_lccs.BaresurfPhyCatL4a,
                                    l4_layers_lccs.MbarematPhyCatL4m,
                                    l4_layers_lccs.MustonesPhyCatL4m,
                                    l4_layers_lccs.MhardpanPhyCatL4m,
                                    l4_layers_lccs.MacropatPhyCatL4a,
                                    l4_layers_lccs.ArtisurfUrbCatL4a,
                                    l4_layers_lccs.MtclineaUrbCatL4m,
                                    l4_layers_lccs.MnolineaUrbCatL4m,
                                    l4_layers_lccs.MartdensUrbCatL4m,
                                    l4_layers_lccs.MnobuiltUrbCatL4m]
