"""
LCCS Level 3 Classification

Functions to apply the LCCS Level 3 classification to an xarray of data.
The output is a numeric code which relates to the following description and LCCS code.

+----------------------------------+------+--------------+
| Class name                       | Code | Numeric code |
+==================================+======+==============+
| Cultivated Terrestrial Vegetated | A11  | 111          |
+----------------------------------+------+--------------+
| Natural Terrestrial Vegetated    | A12  | 112          |
+----------------------------------+------+--------------+
| Cultivated Aquatic Vegetated     | A23  | 123          |
+----------------------------------+------+--------------+
| Natural Aquatic Vegetated        | A24  | 124          |
+----------------------------------+------+--------------+
| Artificial Surface               | B15  | 215          |
+----------------------------------+------+--------------+
| Natural Surface                  | B16  | 216          |
+----------------------------------+------+--------------+
| Artificial Water                 | B27  | 227          |
+----------------------------------+------+--------------+
| Natural Water                    | B28  | 228          |
+----------------------------------+------+--------------+

"""

import logging
import numpy

#: Required input variables
LCCS_L3_REQUIRED_VARIABLES = ["vegetat_veg_cat",
                              "aquatic_wat_cat",
                              "cultman_agr_cat",
                              "artific_urb_cat"]

#: LCCS Level 3 Colour Scheme
LCCS_L3_COLOUR_SCHEME = {111 : (172, 188, 45, 255),
                         112 : (14, 121, 18, 255),
                         123 : (86, 236, 231, 255),
                         124 : (30, 191, 121, 255),
                         215 : (218, 92, 105, 255),
                         216 : (243, 171, 105, 255),
                         220 : (77, 159, 220, 255),
                         227 : (53, 175, 255, 255),
                         228 : (1, 86, 207, 255)}

#: Classes used standard lccs and modified versions.
EXPECTED_L3_CLASSES = {"lccs" : [111, 112, 123, 124, 215, 216, 227, 228],
                       "au"   : [111, 112, 124, 215, 216, 220],
                       "lw"   : [111, 112, 123, 124, 215, 216, 220]}


def colour_lccs_level3(classification_array):
    """"
    Colour classification array using LCCS Level 3 standard
    colour scheme. Returns four arays:

    * red
    * green
    * blue
    * alpha

    :param numpy.array classification_array: numpy array containing level3 classification.

    """
    red = numpy.zeros_like(classification_array, dtype=numpy.uint8)
    green = numpy.zeros_like(red)
    blue = numpy.zeros_like(red)
    alpha = numpy.zeros_like(red)

    for class_id, colours in LCCS_L3_COLOUR_SCHEME.items():
        subset = (classification_array == class_id)
        red[subset], green[subset], blue[subset], alpha[subset] = colours

    return red, green, blue, alpha

def _check_required_variables(classification_data):
    """
    Check requited variables are in xarray
    """
    # Check all input variable exist - warning if they don't
    for var in LCCS_L3_REQUIRED_VARIABLES:
        if var not in classification_data.data_vars:
            logging.warning("Required variable {0} not found".format(var))

def classify_lccs_level3(classification_data, level3_classes_list=EXPECTED_L3_CLASSES["lccs"]):
    """
    Apply Level 3 LCCS Classification

    :param xarray.Dataset classification_data: Input xarray dataset for classification containing the following variables

        * vegetat_veg_cat - Binary mask 1=vegetation, 0=non-vegetation
        * aquatic_wat_cat - Binary mask 1=aquatic, 0=non-aquatic
        * cultman_agr_cat - Binary mask 1=cultivated/managed, 0=natural
        * artific_urb_cat - Binary mask 1=urban, 0=non-urban
        * artwatr_wat_cat - Binary mask 1=artificial water, 0=natural water

    Returns three arrays:

    * level1
    * level2
    * level3

    """

    # Check required input and output variables exist.
    _check_required_variables(classification_data)

    # Set up arrays for outputs
    try:
        output_dims = classification_data["vegetat_veg_cat"].values.shape
    except KeyError:
        raise Exception("No data available for first level of classification "
                        "(vegetation / non-vegetation), can not proceed")

    level1 = numpy.zeros(output_dims, dtype=numpy.uint8)
    level2 = numpy.zeros(output_dims, dtype=numpy.uint8)
    level3 = numpy.zeros(output_dims, dtype=numpy.uint8)

    # Level 1
    # Assign level 1 class of primarily vegetated (A,100) or primarily non-vegetated (B,200)
    level1 = numpy.where(classification_data["vegetat_veg_cat"].values == 1,
                         100, level1)
    level1 = numpy.where(classification_data["vegetat_veg_cat"].values == 0,
                         200, level1)

    # Level 2
    # Assign level 2 class of terrestrial (10) or aquatic (20)
    try:
        level2 = numpy.where(classification_data["aquatic_wat_cat"].values == 1,
                             20, level2)
        level2 = numpy.where(classification_data["aquatic_wat_cat"].values == 0,
                             10, level2)
    except KeyError:
        raise Exception("No data available for second level of classification "
                        "(aquatic / non-aquatic), can not proceed")

    # Level 3

    # Assign level 3 (Supercategory) class based on cultivated or artificial
    try:
        cultivated = classification_data["cultman_agr_cat"].values

        # Cultivated Terrestrial Vegetation (A11)
        level3[(level1 == 100) & (level2 == 10) & (cultivated == 1)] = 111

        # Natural Terrestrial Vegetation (A12)
        level3[(level1 == 100) & (level2 == 10) & (cultivated == 0)] = 112

        # If expecting a cultivated aquatic class then use cultivated
        # layer to split into cultivated and aquatic
        if 123 in level3_classes_list:
            # Natural Aquatic Vegetation (A24)
            level3[(level1 == 100) & (level2 == 20) & (cultivated == 0)] = 124

            # Cultivated Aquatic Vegetation (A23)
            level3[(level1 == 100) & (level2 == 20) & (cultivated == 1)] = 123
        else:
            # Natural Aquatic Vegetation (A24)
            level3[(level1 == 100) & (level2 == 20)] = 124

    except KeyError:
        logging.warning("No cultivated vegetation layer available. Skipping "
                        "assigning level 3 catergories for vegetation")

    try:
        urban = classification_data["artific_urb_cat"].values

        # Artificial Surface (B15)
        level3[(level1 == 200) & (level2 == 10) & (urban == 1)] = 215

        # Natural Surface (B16)
        level3[(level1 == 200) & (level2 == 10) & (urban == 0)] = 216

    except KeyError:
        logging.warning("No urban layer available. Skipping assigning "
                        "level 3 for terrestrial non-vegetation")

    try:
        artificial_water = classification_data["artwatr_wat_cat"].values

        # Artificial Water (B27)
        level3[(level1 == 200) & (level2 == 20) & (artificial_water == 1)] = 227

        # Natural Water (B28)
        level3[(level1 == 200) & (level2 == 20) & (artificial_water == 0)] = 228

        # No data
        level3[(level1 == 200) & (level2 == 20) & (artificial_water != 0) & \
               (artificial_water != 1)] = 220

    except KeyError:
        logging.info("No artificial water layer available. Using level 2 class for "
                     "level 3 aquatic non-vegetation (water)")
        # Pull through level 2 classification
        level3[(level1 == 200) & (level2 == 20)] = 220

    return level1, level2, level3

