"""
Classes to read data into LCCS classification system from a table
"""

import logging
import numpy

from abc import ABCMeta

import xarray

from . import ingest_base


class LEIngestTable(ingest_base.LEIngest):
    """
    Abstract class for importing data from a table into the LE LCCS system.
    """
    __metaclass__ = ABCMeta

    def __init__(self, object_ids):
        """
        Initialise tabular ingestion class.

        :param object_ids: input numpy array of row ids which will be common between all data.
        """
        self.object_ids = object_ids


class LEIngestConstTable(LEIngestTable):
    """
    Create a data column with a constant value.
    """

    def read_to_xarray(self, variable_name=None, constval=0, **kwargs):
        """
        Function to create a variable with a constant value as an xarray.
        The number of rows is equal to self.object_ids

        Can be used when an input variable required for a classification isn't
        available but the value is known from other sources. For example if
        there is no artificial water for the area the classification is being
        run on can use to set all values to 0.

        :param str variable_name: name to use for variable within output xarray
        :param float constval: a constant value to assign to all elements in the xarray
        :return: xarray Dataset containing variable with coordinates self.object_ids
        :rtype: xarray.Dataset
        """
        # Check the output variable name
        if not self.check_variable_name(variable_name):
            raise Exception("Input variable name is inappropriate, see log.")

        # Create array of constant values
        # Set up empty array, using minimum dtype which can hold
        # constant value.
        rslt_vals = numpy.empty(self.object_ids.shape,
                                dtype=numpy.min_scalar_type(constval))
        rslt_vals[...] = constval

        # Convert to xarray.
        variable_xarray = xarray.Dataset({variable_name: (["object"], rslt_vals)},
                                         coords={"object": self.object_ids})

        return variable_xarray


class LEIngestRAT(LEIngestTable):
    """
    Ingest data stored as a Raster Attribute Table (RAT) into the LE LCCS
    system.
    """

    def read_to_xarray(self, input_file, variable_name=None, column_name=None, **kwargs):
        """
        Function to load columns from a Raster Attribute Table (RAT)
        into an xarray Dataset.

        Requires RAT to already contain attributes.

        Uses RIOS to read data.

        :param str input_file: input GDAL file (most likely KEA format) with existing RAT.
        :param str variable_name: name to use for variable within output xarray,.
                                  Setting to None will take all variables in
                                  RAT and use these names.
                                  Variable name must not contain non-alphanumeric
                                  characters.
        :param str column_name: name of column in RAT to get variable from.
                                Not used if variable_name is None.
        :param kwargs: any other input parameters.
        :return: xarray Dataset contaning variable(s) with coordinates self.object_ids.
        :rtype: xarray.Dataset
        """
        
        from osgeo import gdal
        from rios import rat

        # Open RAT
        rat_ds = gdal.Open(input_file, gdal.GA_ReadOnly)
        if rat_ds is None:
            raise IOError("Could not open: '{}'".format(input_file))

        # Get number of rows
        num_rows = rat.readColumn(rat_ds, rat.getColumnNames(rat_ds)[0]).size

        if num_rows != self.object_ids.size:
            raise Exception("RAT for '{0}' has {1} rows, was expecting {2}"
                            "".format(input_file, num_rows, self.object_ids.size))

        # If variable name is not provided assume all columns in RAT are
        # required.
        if variable_name is None:
            logging.info("No variable name supplied, importing all")
            variable_names_list = rat.getColumnNames(rat_ds)
            column_names_list = variable_names_list
        else:
            if column_name is None:
                column_name = variable_name
            variable_names_list = [variable_name]
            column_names_list = [column_name]

        # Extract each required column to an xarray
        variables_xarray_list = []
        for variable_name, column_name in zip(variable_names_list, column_names_list):
            if not self.check_variable_name(variable_name):
                raise Exception("Variable '{}' is inappropriate, see log.".format(variable_name))
            rat_col_data = rat.readColumn(rat_ds, column_name)
            column = xarray.Dataset({variable_name: (["object"], numpy.array(rat_col_data))},
                                    coords={"object": self.object_ids})
            variables_xarray_list.append(column)

        # Close the GDAL dataset.
        rat_ds = None

        # Merge together
        variables_xarray = xarray.merge(variables_xarray_list)

        return variables_xarray


class LEIngestEvalRAT(LEIngestTable):
    """
    Ingest data stored as a Raster Attribute Table (RAT) into the LE LCCS
    system.
    """

    def read_to_xarray(self, input_file, variable_name=None, column_names=None, expstr=None, **kwargs):
        """
        Function to load columns from a Raster Attribute Table (RAT), evaluate
        an expression using them and save the result to an xarray Dataset.

        Requires RAT to already contain attributes.

        Uses RIOS to read data.

        Examples of expressions are:

        ccilc15 == 190

        Which will create a binary mask contaning True where ccilc15 is 190

        It is also possible to apply mathematical expressions to variables, for
        example:

        arcsi_reflectance_b2 / 10000


        :param str input_file: input file with existing RAT.
        :param str variable_name: name to use for variable within output xarray,.
                                  Variable name must not contain non-alphanumeric
                                  characters.
        :param list column_names: list with column names in RAT.
        :param str expstr: expression to evaluate using column names.
        :param kwargs: any other input parameters.
        :return: xarray Dataset contaning variable(s) with coordinates self.object_ids.
        :rtype: xarray.Dataset
        """

        from osgeo import gdal
        from rios import rat

        # Check that the output variable name and input column names have been supplied.
        if not self.check_variable_name(variable_name):
            raise Exception("Input variable name is inappropriate, see log.")
        elif column_names is None:
            raise Exception("No input column names have been supplied")

        # Open RAT
        rat_ds = gdal.Open(input_file, gdal.GA_ReadOnly)
        if rat_ds is None:
            raise IOError("Could not open: '{}'".format(input_file))

        # Get number of rows
        num_rows = rat.readColumn(rat_ds, rat.getColumnNames(rat_ds)[0]).size

        # Check the size of the input variable and the number of object_ids
        # supplied through the constructor.
        if num_rows != self.object_ids.size:
            raise Exception("RAT for '{0}' has {1} rows, was expecting {2}"
                            "".format(input_file, num_rows, self.object_ids.size))

        # Read columns into local variables for the expression to be applied.
        lclvars = dict()
        for column_name in column_names:
            lclvars[column_name] = rat.readColumn(rat_ds, column_name)

        # Evaluate the expression.
        rslt_vals = eval(expstr, {}, lclvars)

        # Convert to xarray.
        variable_xarray = xarray.Dataset({variable_name: (["object"], numpy.array(rslt_vals))},
                                         coords={"object": self.object_ids})

        return variable_xarray
