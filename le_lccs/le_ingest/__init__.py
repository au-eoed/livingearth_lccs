"""
LivingEarth LCCS Ingest Tools

Functions to ingest data into system using inbuilt functions and
plugins.

Plugins
---------

The module must start with 'le_ingest_' and be available
on the PYTHONPATH or LE_LCCS_PLUGINS_PATH.

Functions can take any arguments but must include at least `**kwargs`.

They should all return an xarray.Dataset.

"""
import importlib
import os
import pkgutil
import sys

# Load inbuilt packages
from . import ingest_base
from . import gridded_ingest
from . import table_ingest


# Load plugins
for finder, name, ispkg in pkgutil.iter_modules():
    if name.startswith("le_ingest_"):
        locals()[name] = importlib.import_module(name)
