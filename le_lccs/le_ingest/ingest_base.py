"""
Utilities to ingest data
"""

# import abstract base class stuff
from abc import ABCMeta, abstractmethod
import logging
import re
# import numpy so it can be used in xarray maths.
import numpy

class LEIngest(object):
    __metaclass__ = ABCMeta

    def check_variable_name(self, variable_name):
        """
        A function to check a variable name does not contain any in appropriate characters.

        :param str variable_name: string with the variable name.
        :return: true if variable name is valid, false if it is not.
        :rtype: bool

        """
        variable_name_ok = True
        # Check if the input variable is None.
        if variable_name is None:
            variable_name_ok = False
            logging.error("The variable is None")
        # Matches any non-alphanumeric character; this is equivalent to the class [^a-zA-Z0-9_].
        elif re.compile('[\W]').search(variable_name) is not None:
            variable_name_ok = False
            logging.error("The variable ({}) contains characters which aren't allowed. "
                          "Only alphanumeric characters and underscores are allowed.".format(variable_name))
        # Check the variable name is not just a number
        elif variable_name.isdigit():
            variable_name_ok = False
            logging.error("The variable ({}) is a number and must contain an "
                          "some letters or underscore.".format(variable_name))
        # return True if OK and False is not ok.
        return variable_name_ok

    def xarray_maths(self, input_array, variable_name, in_variable_names, expstr):
        """
        A function to take an xarray.Dataset containing multiple variables
        apply an expression and return a single xarray

        :param xarray.Dataset: input array
        :param str variable_name: name to use for variable within output xarray,.
                                  Variable name must not contain non-alphanumeric
                                  characters.
        :param list in_variable_names: list of input variable names used in
                                       expression.
        :param str expstr: expression to evaluate.
        :return: result of expression
        :rtype: xarray.Dataset

        """

        if not self.check_variable_name(variable_name):
            raise Exception("Input variable name is inappropriate, see log.")

        expr_vars = {}

        for in_variable in in_variable_names:
            expr_vars[in_variable] = input_array[in_variable]

        # Evaluate expression
        # Pass in numpy so can use expressions.
        expr_out = eval(expstr, {"numpy" : numpy}, expr_vars)

        # Return as a xarray.Dataset
        return expr_out.to_dataset(name=variable_name)


    @abstractmethod
    def read_to_xarray(self, **kwargs):
        """
        Abstract function to read data to an xarray.
        :param kwargs: input parameters.
        """
        raise NotImplemented("The class you have used does not provide an implementation for"
                             " read_to_xarray. Are you using a base class by mistake?"
                             " If this is a plugin the plugin author needs to provide an"
                             " implementation for the class.")
