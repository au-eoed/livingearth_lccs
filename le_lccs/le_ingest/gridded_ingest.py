"""
Classes to read gridded data into LCCS classification system.
"""

from abc import ABCMeta
import logging

import numpy
import rasterio
import rasterio.features
import affine
import xarray

from . import ingest_base


class LEIngestGridded(ingest_base.LEIngest):
    """
    Abstract class for ingesting gridded data into LE LCCS system.
    """

    __metaclass__ = ABCMeta

    def __init__(
        self,
        target_min_x,
        target_max_x,
        target_min_y,
        target_max_y,
        target_pixel_size_x,
        target_pixel_size_y,
        target_crs,
        output_time=None,
    ):
        """
        Initialise gridded ingestion class.

        :param float target_min_x:
        :param float target_max_x:
        :param float target_min_y:
        :param float target_max_y:
        :param float target_pixel_size_x:
        :param float target_pixel_size_y:
        :param numpy.array  target_crs:
        :param numpy.datetime64 output_time: output time to use for layer
        """
        self.target_crs = target_crs

        self.target_min_x = target_min_x
        self.target_max_x = target_max_x
        self.target_min_y = target_min_y
        self.target_max_y = target_max_y

        self.target_pixel_size_x = target_pixel_size_x
        self.target_pixel_size_y = target_pixel_size_y

        # Set up array of coordinates
        # These correspond to the centre coordinate of each pixel, which is
        # what is returned by ODC.
        self.target_x_coords = numpy.arange(
            target_min_x + (target_pixel_size_x / 2),
            target_max_x + (target_pixel_size_x / 2),
            target_pixel_size_x,
        )
        self.target_y_coords = numpy.arange(
            target_max_y + (target_pixel_size_y / 2),
            target_min_y + (target_pixel_size_y / 2),
            target_pixel_size_y,
        )
        # Set up output transform
        self.target_transform = affine.Affine(
            self.target_pixel_size_x,
            0,
            self.target_min_x,
            0,
            self.target_pixel_size_y,
            self.target_max_y,
        )

        # Set output time. This is the time to assign to the layer, not what is read from xarray
        # Should be the same for all layers which are to be stacked together in the classification.
        if output_time is None:
            self.output_time = None
        else:
            self.output_time = numpy.array(
                [output_time], dtype=numpy.datetime64
            )

        # Set up output dimensions and coordinates for xarray.
        if self.output_time is not None:
            self.output_dims = ["time", "y", "x"]
            self.output_coords = {
                "time": self.output_time,
                "y": self.target_y_coords,
                "x": self.target_x_coords,
            }
        else:
            self.output_dims = ["y", "x"]
            self.output_coords = {
                "y": self.target_y_coords,
                "x": self.target_x_coords,
            }

    def resample_regrid(self, in_xarray, interpolation_method="linear"):
        """
        Check if xarray matches expected grid, if not resamples or
        regrids and returns result

        :param xarray.Dataset in_xarray: input xarray to check size and regrid or resample as needed
        :param str interpolation_method: method to use for interpolation
        :returns: regridded xarray
        :rtype: xarray.Dataset
        """
        # Check that the image has dimensions
        if (
            in_xarray.coords["x"].values.size == 0
            or in_xarray.coords["y"].values.size == 0
        ):
            raise Exception(
                "Input array contains no data for {}"
                "".format(" ".join(in_xarray.data_vars.keys()))
            )
        # Check that they are the expected size
        elif (
            in_xarray.coords["x"].values.shape != self.target_x_coords.shape
        ) or (
            in_xarray.coords["y"].values.shape != self.target_y_coords.shape
        ):
            logging.warning(
                "Interpolating {}".format(" ".join(in_xarray.data_vars.keys()))
            )
            out_xarray = in_xarray.interp(
                x=self.target_x_coords,
                y=self.target_y_coords,
                method=interpolation_method,
            )
        # Check they line up with input dimensions
        elif (
            max(in_xarray.coords["x"].values - self.target_x_coords) != 0
            or max(in_xarray.coords["y"].values - self.target_y_coords) != 0
        ):
            logging.warning(
                "Re-gridding {}".format(" ".join(in_xarray.data_vars.keys()))
            )
            out_xarray = in_xarray.interp(
                x=self.target_x_coords,
                y=self.target_y_coords,
                method=interpolation_method,
            )
        else:
            out_xarray = in_xarray

        return out_xarray

    def set_to_constant_value(self, variable_name=None, constval=0):
        """
        Function to create a variable with a constant value as an xarray.
        The size is equal to self.target_x_coords x self.target_y_coords

        Can be used when an input variable required for a classification isn't
        available but the value is known from other sources. For example if
        there is no artificial water for the area the classification is being
        run on can use to set all values to 0.

        :param str variable_name: name to use for variable within output xarray
        :param float constval: a constant value to assign to all elements in the xarray
        :return: xarray Dataset containing variable with coordinates self.target_x_coords,
                 self.target_y_coords.
        :rtype: xarray.Dataset
        """
        # Check the output variable name
        if not self.check_variable_name(variable_name):
            raise Exception("Input variable name is inappropriate, see log.")

        # Create array of constant values
        # Set up empty array, using minimum dtype which can hold
        # constant value.
        if self.output_time is None:
            rslt_vals = numpy.empty(
                (self.target_y_coords.size, self.target_x_coords.size),
                dtype=numpy.min_scalar_type(constval),
            )
        else:
            # Add extra dimension for time.
            rslt_vals = numpy.empty(
                (1, self.target_y_coords.size, self.target_x_coords.size),
                dtype=numpy.min_scalar_type(constval),
            )
        rslt_vals[...] = constval

        # Convert to xarray.

        variable_xarray = xarray.Dataset(
            {variable_name: (self.output_dims, rslt_vals)},
            coords=self.output_coords,
        )

        return variable_xarray

    def read_data_from_gdal(
        self,
        input_file,
        variable_name,
        expstr=None,
        band=1,
        resampling="nearest",
        **kwargs
    ):
        """
        Function to load GDAL file into an xarray

        Uses xarray.open_rasterio function.

        Takes an array with x coordinates and y coordinates and uses
        these to resample / re-grid data if required

        :param str input_file: input file in any format GDAL can read.
        :param str variable_name: name to use for variable within output xarray,.
                                  Variable name must not contain non-alphanumeric
                                  characters.
        :param str expstr: expression to evaluate using input band. Can be used to
                           apply a threshold or scaling factor. Must take form of
                           'band > 100'
        :param int band: band to use from raster (default=1)
        :param str resampling: method to use when resampling data (default=nearest)
        :param kwargs: any other input parameters.
        :return: xarray Dataset containing variable(s) with coordinates self.object_ids.
        :rtype: xarray.Dataset
        """
        # Check the output variable name
        if not self.check_variable_name(variable_name):
            raise Exception("Input variable name is inappropriate, see log.")

        try:
            resampling_method = getattr(rasterio.enums.Resampling, resampling)
        except AttributeError:
            raise AttributeError(
                "Method for resampling ({}) not supported. "
                "Valid options are: {}".format(
                    resampling,
                    ",".join([str(i) for i in rasterio.enums.Resampling]),
                )
            )

        with rasterio.open(input_file) as ds:
            # Get pze and check against target.
            # Print info to log file if they are different.
            transform = ds.get_transform()
            if (
                self.target_pixel_size_x != transform[1]
                or self.target_pixel_size_y != transform[5]
            ):
                logging.info(
                    "Interpolating {} from pixel size of: {},{}"
                    " to {},{} using {}.".format(
                        input_file,
                        transform[1],
                        transform[5],
                        self.target_pixel_size_x,
                        self.target_pixel_size_y,
                        resampling,
                    )
                )

            # Open band (don't load)
            band = rasterio.band(ds, int(band))
            # Set up output array for data
            out_array = numpy.empty(
                (self.target_y_coords.size, self.target_x_coords.size),
                dtype=band.dtype,
            )

            # Perform windowed read to get data for target array
            # Resample as data is being read.
            rasterio.warp.reproject(
                source=band,
                destination=out_array,
                dst_crs=self.target_crs,
                dst_transform=self.target_transform,
                resampling=resampling_method,
            )

        # If using an output time then need to add an extra dimension for it.
        if self.output_time is not None:
            out_array = out_array.reshape(
                [1, out_array.shape[0], out_array.shape[1]]
            )

        # Convert to xarray
        variable_xarray = xarray.Dataset(
            {variable_name: (self.output_dims, out_array)},
            coords=self.output_coords,
        )

        # Check matches existing grid
        self.resample_regrid(variable_xarray)

        if expstr is not None:
            # Use 'band' for expressions as more intuitive but dataset is named
            # using variable name so replace in expression before
            # running.
            variable_xarray = self.xarray_maths(
                variable_xarray,
                variable_name,
                [variable_name],
                expstr.replace("band", variable_name),
            )

        variable_xarray.attrs["accessories"] = input_file

        return variable_xarray

    def read_data_from_odc(
        self,
        product,
        measurements,
        variable_name,
        start_time=None,
        end_time=None,
        expstr=None,
        missing_layer_value=None,
        datacube_env=None,
        **kwargs
    ):
        """
        Function to extract data from an open data cube and return an xarray.

        :param str product: name of product in data cube
        :param list measurements: list of measurements to use.
        :param str variable_name: name to use for variable within output xarray,.
                                  Variable name must not contain non-alphanumeric
                                  characters.
        :param start_time str: start time for dataset
        :param end_time str: start time for dataset
        :param str expstr: expression to evaluate using input variable. Can be used to
                           apply a threshold or scaling factor. If more than one
                           measurement is supplied must supply an expression.
        :param kwargs: any other input parameters.
        :return: xarray Dataset containing variable(s) with coordinates self.object_ids.
        :rtype: xarray.Dataset
        """
        # Import the datacube module when needed.
        import datacube

        # Check the output variable name
        if not self.check_variable_name(variable_name):
            raise Exception("Input variable name is inappropriate, see log.")

        if not isinstance(measurements, list):
            measurements = [measurements]

        if len(measurements) > 1 and expstr is None:
            raise Exception(
                "Must supply an expression with multiple " "measurements"
            )

        if datacube_env is not None:
            dc = datacube.Datacube(app="le_lccs", env=datacube_env)
        else:
            dc = datacube.Datacube(app="le_lccs")

        query = {
            "x": (self.target_min_x, self.target_max_x),
            "y": (self.target_max_y, self.target_min_y),
            "crs": self.target_crs,
            "output_crs": self.target_crs,
            "resolution": (self.target_pixel_size_y, self.target_pixel_size_x),
        }

        if start_time is not None and end_time is not None:
            query["time"] = (start_time, end_time)
        # Add lineage
        lineage = []
        dss = dc.find_datasets(
            product=product, measurements=measurements, **query
        )
        for i in dss:
            lineage.append((i.type.name, i.id))

        # Get data from open data cube
        data = dc.load(product=product, measurements=measurements, **query)

        data.attrs["lineage"] = lineage

        # Check if no data has been returned
        if len(data.dims) == 0 and missing_layer_value is None:
            # If no missing layer value has been provided raise exception
            raise Exception(
                "No data found from {product} : {measurements} "
                "for selected dates ({start_time} -- {end_time})"
                "".format(**locals())
            )
        elif len(data.dims) == 0 and missing_layer_value is not None:
            # If a missing layer value has been provided print warning
            # then return an array containing a single value.
            logging.warning(
                "No data found from {product} : {measurements} "
                "for selected dates ({start_time} -- {end_time})."
                "Setting all to {missing_layer_value}."
                "".format(**locals())
            )
            data = self.set_to_constant_value(
                variable_name, constval=missing_layer_value
            )

            # If output time has been provided then set to this.
            if self.output_time is not None:
                try:
                    data[variable_name] = data[variable_name].expand_dims(
                        {"time": self.output_time}
                    )
                # If it already exists will thow ValueError so drop existing dimension and add
                except ValueError:
                    data = data.squeeze("time").drop_vars("time")
                    data[variable_name] = data[variable_name].expand_dims(
                        {"time": self.output_time}
                    )

            return data

        # Check if multiple dates have been returned. Currently only
        # deal with a single date.
        elif data.coords["time"].size > 1:
            raise Exception(
                "Date from multiple dates found for {product} : "
                "{measurements}\n{returned_dates}\n"
                "Adjust date range so a single date is selected"
                "".format(
                    **locals(), returned_dates=data.coords["time"].values
                )
            )

        # Get start and end time (will both be the same)
        start_time = data.time.values.min()
        end_time = data.time.values.max()

        # Check data is expected size
        data = self.resample_regrid(data)
        # If only one measurement was supplied and there is no expression
        # to apply just rename to output variable
        if expstr is None and len(measurements) == 1:
            data = data.rename(measurements[0], variable_name)
        else:
            data = self.xarray_maths(data, variable_name, measurements, expstr)

        # If output time has been provided then set to this.
        if self.output_time is not None:
            try:
                data[variable_name] = data[variable_name].expand_dims(
                    {"time": self.output_time}
                )
            # If it already exists will thow ValueError so drop existing dimension and add
            except ValueError:
                data = data.squeeze("time").drop_vars("time")
                data[variable_name] = data[variable_name].expand_dims(
                    {"time": self.output_time}
                )
        # If not drop time dimension
        else:
            data = data.squeeze("time").drop_vars("time")

        return data


class LEIngestConstGridded(LEIngestGridded):
    """
    Create an array with a constant value
    """

    def read_to_xarray(self, variable_name=None, constval=0, **kwargs):
        return self.set_to_constant_value(variable_name, constval)


class LEIngestGDAL(LEIngestGridded):
    """
    Ingest data stored as a static raster, which GDAL can read.
    """

    def read_to_xarray(
        self,
        input_file,
        variable_name,
        expstr=None,
        band=1,
        resampling="nearest",
        **kwargs
    ):
        """
        Load GDAL file into an xarray.

        Calls self.read_data_from_gdal

        Uses xarray.open_rasterio function.

        Takes an array with x coordinates and y coordinates and uses
        these to resample / re-grid data if required

        :param str input_file: input file in any format GDAL can read.
        :param str variable_name: name to use for variable within output xarray,.
                                  Variable name must not contain non-alphanumeric
                                  characters.
        :param str expstr: expression to evaluate using input band. Can be used to
                           apply a threshold or scaling factor. Must take form of
                           'band > 100'
        :param int band: band to use from raster (default=1)
        :param str resampling: method to use when resampling data (default=nearest)
        :param kwargs: any other input parameters.
        :return: xarray Dataset containing variable(s) with coordinates self.object_ids.
        :rtype: xarray.Dataset
        """
        return self.read_data_from_gdal(
            input_file, variable_name, expstr, band, resampling, **kwargs
        )


class LEIngestODC(LEIngestGridded):
    """
    Ingest data from Open Data Cube.
    """

    def read_to_xarray(
        self,
        product,
        measurements,
        variable_name,
        start_time=None,
        end_time=None,
        expstr=None,
        missing_layer_value=None,
        datacube_env=None,
        **kwargs
    ):
        """
        Extract data from an open data cube and return an xarray.

        :param str product: name of product in data cube
        :param list measurements: list of measurements to use.
        :param str variable_name: name to use for variable within output xarray,.
                                  Variable name must not contain non-alphanumeric
                                  characters.
        :param start_time str: start time for dataset
        :param end_time str: start time for dataset
        :param str expstr: expression to evaluate using input variable. Can be used to
                           apply a threshold or scaling factor. If more than one
                           measurement is supplied must supply an expression.
        :param kwargs: any other input parameters.
        :return: xarray Dataset containing variable(s) with coordinates self.object_ids.
        :rtype: xarray.Dataset
        """

        return self.read_data_from_odc(
            product,
            measurements,
            variable_name,
            start_time,
            end_time,
            expstr,
            missing_layer_value,
            datacube_env,
            **kwargs
        )


class LEIngestODCV(LEIngestGridded):
    """
    Ingest data from Open Data Cube.
    """

    def read_to_xarray(
        self,
        variable_name,
        start_time,
        end_time,
        vp_catalogue,
        virtual_product,
        transformations=None,
        aggregations=None,
        missing_layer_value=None,
        auxiliary_products={},
        datacube_env=None,
        **kwargs
    ):
        """
        Extracts data from an open data cube, calculates a virtual product and returns an xarray.

        For more information on virtual products see: https://datacube-core.readthedocs.io/en/latest/dev/virtual-products.html
        :param str variable_name: name to use for variable within output xarray,.
                                  Variable name must not contain non-alphanumeric
                                  characters.
        :param start_time str: start time for dataset
        :param end_time str: start time for dataset
        :param str vp_catalogue: path to the catalogue file containing the definition of the virtual product to run
        :param list transformations: list of any additional transformations which the recipe needs.
                                     Transformation class name must match the transformations file name.
        :param str virtual_product: name of virtual product function to use.
                                    Must be a product in provided vp_catalogue.
        :auxiliary_products: A dictionary containing the name of auxiliary products
        :param kwargs: any other input parameters.
        :return: xarray Dataset containing variable(s) with coordinates self.object_ids.
        :rtype: xarray.Dataset
        """
        import datacube
        import importlib
        from datacube.virtual import catalog_from_file
        from datacube.virtual import DEFAULT_RESOLVER

        if datacube_env is not None:
            dc = datacube.Datacube(env=datacube_env)
        else:
            dc = datacube.Datacube()

        # get name of product
        item = virtual_product

        # load catalogue
        try:
            catalog = catalog_from_file(vp_catalogue)
        except FileNotFoundError:
            raise FileNotFoundError(
                "Could not find virtual product catalogue {}".format(
                    vp_catalogue
                )
            )

        # load virtual product
        if transformations:
            try:
                # load all needed additional transformations
                for transformation in transformations:
                    trans_loc = importlib.import_module(transformation)
                    trans_class = transformation.split(".")[-1]
                    if (
                        trans_class
                        not in DEFAULT_RESOLVER.lookup_table["transform"]
                    ):
                        module_class = getattr(trans_loc, trans_class)
                        if auxiliary_products:
                            module_class.set_auxiliary_products(
                                auxiliary_products
                            )
                        DEFAULT_RESOLVER.register(
                            "transform", trans_class, module_class
                        )

            except ModuleNotFoundError:
                raise ModuleNotFoundError(
                    "Could not import module '{}'. Likely the path to this"
                    " module needs adding to the PYTHONPATH environmental"
                    " variable".format(transformation)
                )

        if aggregations:
            try:
                # load all needed additional aggregations
                for aggregation in aggregations:
                    agg_loc = importlib.import_module(aggregation)
                    agg_class = aggregation.split(".")[-1]

                    if (
                        agg_class
                        not in DEFAULT_RESOLVER.lookup_table["aggregate"]
                    ):
                        DEFAULT_RESOLVER.register(
                            "aggregate", agg_class, getattr(agg_loc, agg_class)
                        )
            except ModuleNotFoundError:
                raise ModuleNotFoundError(
                    "Could not import module '{}'. Likely the path to this"
                    " module needs adding to the PYTHONPATH environmental"
                    " variable".format(aggregation)
                )

        logging.info(
            "Calculating virtual product using {}".format(virtual_product)
        )

        query = {
            "x": [self.target_min_x, self.target_max_x],
            "y": [self.target_max_y, self.target_min_y],
            "crs": self.target_crs,
            "output_crs": self.target_crs,
            "resolution": (self.target_pixel_size_y, self.target_pixel_size_x),
            "time": (start_time, end_time),
        }

        # retrieve datasets for query
        datasets = catalog[item].query(
            dc, **query, dask_chunks={"x": 1000, "y": 1000}
        )

        grouped = catalog[item].group(
            datasets, **query, dask_chunks={"x": 1000, "y": 1000}
        )
        data = catalog[item].fetch(
            grouped, **query, dask_chunks={"x": 1000, "y": 1000}
        )

        # gather lineage information
        lineage = []
        lineage = [(i.type.name, i.id) for i in datasets.contained_datasets()]

        data.attrs["lineage"] = lineage

        # Check if no data has been returned
        if len(data.dims) == 0 and missing_layer_value is None:
            # If no missing layer value has been provided raise exception
            raise Exception(
                "No data found from {product} : {measurements} "
                "for selected dates ({start_time} -- {end_time})"
                "".format(**locals())
            )
        elif len(data.dims) == 0 and missing_layer_value is not None:
            # If a missing layer value has been provided print warning
            # then return an array containing a single value.
            logging.warning(
                "No data found from {product} : {measurements} "
                "for selected dates ({start_time} -- {end_time})."
                "Setting all to {missing_layer_value}."
                "".format(**locals())
            )
            data = self.set_to_constant_value(
                variable_name, constval=missing_layer_value
            )
            return data

        # Check data is expected size
        data = self.resample_regrid(data)

        # If output time has been provided then set to this.
        if self.output_time is not None:
            try:
                data[variable_name] = data[variable_name].expand_dims(
                    {"time": self.output_time}
                )
            # If it already exists will thow ValueError so just update existing dimension
            except ValueError:
                data = data.squeeze("time").drop_vars("time")
                data[variable_name] = data[variable_name].expand_dims(
                    {"time": self.output_time}
                )

        return data
