"""
Base class for exporting data from LCCS to various formats.
"""

class LEExport(object):

    def write_xarray(self, output_file, **kwargs):
        """
        Abstract function to write data from xarray
        """
        raise NotImplementedError
