"""
Utilities to export gridded data.
"""

import logging
import numpy
import os

import affine
import rasterio
import os.path
from pathlib import Path
from eodatasets3 import DatasetAssembler
import datetime
from eodatasets3.images import GridSpec
from rasterio.crs import CRS
from rasterio.enums import Resampling
from scipy.ndimage import binary_fill_holes
from . import export_base


class LEExportGridded(export_base.LEExport):
    """
    Abstract class for exporting gridded data from LE LCCS system.
    """

    def __init__(
        self,
        target_min_x,
        target_max_x,
        target_min_y,
        target_max_y,
        target_pixel_size_x,
        target_pixel_size_y,
        target_crs,
        output_time=None,
    ):
        """
        Initialise gridded export

        :param float target_min_x:
        :param float target_max_x:
        :param float target_min_y:
        :param float target_max_y:
        :param float target_pixel_size_x:
        :param float target_pixel_size_y:
        :param numpy.array: target_crs:
        :param numpy.datetime64 output_time: dummy input so inputs are the same as ingest classes.
        """
        self.target_crs = target_crs

        self.target_min_x = target_min_x
        self.target_max_x = target_max_x
        self.target_min_y = target_min_y
        self.target_max_y = target_max_y

        self.target_pixel_size_x = target_pixel_size_x
        self.target_pixel_size_y = target_pixel_size_y

        # Set up array of coordinates
        # These corespond to the centre coordinate of each pixel, which is
        # what is returned by ODC.
        self.target_x_coords = numpy.arange(
            target_min_x + (target_pixel_size_x / 2),
            target_max_x + (target_pixel_size_x / 2),
            target_pixel_size_x,
        )
        self.target_y_coords = numpy.arange(
            target_max_y + (target_pixel_size_y / 2),
            target_min_y + (target_pixel_size_y / 2),
            target_pixel_size_y,
        )
        # Set up output transform
        self.target_transform = affine.Affine(
            self.target_pixel_size_x,
            0,
            self.target_min_x,
            0,
            self.target_pixel_size_y,
            self.target_max_y,
        )


class LEExportGDAL(LEExportGridded):
    """
    Class to export data as a raster using rasterio.
    """

    def write_xarray(
        self,
        data_xarray,
        output_file,
        variable_names_list=None,
        exclude_variable_names_list=None,
        driver="GTiff",
        out_dtype=None,
        cast_to_out_dtype=True,
        odc_indexable=False,
        **kwargs,
    ):
        """
        Function to write an xarray dataset to a raster (e.g., GeoTiff).
        Can write a selection of variables or all of them.
        Each variable is written as a new band.

        :param xarray.Dataset data_xarray: xarray dataset containing data to write out.
        :param str output_file: path to output file.
        :param list variable_names_list: list containing variables to write out.
        :param list exclude_variable_names_list: list containing variables to skip writing out.
        :param str driver: name of GDAL driver to use (e.g. GTiff).
        :param type out_dtype: output data type to use.
        :param boolean multiband: output as multiband tiff or single band per tiff

        """

        if (
            variable_names_list is not None
            and exclude_variable_names_list is not None
        ):
            raise Exception(
                "Can't specify a list of variables to include and exclude"
            )

        all_variables = [var for var in data_xarray.data_vars]

        # If a list of variables isn't supplied assume want all in xarray
        if variable_names_list is None and exclude_variable_names_list is None:
            variable_names_list = all_variables
        # If a list of variables to exclude is passed in remove these
        elif exclude_variable_names_list is not None:
            variable_names_list = []
            for var in all_variables:
                if var not in exclude_variable_names_list:
                    variable_names_list.append(var)
        # Else check the ones supplied are in xarray
        else:
            for var in variable_names_list:
                if var not in all_variables:
                    raise Exception(
                        "The variable {} is not in the supplied "
                        "xarray".format(var)
                    )

        output_x_size = self.target_x_coords.size
        output_y_size = self.target_y_coords.size

        additional_creation_options = {}

        if driver == "GTiff":
            additional_creation_options["compress"] = "LZW"
            additional_creation_options["tiled"] = "True"

        if not odc_indexable:
            # If dtype isn't specified get from first variable
            if out_dtype is None:
                out_dtype = data_xarray[variable_names_list[0]].dtype

            # Go through and check dtype of variables
            # Keep only those which match output type
            checked_variable_names_list = []
            for i, var in enumerate(variable_names_list):
                if (
                    data_xarray[var].dtype != out_dtype
                    and not cast_to_out_dtype
                ):
                    logging.warning(
                        "Data type of {} does not match output type for"
                        " raster. Skipping this variable".format(var)
                    )
                else:
                    checked_variable_names_list.append(var)

            # Number of output bands is the same as the number of variables
            num_bands = len(checked_variable_names_list)

            # Write classification out
            out_raster = rasterio.open(
                output_file,
                "w",
                driver=driver,
                height=output_y_size,
                width=output_x_size,
                count=num_bands,
                dtype=out_dtype,
                crs=self.target_crs,
                transform=self.target_transform,
                **additional_creation_options,
            )

            for band, var in enumerate(checked_variable_names_list, start=1):
                # Write data
                if cast_to_out_dtype:
                    data_xarray[var].values = data_xarray[var].values.astype(
                        out_dtype
                    )
                # Reshape before writing out. This means if there is a time dimension then
                # this will be dropped.
                out_raster.write(
                    data_xarray[var].values.reshape(
                        [output_y_size, output_x_size]
                    ),
                    band,
                )
                # Set band name
                out_raster.set_band_description(band, var)

            out_raster.close()
            logging.info("Saved to {}".format(output_file))

        else:  # Create datasets with associated metadata for indexing into a datacube

            filepath, filename = os.path.split(output_file)
            filename, ext = os.path.splitext(filename)

            if not Path(filepath).exists():
                os.makedirs(filepath)
            with DatasetAssembler(
                Path(filepath), naming_conventions="default"
            ) as p:
                p.platform = kwargs["platform"]
                p.instrument = kwargs["instrument"]
                if kwargs.get("product_date", None) is None:
                    raise ValueError("pdoduct_date is None, for odc_indexable output, file must supply product_date in format YYYY-mm-dd.")
                p.datetime = datetime.datetime.strptime(kwargs["product_date"], "%Y-%m-%d")
                p.product_family = kwargs["product_family"]
                p.product_name = kwargs["product_name"]
                p.producer = kwargs["producer"]
                p.region_code = kwargs["tile_id"]
                p.processed_now()
                for dependency in kwargs["software_details"]:
                    p.note_software_version(
                        dependency[0], dependency[1], dependency[2]
                    )

                for dataset in data_xarray.attrs.get("lineage"):
                    p.note_source_datasets(dataset[0], dataset[1])

                if data_xarray.attrs.get("accessories"):
                    p.note_accessory_file(
                        "lineage:static", data_xarray.attrs.get("accessories")
                    )

                if data_xarray.attrs.get("input_products"):
                    p.extend_user_metadata(
                        "input_products",
                        data_xarray.attrs.get("input_products"),
                    )

                # Write measurements.
                for band, var in enumerate(variable_names_list):
                    filename = f"{kwargs.get('region_code')}_{var}"
                    filename = var
                    # Write data
                    if cast_to_out_dtype:
                        data_xarray[var].values = data_xarray[
                            var
                        ].values.astype(out_dtype)

                    p.write_measurement_numpy(
                        filename,
                        data_xarray[var].values.reshape(
                            [output_y_size, output_x_size]
                        ),
                        GridSpec(
                            shape=(output_x_size, output_y_size),
                            transform=self.target_transform,
                            crs=CRS.from_string(self.target_crs),
                        ),
                        overview_resampling=Resampling.mode,
                    )
                # Fill holes in the valid-data mask before geometry is calculated.

                for _, mask in p._measurements.mask_by_grid.items():
                    binary_fill_holes(mask, output=mask)
                p.done()


class LEExportNetCDF(LEExportGridded):
    """
    Class to export data as a netCDF file.
    """

    def write_xarray(
        self,
        data_xarray,
        output_file,
        variable_names_list=None,
        exclude_variable_names_list=None,
        out_encoding_options={"zlib": True, "complevel": 4},
        **kwargs,
    ):
        """
        Function to write an xarray dataset to a raster (e.g., GeoTiff).
        Can write a selection of variables or all of them.
        Each variable is written as a new band.

        :param xarray.Dataset data_xarray: xarray dataset containing data to write out.
        :param str output_file: path to output file.
        :param list variable_names_list: list containing variables to write out,
        :param list exclude_variable_names_list: list containing variables to skip writing out.
        :param dict out_encoding_options: Output netCDF encoding options to use for each variable. Can use to set compression etc.
        :param type out_dtype: output data type to use.

        """

        if (
            variable_names_list is not None
            and exclude_variable_names_list is not None
        ):
            raise Exception(
                "Can't specify a list of variables to include and exclude"
            )

        all_variables = [var for var in data_xarray.data_vars]

        # If a list of variables isn't supplied assume want all in xarray
        if variable_names_list is None and exclude_variable_names_list is None:
            variable_names_list = all_variables
        # If a list of variables to exclude is passed in remove these
        elif exclude_variable_names_list is not None:
            variable_names_list = []
            for var in all_variables:
                if var not in exclude_variable_names_list:
                    variable_names_list.append(var)
        # Else check the ones supplied are in xarray
        else:
            for var in variable_names_list:
                if var not in all_variables:
                    raise Exception(
                        "The variable {} is not in the supplied "
                        "xarray".format(var)
                    )

        # Set up encoding options for output
        out_nc_encoding = {}
        if out_encoding_options is not None:
            for var_name in variable_names_list:
                out_nc_encoding[var_name] = out_encoding_options

        data_xarray[variable_names_list].to_netcdf(
            output_file,
            format="NETCDF4",
            engine="netcdf4",
            encoding=out_nc_encoding,
        )