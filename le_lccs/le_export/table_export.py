"""
Utilities to write LCCS classification to a Raster Attribute Table.
"""
import os
import logging

from . import export_base

class LEExportTable(export_base.LEExport):
    """
    Abstract class for exporting data from a table in the LE LCCS system
    to a table
    """

class LEExportRAT(LEExportTable):
    """
    Export data stored as a Raster Attribute Table (RAT) into the LE LCCS
    system.
    """

    def write_xarray(self, data_xarray, output_file, variable_names_list=None,
                     **kwargs):
        """
        Write an xarray to a Raster Attribute Table
        """

        from osgeo import gdal
        from rios import rat

        all_variables = [var for var in data_xarray.data_vars]

        # If a list of variables isn't supplied assume want all in xarray
        if variable_names_list is None:
            variable_names_list = all_variables
        # Else check the ones supplied are in xarray
        else:
            for var in variable_names_list:
                if var not in all_variables:
                    raise Exception("The variable {} is not in the supplied "
                                    "xarray".format(var))

        # Open output file to append to
        if not os.path.isfile(output_file):
            raise Exception("Output file must exist to append to")

        rat_ds = gdal.Open(output_file, gdal.GA_Update)

        if rat_ds is None:
            raise IOError("Could not open '{}' to update".format(output_file))

        for var in variable_names_list:
            rat.writeColumn(rat_ds, var, data_xarray[var].values)

        rat_ds = None
        logging.info("Wrote columns to {}".format(output_file))

