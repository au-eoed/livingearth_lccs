"""
LivingEarth LCCS Export tools

Functions to export data classifed using the LCCS system.

Plugins
---------

The module must start with 'le_export_' and be available
on the PYTHONPATH or LE_LCCS_PLUGINS_PATH.

Functions can take any arguments but must include at least `**kwargs`.

They should all accept an xarray.Dataset.

"""
import importlib
import os
import pkgutil
import sys

# Load inbuilt packages
from . import gridded_export
from . import table_export

# Load plugins
for finder, name, ispkg in pkgutil.iter_modules():
    if name.startswith("le_export_"):
        locals()[name] = importlib.import_module(name)
