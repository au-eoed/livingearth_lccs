#!/usr/bin/env python
"""
Module to run Living Earth LCCS Classification on gridded data

"""

import logging
import yaml
import time
import numpy
import xarray

from .. import le_ingest
from .. import le_export
from ..le_classification import lccs_l3
from ..le_classification import lccs_l4

from importlib_metadata import distribution, PackageNotFoundError

def le_region_pkg_metadata(region_code):
    # Get details of region specific software used to perform classification
    # If specific software package hasn't been defined for area returns None
    # so no additional data will be added.
    if region_code == "au":
        try:
            dist = distribution('livingearth_australia')
        except PackageNotFoundError:
            logging.warning("Region is 'au' but livingearth_australia package not found")
            return None
    else:
        return None

    name = dist.metadata['Name']
    version = dist.metadata['Version']
    url = dist.metadata['Home-page']
    return (name, url, version)

class Product():
    VERSION = "1.1.0"
    NAME = "LIVINGEARTH_LCCS"
    PRODUCT_FAMILY = "LC"
    URL = "https://bitbucket.org/au-eoed/livingearth_lccs"

    def __init__(self, cfg, level, product_date, tile_id=''):

        self._cfg = cfg["Outputs"][level]
        if "out_dtype" in self._cfg.keys():
            self._cfg.update({"out_dtype": getattr(numpy, self._cfg["out_dtype"])})

        self._cfg["odc_indexable"] = self._cfg["odc_indexable"] if \
            self._cfg.get("odc_indexable") is not None else False
        self._cfg["product_date"] = product_date
        self._cfg["product_name"] = cfg["product_name"]
        self._cfg["product_family"] = cfg["product_family"]
        self._cfg["dataset_version"] = cfg["dataset_version"]
        self._cfg["tile_id"] = tile_id
        self._cfg["software_details"] = [(self.NAME, self.URL, self.VERSION)]
        region_specific_software_details = le_region_pkg_metadata(cfg["region_specific_lccs_code"])
        if region_specific_software_details is not None:
            self._cfg["software_details"].append(region_specific_software_details)
        self._cfg["classification_scheme"] = cfg["classification_scheme"]
        self._cfg["producer"] = cfg["producer"]
        self._cfg["platform"] = cfg["platform"]
        self._cfg["instrument"] = cfg["instrument"]

    def config(self):
        return self._cfg

    def update(self, key, value):
        self._cfg.update({key: value})

    def export_module_name(self):
        return self._cfg["export_class"].split('.')[0]

    def export_class_name(self):
        return self._cfg["export_class"].split('.')[1]

def get_input_files(layer):
    input_files = []
    for product, val in layer.items():
        if "input_file" in val:
            input_files.append(val.get("input_file"))
    return input_files



def run_classification(config_file, extent=None,
                       output_l3_file_name=None, output_l3_rgb_file_name=None,
                       output_l4_file_name=None, output_l4_rgb_file_name=None,
                       level3_only=False, product_date=None,
                       tile_id=None, profiling_flag=False):
    """
    Big function to run the LCCS Level 3 and Level 4 classification.

    Steps are:

    1. Read in layers defined in config file
    2. Run Level 3 classification and export
    3. Run Level 4 classification and export

    """

    # Read in config file
    with open(config_file, "r") as f:
        config = yaml.safe_load(f)

    # Get classification scheme to use, the default is 'lccs' but other options (e.g., au)
    # are available.
    classification_scheme = config["classification_scheme"] \
        if config.get("classification_scheme") is not None else "lccs"

    try:
        level3_classes_list = lccs_l3.EXPECTED_L3_CLASSES[classification_scheme]
        level4_classes_list = lccs_l4.LAYER_CLASSES[classification_scheme]
    except KeyError:
        raise Exception("Classes not found for scheme '{}', the following options are available: {}"
                        "".format(classification_scheme, ", ".join(list(lccs_l4.LAYER_CLASSES.keys()))))

    # Set up dictionary to define size parameters
    target_size = {}
    # If an extent is provided then use this instead of the values
    # in the config file
    if extent is not None:
        if not isinstance(extent, list) or len(extent) != 4:
            raise TypeError("Extent must be a list with 4 values")

        else:
            target_size["target_min_x"] = extent[0]
            target_size["target_max_x"] = extent[2]
            target_size["target_min_y"] = extent[1]
            target_size["target_max_y"] = extent[3]
    else:
        target_size["target_min_x"] = config["extent"]["min_x"]
        target_size["target_max_x"] = config["extent"]["max_x"]
        target_size["target_min_y"] = config["extent"]["min_y"]
        target_size["target_max_y"] = config["extent"]["max_y"]

    target_size["target_pixel_size_x"] = config["resolution"][0]
    target_size["target_pixel_size_y"] = config["resolution"][1]

    target_size["target_crs"] = config["crs"]

    # Check the dimensions are a multiple of the resolution
    width = target_size["target_max_x"] - target_size["target_min_x"]
    height = target_size["target_max_y"] - target_size["target_min_y"]

    if width % target_size["target_pixel_size_x"] != 0:
        raise Exception("Width must be a multiple of x resolution, please adjust the extent.")
    if height % target_size["target_pixel_size_y"] != 0:
        raise Exception("Height must be a multiple of y resolution, please adjust the extent.")

    # If the output time for the classification is specified in the config file then
    # add this to the target size dictionary so it is passed to the ingest class
    # else pass.
    try:
        target_size["output_time"] = config["classification_time"]
    except KeyError:
        pass

    # Set up list of input layers to read in
    # Try to read all level 3 layers
    try:
        input_layers = config["L3layers"]
    # If no 'L3layers' defined in the config file
    # try 'Layers' (format used for older Level3 only config files)
    except KeyError:
        input_layers = config["Layers"]

    # Compile a list of input_files for layer3
    l3_input_files = get_input_files(input_layers)

    l4_layers = []

    # If a level4 output file is required read in input
    # layers needed for it at same time as level3 inputs
    if (config.get("Outputs").get("level4_data") or
            config.get("Outputs").get("level4_rgb")):
        try:
            input_layers_l4 = config["L4layers"]

        except KeyError:
            raise KeyError("Level 4 output specified but no "
                           " 'L4layers' defined in input config")
        input_layers.update(input_layers_l4)
        l4_layers = input_layers_l4.keys()
        # Add input files from layer4
        l4_input_files = get_input_files(input_layers_l4)
    # If don't want level4 then create an empty list
    else:
        l4_input_files = []

    # Read each into xarray and save to list
    logging.info("Reading in data")
    variables_xarray_list = []
    layer_names = []

    l3_datasets = []
    l4_datasets = []
    accessories = []

    profiling = {}

    for var_name, ingest_parameters in input_layers.items():
        # TODO: Need to write a wrapper to make recursive calls of `getattr` nicer.
        # Don't run any of the 'xarray_maths' classes yet - nead to load all other
        # variables first
        import_class_name = ingest_parameters["ingest_class"]
        # Get the class to use for importing

        ts = time.time()
        if import_class_name != "xarray_maths":
            import_class = getattr(getattr(le_ingest,
                                           import_class_name.split(".")[0]),
                                   import_class_name.split(".")[1])
            # Set up instance of import class
            import_obj = import_class(**target_size)

            # Import
            ingest_parameters["variable_name"] = var_name
            variables_xarray = import_obj.read_to_xarray(**ingest_parameters)
            if variables_xarray is not None:
                variables_xarray_list.append(variables_xarray)
                layer_names.append(var_name)
                profiling.update({f"{var_name}": time.time() - ts})

                for attr, val in variables_xarray.attrs.items():
                    if attr == 'accessories' and \
                    val not in l3_input_files and \
                    val not in l4_input_files:
                        accessories.append(val)
                    elif attr == 'lineage':
                        l3_datasets.extend([x for x in val if x not in l3_datasets])
                        if l4_layers:
                            l4_datasets.extend([x for x in val if x not in l4_datasets])

    # Merge to a single dataset
    classification_data = xarray.merge(variables_xarray_list)

    # Apply band maths (if any)
    logging.info("  Applying band maths")
    ts = time.time()
    for var_name, ingest_parameters in input_layers.items():
        import_class_name = ingest_parameters["ingest_class"]
        if import_class_name == "xarray_maths":
            variables_xarray_list.append(le_ingest.ingest_base.LEIngest().xarray_maths(
                classification_data, var_name, layer_names,
                ingest_parameters["expstr"]))

    # Make new stack with additional layers
    classification_data = xarray.merge(variables_xarray_list)
    profiling.update({f"band_maths_{var_name}": time.time() - ts})

    # Get dimensions (tuple)
    classification_dims = classification_data["vegetat_veg_cat"].dims

    # Apply Level 3 classification
    logging.info("Applying Level 3 Classification")
    ts = time.time()
    level1, level2, level3 = lccs_l3.classify_lccs_level3(
                                                          classification_data,
                                                          level3_classes_list=level3_classes_list)


    # Get colour scheme
    red, green, blue, alpha = lccs_l3.colour_lccs_level3(level3)
    l3_metadata = {"lineage": set(l3_datasets)}
    level3_out_xarray = xarray.Dataset(
        {"level1": (classification_dims, level1),
         "level2": (classification_dims, level2),
         "level3": (classification_dims, level3),
         "Red": (classification_dims, red),
         "Green": (classification_dims, green),
         "Blue": (classification_dims, blue)},
        coords=classification_data.coords,
        attrs=l3_metadata
        )

    # Write out
    export_level3_data = False
    export_level3_rgb = False

    # Check if outputs have been defined for level3
    try:
        config["Outputs"]["level3_data"]
        export_level3_data = True
    except KeyError:
        pass

    try:
        config["Outputs"]["level3_rgb"]
        export_level3_rgb = True
    except KeyError:
        pass

    # Export data (if required)
    if export_level3_data:
        logging.info("  Exporting Level 3 Classification")
        product = Product(config, "level3_data", product_date, tile_id)

        # If output file name has been passed in use this instead of the
        # one in the config file
        if output_l3_file_name is not None:
            product.update("output_file", output_l3_file_name)
        # Get export class to use
        export_class = getattr(getattr(le_export, product.export_module_name()),
                               product.export_class_name())
        # Set up instance of import class
        export_obj = export_class(**target_size)

        # Write out xarray
        export_obj.write_xarray(level3_out_xarray, **product.config())

    # Export RGB image (if required)
    if export_level3_rgb:
        logging.info("  Exporting Level 3 Classification RGB image")
        product = Product(config, "level3_rgb", product_date, tile_id)

        # Set up to only write RGB.
        product.update("variable_names_list", ["Red", "Green", "Blue"])
        product.update("out_dtype", numpy.uint8)

        # If output file name has been passed in use this instead of the
        # one in the config file
        if output_l3_rgb_file_name is not None:
            product.update("output_file", output_l3_rgb_file_name,)

#         # Get export class to use
        export_class = getattr(getattr(le_export, product.export_module_name()),
                               product.export_class_name())
        # Set up instance of export class
        export_obj = export_class(**target_size)

        # Write out xarray
        export_obj.write_xarray(level3_out_xarray, **product.config())

    profiling.update({f"level_3_{var_name}": time.time() - ts})

    # If a level4 output file is specified perform classification
    if config.get("Outputs").get("level4_data") or config.get("Outputs").get("level4_rgb"):
        logging.info("Applying Level 4 Classification")

        # Join level3 to rest of classification data
        level3_xarray = xarray.Dataset({"level3": (classification_dims, level3)},
                                       coords=classification_data.coords)

        classification_data = xarray.merge([classification_data, level3_xarray])

        # Apply classificaation
        ts = time.time()
        l4_out_classification_array = lccs_l4.classify_lccs_level4(
            classification_data, level4_classes_list=level4_classes_list)


        # Get colours and singleband codes
        logging.info("  Getting colours")

        pixel_id, red, green, blue, alpha = lccs_l4.get_combined_level4(
            l4_out_classification_array,
            lccs_l4.DEFAULT_LCCS_LC_COLOUR_SCHEMES[classification_scheme])

        l4_classification_codes_colours = xarray.Dataset(
            {"Red": (classification_dims, red),
             "Green": (classification_dims, green),
             "Blue": (classification_dims, blue)})

        l4_out_classification_array['level4'] = (classification_dims, pixel_id)
        l4_out_classification_array = xarray.merge([l4_out_classification_array,
                                                    l4_classification_codes_colours])

        input_products = {}
        input_products.update({"L3layers": ''.join(l3_input_files)}) if len(l3_input_files) > 0 else None
        input_products.update({"L4layers": ''.join(l4_input_files)}) if len(l4_input_files) > 0 else None

        l4_out_classification_array.attrs = {"lineage": set(l4_datasets + l4_datasets),
                                            "accessories": ''.join(accessories),
                                            "input_products": input_products}
        # Export data (if required)
        if config.get("Outputs").get("level4_data"):
            logging.info("  Exporting Level 4 Classification")

            product = Product(config, "level4_data", product_date, tile_id)

            if output_l4_file_name is not None:
                product.update("output_file", output_l4_file_name)
            # Get export class to use
            export_class = getattr(getattr(le_export, product.export_module_name()),
                                   product.export_class_name())
            # Set up instance of import class
            export_obj = export_class(**target_size)

            # Write out xarray
            export_obj.write_xarray(l4_out_classification_array, **product.config())

        # Export RGB image (if required)
        if config.get("Outputs") and config.get("Outputs").get("level4_rgb"):
            logging.info("  Exporting Level 4 Classification RGB image")
            product = Product(config, "level4_rgb", product_date, tile_id)
            # Set up to only write RGB.
            product.update("variable_names_list", ["Red", "Green", "Blue"])
            # If output file name has been passed in use this instead of the
            # one in the config file
            if output_l4_rgb_file_name is not None:
                product.update("output_file", output_l4_rgb_file_name)

            # Get export class to use
            export_class = getattr(getattr(le_export, product.export_module_name()),
                                   product.export_class_name())

            # Set up instance of export class
            export_obj = export_class(**target_size)

            # Write out xarray
            export_obj.write_xarray(l4_out_classification_array, **product.config())

            profiling.update({f"level_4_{var_name}": time.time() - ts})

        # Return an xarray containing input data and output classification.
        return xarray.merge([classification_data, l4_out_classification_array]) if not profiling_flag else profiling
