"""
Utilities for applying change detection

"""
import numpy
import rasterio
import xarray

from ..le_classification import simple_change
from ..le_classification import observed_change

def calc_change_gtif(year1_level4_file, year2_level4_file,
                     out_level3_change_file=None, out_observed_change_file=None, out_level4_change_file=None,
                     ignore_no_change=True, classification_scheme="lccs"):
    """
    A function to calculate simple change between two geotiffs.
    """

    if out_level3_change_file is None and out_observed_change_file is None and out_level4_change_file is None:
        raise Exception("Must provide at least one output filename")

    print("Reading in data...")
    # Open files as a data array. Assume want entire image not a subset.
    # Specify chunks to read using dask, won't place the whole thing in memory
    # to start with.
    level4_yr1 = xarray.open_rasterio(year1_level4_file, chunks={'band': 1, 'x': 1024, 'y': 1024})
    level4_yr2 = xarray.open_rasterio(year2_level4_file, chunks={'band': 1, 'x': 1024, 'y': 1024})

    # Convet to a dataframe with each band as a separate variable.
    # Take variable names from band description
    level4_yr1_list = []
    level4_yr2_list = []

    for i, layer_name in enumerate(level4_yr1.attrs["descriptions"]):
        level4_yr1_list.append(level4_yr1[i].to_dataset(name=layer_name).drop_vars("band"))
        level4_yr2_list.append(level4_yr2[i].to_dataset(name=layer_name).drop_vars("band"))

    level4_yr1 = xarray.merge(level4_yr1_list)
    level4_yr2 = xarray.merge(level4_yr2_list)

    print("Calculating simple change...")
    # Calculate level4 change for all available classes
    out_change = simple_change.get_lccs_l4_change_codes(level4_yr1, level4_yr2,
                                                        ignore_no_change=ignore_no_change,
                                                        classification_scheme=classification_scheme)

    print("Calculating evidence based change pairs...")
    out_observed_change = observed_change.get_observed_change_pairs(
        out_change, level4_yr1, level4_yr2)

    # Open file in rasterio so we can copy CRS and transform across to output file
    in_ds = rasterio.open(year1_level4_file, "r")

    if out_level3_change_file is not None:
        print("Writing out level3 change to file...")
        # Open output file
        out_ds = rasterio.open(out_level3_change_file, "w", driver="GTiff",
                               height=out_change.dims["y"], width=out_change.dims["x"],
                               count=1, dtype=numpy.uint32, nodata=0,
                               crs=in_ds.crs, transform=in_ds.transform,
                               compress="LZW", tiled="True")
        out_ds.write(out_change["level3_change"].values, 1)
        out_ds.set_band_description(1, "level3_change")
        out_ds.close()

    if out_observed_change_file is not None:
        print("Writing out observerd change to file...")

        # Get a list of output variables. Each change pair is stored as a separate variable
        # so will be written as a separate band.
        out_variables = list(out_observed_change.data_vars)

        out_ds = rasterio.open(out_observed_change_file, "w", driver="GTiff",
                               height=out_observed_change.dims["y"],
                               width=out_observed_change.dims["x"],
                               count=len(out_variables), dtype=numpy.uint8,
                               nodata=0,
                               crs=in_ds.crs, transform=in_ds.transform,
                               compress="LZW", tiled="True")

        # Write each change layer out as a separate band
        for band, var in enumerate(list(out_variables), start=1):
            out_ds.write(out_observed_change[var].values, band)
            out_ds.set_band_description(band, var)
        out_ds.close()


    if out_level4_change_file is not None:
        print("Writing out level4 change to file...")

        # Get a list of output variables
        out_variables = list(out_change.data_vars)
        # If level3 change was written to a separate file don't also write out
        # with rest of level4.
        if out_level3_change_file is not None:
            out_variables = [v for v in list(out_change.data_vars) if v not in ["level3_change"]]

        # Get output datatype from first variable. If level3 is included this will be unit31
        # Else probably uint16 but good to check!
        out_dtype = out_change[out_variables[0]].values.dtype

        out_ds = rasterio.open(out_level4_change_file, "w", driver="GTiff",
                               height=out_change.dims["y"], width=out_change.dims["x"],
                               count=len(out_variables), dtype=out_dtype,
                               nodata=0,
                               crs=in_ds.crs, transform=in_ds.transform,
                               compress="LZW", tiled="True")

        # Write each change layer out as a separate band
        for band, var in enumerate(list(out_variables), start=1):
            out_ds.write(out_change[var].values.astype(out_dtype), band)
            out_ds.set_band_description(band, var)
        out_ds.close()

    in_ds.close()

